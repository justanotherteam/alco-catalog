const ExtractTextPlugin = require('extract-text-webpack-plugin');
const helpers = require('./helpers');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
    context: helpers.root('src'),
    entry: ['babel-polyfill', '../src/app/app.js'],
    resolve: {
        modules: [path.resolve(__dirname, '../src'), 'node_modules'],
        extensions: ['.js'],
        alias: {
            moment: 'moment/src/moment'
        }
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style-loader!css-loader" },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        'postcss-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                includePaths: [path.resolve(__dirname, '../src')],
                            },
                        },
                    ]
                })
            },
            {
                test: /\.js?$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/preset-react', '@babel/preset-env'],
                    plugins: [
                        ['@babel/plugin-proposal-decorators', {'legacy': true}],
                        ['@babel/plugin-proposal-class-properties', {'loose': true}],
                        '@babel/plugin-syntax-dynamic-import',
                        '@babel/plugin-proposal-optional-chaining'
                    ],
                }
            },
        ]
    },
    output: {
        path: helpers.root('dist'),
        filename: 'app.min.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html'
        }),
        new ExtractTextPlugin({
            filename: 'styles.[contenthash].css',
            allChunks: true
        })
    ]
};
