const path = require('path');

const EVENT = process.env.npm_lifecycle_event || '';
const ROOT = path.resolve(__dirname, '..');
const root = path.join.bind(path, ROOT);
const hasNpmFlag = flag => EVENT.includes(flag);

exports.root = root;
exports.hasNpmFlag = hasNpmFlag;
