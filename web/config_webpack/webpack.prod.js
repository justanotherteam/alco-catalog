const commonConfig = require('./webpack.common.js');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const helpers = require('./helpers');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');

const PUBLIC_URL = '/';
const getAppConfigFolder = () => {
    let folderName = 'pnn_dev';

    if (helpers.hasNpmFlag('nivac')) {
        folderName = 'nivac_prod';
    }
    else if (helpers.hasNpmFlag('pnn-stg')) {
        folderName = 'pnn_stg';
    }

    return folderName;
};
const appConfigFolder = getAppConfigFolder();

module.exports = webpackMerge(commonConfig, {
    devtool: false,
    externals: {
        'Config': JSON.stringify(require(`../config_app/${appConfigFolder}/config.json`))
    },
    output: {
        path: helpers.root('dist'),
        publicPath: PUBLIC_URL,
        filename: '[name].[chunkhash].js',
        chunkFilename: '[id].[chunkhash].chunk.js'
    },
    module: {
        loaders: [
            {
                test: /\.(png|jpg|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'assets/[hash].[ext]'
                        }
                    }
                ]
            },
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production'),
                PUBLIC_URL: JSON.stringify(PUBLIC_URL)
            }
        }),
        new CopyWebpackPlugin([
            {
                from: helpers.root('config_app', 'web.config')
            },
            {
                from: helpers.root(`config_app/${appConfigFolder}`, 'apple-app-site-association')
            },
            {
                from: helpers.root('src', 'manifest.json')
            },
            {
                from: helpers.root('src', 'assets', 'icons'),
                to: 'assets/icons'
            }
        ]),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'vendor.[chunkhash].js',
            minChunks(module) {
                return module.context &&
                    module.context.indexOf('node_modules') >= 0;
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            output: {
                comments: false
            },
            sourceMap: true
        }),
        new webpack.ProgressPlugin()
    ]
});
