const commonConfig = require('./webpack.common.js');
const path = require('path');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const PUBLIC_URL = '/';

module.exports = webpackMerge(commonConfig, {
    devtool: 'inline-sourcemap',
    devServer: {
        historyApiFallback: true,
        disableHostCheck: true,
        host: 'localhost',
        port: 3000
    },
    externals: {
        'Config': JSON.stringify(require('../config_app/dev/config.json'))
    },
    module: {
        loaders: [
            {
                test: /\.(png|jpg|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]'
                        }
                    }
                ]
            },
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('development'),
                PUBLIC_URL: JSON.stringify(PUBLIC_URL)
            }
        })
    ],
});
