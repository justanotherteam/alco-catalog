import localStorageService from 'app/services/localStorageService';
import sessionStorageService from 'app/services/sessionStorageService';
import {LOGIN_TOKEN} from 'app/consts/localStorageKeys';

const login = (authorizationToken, isRememberMe, callback) => {
    if (isRememberMe) {
        localStorageService.setItem(LOGIN_TOKEN, authorizationToken);
    }
    else {
        sessionStorageService.setItem(LOGIN_TOKEN, authorizationToken);
    }

    callback();
};

const logout = () => {
    sessionStorageService.removeItem(LOGIN_TOKEN);
    localStorageService.removeItem(LOGIN_TOKEN);
};

const getAuthorizationToken = () => sessionStorageService.getItem(LOGIN_TOKEN) || localStorageService.getItem(LOGIN_TOKEN);

export default {
    getAuthorizationToken,
    login,
    logout,
}