import * as apiServiceMethods from 'app/consts/apiServiceMethods';

const getApiRequestNewState = (state, newData, apiServiceMethod) => ({
    ...state,
    requests: {
        ...state.requests,
        [apiServiceMethod]: {
            ...state.requests[apiServiceMethod],
            data: newData
        }
    }
});

const getRequestData = (state, action) => {
    const {requestType, storeParams} = action;
    let data = action.payload;

    if (storeParams && storeParams.addToPreviousData) {
        if (state.requests[requestType] && state.requests[requestType].data) {
            const stateData = state.requests[requestType].data;
            const {idPropertyName} = storeParams;

            if (idPropertyName) {
                data = stateData.concat(data.filter(function (item) {
                    const duplicatedData = stateData.find((stateDataItem) => stateDataItem[idPropertyName] === item[idPropertyName]);

                    return !duplicatedData;
                }));
            }
            else {
                data = stateData.concat(data);
            }
        }
    }

    return data;
};

const removeFromRequest = (state, requestTypes, filter) => {
    let requests = {};

    requestTypes.forEach((itemsRequestType) => {
        const request = state.requests[itemsRequestType];

        if (request && request.data) {
            let newData;

            if (Array.isArray(request.data) && request.data.length) {
                newData = request.data.filter(filter);
            }
            if (newData) {
                requests[itemsRequestType] = {data: newData};
            }
        }
    });

    return setNewRequestState(state, requests);
};

const setNewState = (state, requestTypes, dataMapper) => {
    let requests = {};

    requestTypes.forEach((itemsRequestType) => {
        const request = state.requests[itemsRequestType];

        if (request && request.data) {
            let newData;

            if (Array.isArray(request.data) && request.data.length) {
                newData = request.data.map(item => dataMapper(item, itemsRequestType));
            }
            else {
                newData = dataMapper(request.data, itemsRequestType);
            }

            if (newData) {
                requests[itemsRequestType] = {data: newData};
            }
        }
    });

    return setNewRequestState(state, requests);
};

const setNewRequestState = (state, requests) => {
    return {
        ...state,
        requests: {
            ...state.requests,
            ...requests
        }
    };
};

export default {
    getRequestData,
    removeFromRequest,
    setNewState,
};