import {createBrowserHistory} from 'history'

import nodeUtils from 'app/utils/nodeUtils';

export default createBrowserHistory({
    basename: nodeUtils.getPublicUrl()
});