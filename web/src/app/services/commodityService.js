import server from 'app/services/server';
import {
    GET_COMMODITIES, 
    GET_FILTERS, 
    GET_RANGE,
    GET_COMMODITIES_ALL,
    GET_COMMODITIES_NEWEST,
    GET_COMMODITIES_TO_PARSE,
    DELETE_COMMODITY,
    ADD_COMMODITY,
    GET_RESPONSES_NEWEST,
} from 'app/consts/apiServiceMethods';
import {
    COMMODITY_FULL_DATA, 
    COMMODITY_AVAILABLE_FILTERS,
    COMMODITIES_ALL,
    COMMODITIES_RANGE,
    COMMODITIES_TO_PARSE,
    RESPONSES_NEWEST,
} from 'app/consts/actionTypes';
import {
    NEWEST_COMMODITIES_COUNT, 
    NEWEST_RESPONSES_COUNT,
} from 'app/consts/global';

const getCommodityFullData = (id) => (dispatch) => dispatch(server.get(`${GET_COMMODITIES}/${id}`))
    .then((response) => {
        dispatch({type: COMMODITY_FULL_DATA, payload: response.data});
    });

const getFilters = (categoryId) => (dispatch) => dispatch(server.get(`${GET_FILTERS}/${categoryId}`))
    .then((response) => {
        dispatch({type: COMMODITY_AVAILABLE_FILTERS, payload: response.data});
    });

    const getCommodities = (data) => (dispatch) => dispatch(server.post(GET_RANGE, data))
    .then((response) => {
        dispatch({type: COMMODITIES_RANGE, payload: response.data});
    });

const getCommoditiesAll = () => (dispatch) => dispatch(server.get(GET_COMMODITIES_ALL))
    .then((response) => {
        dispatch({type: COMMODITIES_ALL, payload: response.data});
    });

const getCommoditiesNewest = () => (dispatch) => dispatch(server.get(`${GET_COMMODITIES_NEWEST}/${NEWEST_COMMODITIES_COUNT}`))
    .then((response) => {
        dispatch({type: COMMODITIES_RANGE, payload: {count: 0, commodities: response.data}});
    });

const getCommoditiesToParse = () => (dispatch) => dispatch(server.get(GET_COMMODITIES_TO_PARSE))
    .then((response) => {
        dispatch({type: COMMODITIES_TO_PARSE, payload: response.data});
    });

const getResponsesNewest = () => (dispatch) => dispatch(server.get(`${GET_RESPONSES_NEWEST}/${NEWEST_RESPONSES_COUNT}`))
    .then((response) => {
        dispatch({type: RESPONSES_NEWEST, payload: response.data});
    });

const deleteCommodity = (id) => (dispatch) => dispatch(server.requestDelete(`${DELETE_COMMODITY}/${id}`));

const addCommodity = (data) => (dispatch) => dispatch(server.post(ADD_COMMODITY, data));
    

export default {
    getCommodityFullData,
    getFilters,
    getCommodities,
    getCommoditiesAll,
    getCommoditiesNewest,
    getCommoditiesToParse,
    deleteCommodity,
    addCommodity,
    getResponsesNewest,
};