import * as qs from 'qs';
import axios from 'axios';

import * as statusCodes from 'app/consts/statusCodes';
import appConfig from 'app/utils/appConfig';
import authService from 'app/services/authService';
import urlUtils from 'app/utils/urlUtils';
import { CATEGORIES_ALL, } from 'app/consts/actionTypes';

const get = (url, params = {}) => requestApi(url, 'get', {
    params: {
        ...params
    },
    paramsSerializer: urlUtils.stringifyParams
});
const getPostDeleteVerb = (isPost) => isPost ? post : requestDelete;
const post = (url, body = {}) => requestApi(url, 'post', {data: body});
const put = (url, requestConfig = {}) => requestApi(url, 'put', requestConfig);
const requestDelete = (url, requestConfig = {}) => requestApi(url, 'delete', requestConfig);

const requestApi = (url, method, requestConfig = {}) => {
    return (dispatch) => {
        const authorizationToken = authService.getAuthorizationToken();
        let headers = {'Content-Type': 'application/json'};
        if(authorizationToken)
            headers = {...headers, 'Authorization': `Bearer ${authorizationToken}`};

        let config = {method: method, url: url, baseURL: appConfig.getApiEndpoint(), headers};
        config = {...config, ...requestConfig};

        return request(config);
    }
};

const request = (requestConfig) => axios.request(requestConfig);
const getApiMethodFullPath = (method) => urlUtils.join(appConfig.getApiEndpoint(), method);

const prepereRawData = (data) => {
    return 'count=10&page=1&filters=[]';
}

export default {
    get,
    getApiMethodFullPath,
    getPostDeleteVerb,
    post,
    put,
    requestDelete,
};