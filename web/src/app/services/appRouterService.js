import * as routePaths from 'app/consts/routePaths';
import routerService from 'app/services/routerService';

const forwardToHome = () => routerService.replace(routePaths.HOME);
const forwardToCommodity = (id) => routerService.replace(`${routePaths.COMMODITY}/${id}`);

const replaceToLogin = () => routerService.replace(routePaths.LOGIN);

export default {
    forwardToHome,
    replaceToLogin,
    forwardToCommodity,
};