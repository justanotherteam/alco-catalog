import history from 'app/services/history'
import {WILDCARD} from 'app/consts/routePaths';

const forwardTo = (location) => history.push(location);
const forwardToNotFound = () => history.push(WILDCARD);
const getRoute = (routes) => [...routes].pop();
const goBack = () => history.goBack();
const reload = () => window.location.reload();
const replace = (location) => history.replace(location);

export default {
    forwardTo,
    forwardToNotFound,
    getRoute,
    goBack,
    replace,
};