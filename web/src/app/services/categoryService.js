import server from 'app/services/server';
import {GET_CATEGORIES, ADD_CATEGORY, DELETE_CATEGORY} from 'app/consts/apiServiceMethods';
import {CATEGORIES_ALL} from 'app/consts/actionTypes';

const getCategories = () => (dispatch) => dispatch(server.get(GET_CATEGORIES))
    .then((response) => {
        dispatch({type: CATEGORIES_ALL, payload: response.data.categories});
    });

const deleteCategory = (id) => (dispatch) => dispatch(server.requestDelete(`${DELETE_CATEGORY}/${id}`));

const addCategory = (name) => (dispatch) => dispatch(server.put(`${ADD_CATEGORY}/${name}`));
    
export default {
    getCategories,
    deleteCategory,
    addCategory,
};