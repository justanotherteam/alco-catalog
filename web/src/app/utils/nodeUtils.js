const getPublicUrl = () => process.env.PUBLIC_URL;

export default {
    getPublicUrl,
}