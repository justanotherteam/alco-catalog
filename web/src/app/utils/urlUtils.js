import * as qs from 'qs';
import urlJoin from 'url-join';

const addQueryString = (path, query) => `${path}?${query}`;

const combinePathWithQueryParams = (path, params) => combinePathWithQueryParamsObj(path, {params: params});

const combinePathWithQueryParamsObj = (path, queryParamsObj) => {
    const query = stringifyParams(queryParamsObj);

    return addQueryString(path, query);
};

const join = (...params) => urlJoin(params);

const stringifyParams = (params) => qs.stringify(params, {indices: false});

export default {
    addQueryString,
    combinePathWithQueryParams,
    combinePathWithQueryParamsObj,
    join,
    stringifyParams,
}