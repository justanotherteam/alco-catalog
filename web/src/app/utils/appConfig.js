const Config = require('Config');

const getAllowedImageExtensions = () => Config.allowedImageExtensions;
const getAllowedVideoExtensions = () => Config.allowedVideoExtensions;
const getApiEndpoint = () => Config.apiEndpoint;
const getBackendEndpoint = () => Config.backendEndpoint;
const getDefaultCultureCode = () => Config.defaultCultureCode;
const getImageMaxSize = () => Config.imageMaxSize;
const getStorageRoot = () => Config.storageRoot;
const getVideoMaxSize = () => Config.videoMaxSize;

export default {
    getAllowedImageExtensions,
    getAllowedVideoExtensions,
    getApiEndpoint,
    getBackendEndpoint,
    getDefaultCultureCode,
    getImageMaxSize,
    getStorageRoot,
    getVideoMaxSize,
}