import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Router} from 'react-router-dom';

import App from 'app/containers/App';
import configureStore from 'app/store';
import history from 'app/services/history'
import Routes from 'app/containers/App/Routes/routes';

require('./../assets/styles/main.scss');

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <App>
            <Router history={history}>
                <Routes/>
            </Router>
        </App>
    </Provider>, document.getElementById('app')
);
