import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import CommodityComponent from 'app/components/CommodityComponent';
import commodityActions from 'app/actions/commodityActions';
import commodityService from 'app/services/commodityService';
import appRouterService from 'app/services/appRouterService';
import {makeSelectCommoditiesNewest, makeSelectResponsesNewest} from 'app/selectors/commodity';

const mapStateToProps = createStructuredSelector({
    commodities: makeSelectCommoditiesNewest(),
    responses: makeSelectResponsesNewest(),
}); 

@connect(mapStateToProps)
export default class HomePage extends React.PureComponent {
    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(commodityService.getCommoditiesNewest());
        dispatch(commodityService.getResponsesNewest());
    }

    componentWillUnmount() {
        const {dispatch} = this.props;
        dispatch(commodityActions.cleanCommoditiesRange());
        dispatch(commodityActions.cleanResponsesNewest());
    }
    
    render() {
        const { commodities, responses } = this.props;

        const responsesComponent = (
            <div className="responses-items">
                {responses.map((r, i) => { 
                    return (
                        <div className="response-item" key={i} onClick={() => appRouterService.forwardToCommodity(r.commodityId)}>
                            <div className="response-header">
                                <div className="response-author">
                                    <p>{r.author || "anonimus"}</p>
                                </div>
                                <div className="response-rating">
                                    <p>{r.rating}</p>
                                </div>
                            </div>
                            <div className="response-text">
                                <p>{r.text}</p>
                            </div>
                        </div>
                    );
                })}
            </div>
        );

        return(
            <div className="page-content home-page">
                <div className="content">
                    <div className="header-title">
                        <h1>Home Page</h1>
                    </div>
                    <div className="commodities-newest">
                        <h1>Newest commodities:</h1>
                        <div className="commodities-list-container">
                            <div className="commodities-list">
                                {
                                    commodities.map(c => {
                                        return(
                                            <CommodityComponent
                                                id={c.id}
                                                key={c.id}
                                            />
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div className="responses-newest">
                        <h1>Newest responses:</h1>
                        {responsesComponent}
                    </div>
                </div>
            </div>
        )
    }
}