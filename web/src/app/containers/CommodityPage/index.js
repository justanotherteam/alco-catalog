import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import Loader from 'app/components/Loader';
import Link from 'app/components/Link';
import {makeSelectFullCommodityData} from 'app/selectors/commodity';
import commodityService from 'app/services/commodityService';
import * as routeParams from 'app/consts/routeParams';

const mapStateToProps = createStructuredSelector({
    commodity: makeSelectFullCommodityData(),
}); 

@connect(mapStateToProps)
export default class CommodityPage extends React.PureComponent {
    componentDidMount() {
        const {dispatch, match} = this.props;
        dispatch(commodityService.getCommodityFullData(match.params[routeParams.COMMODITY_ID]));
    }

    render() {
        const { commodity } = this.props;
        if(!commodity)
            return(
                <Loader isVisible={true}/>
            );

        const prices = commodity.prices.map(p => p.price);
        const minPrice = Math.min(...prices);
        const maxPrice = Math.max(...prices);
        const pricesString = minPrice === maxPrice ? minPrice : `${minPrice} - ${maxPrice}`;

        const storesPrices = (
            <div className="store-prices">
                <h4 className="stores">Stores:</h4>
                {commodity.prices.map((p, i) => { 
                    return (
                        <Link to={p.url} isExternalLink={true} key={i}>
                            <div className="price-item" >
                                <div className="store-image">
                                    <img src={p.storeImageUrl}/>
                                </div>
                                <div className="store-price">
                                    <p>{p.price}</p>
                                </div>
                            </div>
                        </Link>
                    );
                })}
            </div>
        );

        const characteristics = (
            <div className="characteristic-items">
                {commodity.characteristics.map((c, i) => { 
                    return [
                        <div className="characteristic-name" key={i * 2}>
                            {c.name}
                        </div>,
                        <div className="characteristic-value" key={i * 2 + 1}>
                            {c.value}
                        </div>
                    ];
                })}
            </div>
        );

        const responses = (
            <div className="responses-items">
                {commodity.responses.map((r, i) => { 
                    return (
                        <div className="response-item" key={i}>
                            <div className="response-header">
                                <div className="response-author">
                                    <p>{r.author || "anonimus"}</p>
                                </div>
                                <div className="response-rating">
                                    <p>{r.rating}</p>
                                </div>
                            </div>
                            <div className="response-text">
                                <p>{r.text}</p>
                            </div>
                        </div>
                    );
                })}
            </div>
        );

        return(
            <div className="commodity-page">
                <h1>{commodity.name}</h1>
                <div className="commodity-main-container">
                    <div className="commodity-image">
                        <img src={commodity.pirtureUrl}/>
                    </div>
                    <div className="price-container">
                        <h2>{pricesString + "$"}</h2>
                        {storesPrices}
                    </div>
                </div>
                <div className="commodity-characteristic-container">
                    <div className="characteristic-container">
                        <h2>Characteristics:</h2>
                        {characteristics}
                    </div>
                    <div className="description-container">
                        <h2>Description:</h2>
                        <div className="description">
                            {commodity.description || "no description"}
                        </div>
                    </div>
                </div>
                <div className="commodity-responses-container">
                    <h2>Responses:</h2>
                    {responses}
                </div>
            </div>
        )
    }
}