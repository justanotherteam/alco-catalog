import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import {login} from 'app/actions/authActions';

const mapStateToProps = createStructuredSelector({
}); 

@connect(mapStateToProps)
export default class LoginPage extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            password: "",
            isRememberMe: true,
        }
    }

    handleSubmit() {
        const {dispatch} = this.props;
        dispatch(login(this.state.userName, this.state.password, this.state.isRememberMe));
    }

    render() {        
        return(
            <div className="page-content login-page">
                <div className="content">
                    <div className="header-title">
                        <h1>Login</h1>
                    </div>
                    <div className="login-form">
                        <div className="form-field">
                            <label>
                                User Name:
                            </label>
                            <input type="text" value={this.state.userName} onChange={(e) => this.setState({userName: e.target.value})} />
                        </div>
                        <div className="form-field">
                            <label>
                                Password:
                            </label>
                            <input type="text" value={this.state.password} onChange={(e) => this.setState({password: e.target.value})} />
                        </div>
                        <div className="login-button">
                            <button className="add-button" onClick={() => {this.handleSubmit()}}>
                                Login
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}