import React from 'react';

export default class NotFoundPage extends React.PureComponent {
    render() {
        return <div className='not-found-page'>
            <div className='not-found-header'>Page not found.</div>
        </div>;
    }
}