import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {Route, Switch, Redirect, withRouter} from 'react-router-dom';

import * as routePaths from 'app/consts/routePaths';
import AdminPage from 'app/containers/Admin';
import CatalogPage from 'app/containers/CatalogPage';
import CommodityPage from 'app/containers/CommodityPage';
import HomePage from 'app/containers/HomePage';
import LoginPage from 'app/containers/LoginPage';
import LogoutPage from 'app/containers/LogoutPage';
import NotFound from 'app/containers/NotFoundPage';
import PrivateRoute from 'app/containers/App/Routes/privateRoute';
import PublicRoute from 'app/containers/App/Routes/publicRoute';
import {makeSelectIsLoggedIn} from 'app/selectors/auth';

const mapStateToProps = createStructuredSelector({
    isLoggedIn: makeSelectIsLoggedIn(),
});

@withRouter
@connect(mapStateToProps)
export default class App extends React.PureComponent {
    render() {
        const {isLoggedIn} = this.props;
        return <Switch>
            <Route exact path={routePaths.ROOT} render={() => (
                <Redirect to={routePaths.HOME}/>
            )}/>

            <PublicRoute exact path={routePaths.HOME} component={HomePage}/>
            <PublicRoute exact path={routePaths.CATALOG_PARAMETERIZED} component={CatalogPage}/>
            <PublicRoute exact path={routePaths.COMMODITY_PARAMETERIZED} component={CommodityPage}/>
            <PublicRoute exact path={routePaths.LOGIN} component={LoginPage}/>
            <PublicRoute exact path={routePaths.LOGOUT} component={LogoutPage}/>

            <PrivateRoute path={routePaths.ADMIN_PAGE} component={AdminPage}/>

            <Route component={NotFound}/>
        </Switch>;
    }
}