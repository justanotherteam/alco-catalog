import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {Redirect} from 'react-router-dom';

import * as routePaths from 'app/consts/routePaths';
import AppLayout from 'app/containers/App/appLayout';
import {makeSelectIsLoggedIn} from 'app/selectors/auth';

const mapStateToProps = createStructuredSelector({
    isLoggedIn: makeSelectIsLoggedIn(),
});

@connect(mapStateToProps)
export default class PrivateRoute extends React.PureComponent {
    render() {
        const {isLoggedIn} = this.props;

        return isLoggedIn
            ? <AppLayout {...this.props}/>
            : <Redirect to={routePaths.LOGIN}/>;
    }
}