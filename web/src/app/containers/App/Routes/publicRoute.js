import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {Redirect} from 'react-router-dom';

import * as routePaths from 'app/consts/routePaths';
import AppLayout from 'app/containers/App/appLayout';
import authService from 'app/services/authService';
import {makeSelectIsLoggedIn} from 'app/selectors/auth';

const mapStateToProps = createStructuredSelector({
    isLoggedIn: makeSelectIsLoggedIn(),
});

@connect(mapStateToProps)
export default class PublicRoute extends React.PureComponent {
    render() {
        const {dispatch, isLoggedIn} = this.props;
        const {location} = this.props;
        return <AppLayout {...this.props}/>;
    }
}