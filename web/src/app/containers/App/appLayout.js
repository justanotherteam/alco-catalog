import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {Route} from 'react-router-dom';

import Header from 'app/components/Header';
import LoaderGlobal from 'app/components/Loader/loaderGlobal';
import InfoPopup from 'app/components/Popup/infoPopup';
import SelectPopup from 'app/components/Popup/selectPopup';
import {setLocationHistory} from 'app/actions/router';
import {
    makeSelectInfoPopupIsVisible, makeIsLoaderVisible,
    makeSelectSelectPopupIsVisible
} from 'app/selectors/global';

const mapStateToProps = createStructuredSelector({
    isInfoPopupVisible: makeSelectInfoPopupIsVisible(),
    isSelectPopupVisible: makeSelectSelectPopupIsVisible(),
    isLoaderVisible: makeIsLoaderVisible(),
});

@connect(mapStateToProps)
export default class AppLayout extends React.Component {
    componentDidMount() {
        this.dispatchLocationHistory();
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            this.dispatchLocationHistory();
        }
    }

    render() {
        const {isLoggedIn, component: Component, path, isAppLoading, isInfoPopupVisible, isSelectPopupVisible} = this.props;
        return <Route path={path} render={matchProps => [
            isInfoPopupVisible ? <InfoPopup/> : null,
            isSelectPopupVisible ? <SelectPopup/> : null, 
            <div
                className={'mainContainer'}
                id='mainContainer'
            >
                {
                    isAppLoading
                        ? <LoaderGlobal isVisible={true}/>
                        : <div>
                            <Header/>
                                <Component {...matchProps} />
                                {
                                    isLoggedIn &&
                                    <div>
                                    </div>
                                }
                        </div>
                }       
            </div>
        ]}/>;
    }

    dispatchLocationHistory = () => {
        const {dispatch, computedMatch: {path, url}, location: {search}} = this.props;

        dispatch(setLocationHistory(path, url, search));
    };
}