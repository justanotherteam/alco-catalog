import React from 'react';
import {connect} from 'react-redux';

import categoryService from 'app/services/categoryService';

@connect()
export default class App extends React.PureComponent {
    componentDidMount() {
        const {dispatch} = this.props;
        
        dispatch(categoryService.getCategories());
    }

    render() {
        return  this.props.children;
    }
}