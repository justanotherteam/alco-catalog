import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import CommoditiesContainer from 'app/components/CommoditiesContainer';
import Filters from 'app/components/Filters';
import {setCurrentCategory, cleanCurrentCategory} from 'app/actions/categoryActions';
import commodityActions from 'app/actions/commodityActions';
import commodityService from 'app/services/commodityService';
import {makeSelectActiveFilters} from 'app/selectors/commodity';
import {makeSelectCurrentCargory} from 'app/selectors/category';
import * as routeParams from 'app/consts/routeParams';
import {COUNT_ON_PAGE} from 'app/consts/global';

const mapStateToProps = createStructuredSelector({
}); 

@connect(mapStateToProps)
export default class CatalogPage extends React.PureComponent {
    componentDidMount() {
        const {dispatch, match} = this.props;
        dispatch(setCurrentCategory(match.params[routeParams.CATEGORY_ID]));
        dispatch(commodityService.getFilters(match.params[routeParams.CATEGORY_ID]));
    }

    componentDidUpdate() {
        const {dispatch, match} = this.props;
        dispatch(setCurrentCategory(match.params[routeParams.CATEGORY_ID]));
        dispatch(commodityService.getFilters(match.params[routeParams.CATEGORY_ID]));
    }

    componentWillUnmount() {
        const {dispatch} = this.props;
        dispatch(cleanCurrentCategory());
        dispatch(commodityActions.cleanAvailableFilters());
        dispatch(commodityActions.cleanAllActiveFilters());
    }

    render() {        
        return(
            <div className="page-content catalog-page">
                <div className="content">
                    <Filters/>
                    <CommoditiesContainer/>
                </div>
            </div>
        )
    }
}