import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import Select from 'react-select'

import Link from 'app/components/Link';
import TabsHeader from 'app/components/TabsHeader';
import {showSelectPopup, showInfoPopup} from 'app/actions/global';
import commodityActions from 'app/actions/commodityActions';
import categoryService from 'app/services/categoryService';
import commodityService from 'app/services/commodityService';
import {makeSelectAllCategories} from 'app/selectors/category';
import {makeSelectCommoditiesAll, makeSelectCommoditiesToParse} from 'app/selectors/commodity';
import {COMMODITY} from 'app/consts/routePaths';
import {ADMIN_CATEGORY, ADMIN_COMMODITY} from 'app/consts/tabIndexes';


const mapStateToProps = createStructuredSelector({
    categories: makeSelectAllCategories(),
    commodities: makeSelectCommoditiesAll(),
    commoditiesToParse: makeSelectCommoditiesToParse(),
}); 

@connect(mapStateToProps)
export default class AdminPage extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            headerIndex: ADMIN_COMMODITY,
            newCategory: {
                name: "",
            },
            newCommodity: {
                code: "",
                categoryId: "",
            },
        };
        this.onHeaderItemClick = (index) => {
            this.setState({headerIndex: index});
        }
    }  

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(commodityService.getCommoditiesAll());
        dispatch(commodityService.getCommoditiesToParse());
    }

    componentWillUnmount() {
        const {dispatch} = this.props;
        dispatch(commodityActions.cleanCommoditiesAll());
        dispatch(commodityActions.cleanCommoditiesToParse());
    }

    onCategoryDelClick(id) {
        const {dispatch} = this.props;
        dispatch(showSelectPopup({
            isVisible: true, 
            text: "Are you shure?",
            onYesHandler: () => {
                dispatch(categoryService.deleteCategory(id))
                .then(() => {
                    dispatch(categoryService.getCategories());
                });
            }
        }))
    }
    
    onCategoryAddClick(name) {
        const {dispatch} = this.props;
        dispatch(categoryService.addCategory(name))
            .then(() => {
                dispatch(categoryService.getCategories());
            })
            .catch((ex) => {
                const {data} = ex.response;
                dispatch(showInfoPopup({
                    isVisible: true, 
                    text: data.title ? data.title : data,
                }))
            });
    }
    
    onCommodityDelClick(id) {
        const {dispatch} = this.props;
        dispatch(showSelectPopup({
            isVisible: true, 
            text: "Are you shure?",
            onYesHandler: () => {
                dispatch(commodityService.deleteCommodity(id))
                .then(() => {
                    dispatch(commodityService.getCommoditiesAll());
                });
            },
            onNoHandler: () => {},
        }));
    }
    
    onCommodityAddClick() {
        const {dispatch} = this.props;
        dispatch(commodityService.addCommodity(this.state.newCommodity))
            .then(() => {
                dispatch(commodityService.getCommoditiesToParse());
                dispatch(commodityService.getCommoditiesAll());
            })
            .catch((ex) => {
                const {data} = ex.response;
                dispatch(showInfoPopup({
                    isVisible: true, 
                    text: data.title ? data.title : data,
                }))
            });
    }
    
    render() {
        const {categories, commodities, commoditiesToParse} = this.props;

        const tabs = [
            {
                index: ADMIN_COMMODITY,
                title: "Commodities",
            },
            {
                index: ADMIN_CATEGORY,
                title: "Categories",
            },
        ]

        const options = categories.map((c) => {
            return {value: c.id, label: c.name};
        });

        let adminComponent;
        switch(this.state.headerIndex) {
            case ADMIN_CATEGORY : {
                adminComponent = <div className="admin-component">
                    <div className="add-component">
                        <div className="add-category-name add-item">
                            <label>
                                Category Name:
                            </label>
                            <input 
                                type="text" 
                                value={this.state.newCategory.name} 
                                onChange={(e) => this.setState({newCategory: {...this.state.newCategory, name: e.target.value}})} 
                            />
                        </div>
                        <button className="add-button" onClick={() => this.onCategoryAddClick(this.state.newCategory.name)}>ADD</button>
                    </div>
                    {categories.map((c, i) => {
                        return <div className="edit-item category-edit-item" key={i}>
                            <div className="edit-item-element-id edit-item-element">{c.id}</div>
                            <div className="edit-item-element-name-category edit-item-element">{c.name}</div>
                            <div className="edit-item-element-button edit-item-element">
                                <button className="delete-button" onClick={() => this.onCategoryDelClick(c.id)}>DEL</button>
                            </div>
                        </div>
                    })}
                </div>
                break;
            }
            case ADMIN_COMMODITY : {
                adminComponent = <div className="admin-component">
                    <div className="add-component">
                        <div className="add-commodity-code add-item">
                            <label>
                                Commodity Code:
                            </label>
                            <input 
                                type="text" 
                                value={this.state.newCommodity.code} 
                                onChange={(e) => this.setState({newCommodity: {...this.state.newCommodity, code: e.target.value}})} 
                            />
                        </div>
                        <div className="add-commodity-category add-item">
                            <label>
                                Caegory:
                            </label>
                           <Select 
                                value={this.state.newCommodity.categoryId}
                                options={options}
                                onChange={(option) => this.setState({newCommodity: {...this.state.newCommodity, categoryId: option.value}})}    
                            />
                        </div>
                        <button className="add-button add-button-commodities" onClick={() => this.onCommodityAddClick(this.state.newCommodity.code)}>ADD</button>
                    </div>
                    <h1>To Parse: {commoditiesToParse.length}</h1>
                    {commoditiesToParse.map((c, i) => {
                            return <div className="edit-item commodity-edit-item" key={i}>
                                <div className="edit-item-element">
                                    <p>
                                        {c.code}
                                    </p>
                                </div>
                                <div className="edit-item-element">
                                    <p>
                                        {c.id}
                                    </p>
                                </div>
                                <div className="edit-item-element">
                                    <p>
                                        {c.categoryName}
                                    </p>
                                </div>
                                <div className="edit-item-element">
                                    <p>
                                        {c.isSended ? "PROCESSED" : "PENDING"}
                                    </p>
                                </div>
                            </div>
                        })}                    
                    <h1>Parsed: {commodities.length}</h1>
                    {commodities.map((c, i) => {
                            return <div className="edit-item commodity-edit-item" key={i} >
                                <div className="edit-item-element-image edit-item-element">
                                    <Link to={`${COMMODITY}/${c.id}`}>
                                        <img src={c.imageUrl}></img>
                                    </Link>
                                </div>
                                <div className="edit-item-element-code edit-item-element">
                                    <p>
                                        {c.code}
                                    </p>
                                </div>
                                <div className="edit-item-element-name edit-item-element">
                                    <p>
                                        {c.name}
                                    </p>
                                </div>
                                <div className="edit-item-element-id edit-item-element">
                                    <p>
                                        {c.id}
                                    </p>
                                </div>
                                <div className="edit-item-element-button edit-item-element">
                                    <button className="delete-button" onClick={() => this.onCommodityDelClick(c.id)}>DEL</button>
                                </div>
                            </div>
                        })}                    
                </div>
                break;
            }
        };

        return(
            <div className="page-content admin-page">
                <div className="content">
                    <div className="header-title">
                        <h1>Admin Category</h1>
                    </div>
                    <TabsHeader
                        onHeaderItemClick={this.onHeaderItemClick}
                        tabs={tabs}
                        startIndex={this.state.headerIndex}
                    />
                    {adminComponent}
                </div>
            </div>
        )
    }
}