import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import {logout} from 'app/actions/authActions';

const mapStateToProps = createStructuredSelector({
}); 

@connect(mapStateToProps)
export default class LogoutPage extends React.PureComponent {
    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(logout());
    }

    render() {        
        return null;
    }
}