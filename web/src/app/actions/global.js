import {
    SET_IS_LOADER_VISIBLE,
    SET_IS_INFO_POPUP,
    SET_IS_INFO_POPUP_CLEAN,
    SET_IS_SELECT_POPUP,
    SET_IS_SELECT_POPUP_CLEAN,
} from 'app/consts/actionTypes';

export function showLoader() {
    return setIsLoaderVisible(true);
}

export function hideLoader() {
    return setIsLoaderVisible(false);
}

export function setIsLoaderVisible(isVisible) {
    return (dispatch) => {
        dispatch({type: SET_IS_LOADER_VISIBLE, payload: isVisible});
    }
}

export function showInfoPopup(popupData) {
    return (dispatch) => {
        dispatch({type: SET_IS_INFO_POPUP, payload: popupData});
    }
}

export function hideInfoPopup() {
    return (dispatch) => {
        dispatch({type: SET_IS_INFO_POPUP_CLEAN});
    }
}

export function showSelectPopup(popupData) {
    return (dispatch) => {
        dispatch({type: SET_IS_SELECT_POPUP, payload: popupData});
    }
}

export function hideSelectPopup() {
    return (dispatch) => {
        dispatch({type: SET_IS_SELECT_POPUP_CLEAN});
    }
}
