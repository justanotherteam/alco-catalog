import {
    COMMODITY_ACTIVE_FILTER_ADD, 
    COMMODITY_ACTIVE_FILTER_CLEAN,
    COMMODITY_ACTIVE_FILTER_CLEAN_ALL,
    COMMODITY_AVAILABLE_FILTERS_CLEAN,
    COMMODITIES_RANGE_CLEAN,
    COMMODITIES_ALL_CLEAN,
    COMMODITIES_TO_PARSE_CLEAN,
    RESPONSES_NEWEST_CLEAN,
} from 'app/consts/actionTypes';

const addActiveFilter = (filter) => (dispatch) => 
    dispatch({type: COMMODITY_ACTIVE_FILTER_ADD, payload: filter});

const cleanActiveFilter = (filterId) => (dispatch) => 
    dispatch({type: COMMODITY_ACTIVE_FILTER_CLEAN, payload: filterId});
    
const cleanAllActiveFilters = () => (dispatch) => 
    dispatch({type: COMMODITY_ACTIVE_FILTER_CLEAN_ALL});

const cleanAvailableFilters = () => (dispatch) => 
    dispatch({type: COMMODITY_AVAILABLE_FILTERS_CLEAN});
    
const cleanCommoditiesRange = () => (dispatch) => 
    dispatch({type: COMMODITIES_RANGE_CLEAN});

const cleanCommoditiesAll = () => (dispatch) => 
    dispatch({type: COMMODITIES_ALL_CLEAN});

const cleanCommoditiesToParse = () => (dispatch) => 
    dispatch({type: COMMODITIES_TO_PARSE_CLEAN});

const cleanResponsesNewest = () => (dispatch) => 
    dispatch({type: RESPONSES_NEWEST_CLEAN});

export default {
    addActiveFilter,
    cleanActiveFilter,
    cleanAllActiveFilters,
    cleanAvailableFilters,
    cleanCommoditiesRange,
    cleanCommoditiesAll,
    cleanCommoditiesToParse,
    cleanResponsesNewest,
};