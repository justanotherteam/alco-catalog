import {SET_LOCATION_HISTORY} from 'app/consts/actionTypes';

export const setLocationHistory = (path, pathname) => ({type: SET_LOCATION_HISTORY, payload: {location: {path, pathname}}});