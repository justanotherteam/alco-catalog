import {
    CATEGORIES_ALL,
    CATEGORY_CURRENT,
    CATEGORY_CURRENT_CLEAN,
} from 'app/consts/actionTypes';

export function setCategories(categories) {
    return { type: CATEGORIES_ALL, payload: categories };
}

export function setCurrentCategory(categoryId) {
    return { type: CATEGORY_CURRENT, payload: categoryId };
}

export function cleanCurrentCategory() {
    return { type: CATEGORY_CURRENT_CLEAN };
}