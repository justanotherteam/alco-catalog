import appRouterService from 'app/services/appRouterService';
import auth from 'app/services/authService';
import server from 'app/services/server';
import {AUTH_LOGIN} from 'app/consts/apiServiceMethods';
import {showInfoPopup} from 'app/actions/global';
import {hideLoader} from 'app/actions/global';
import {
    AUTH_SET_AUTH_TOKEN,
    AUTH_SET_USER_NAME,
    AUTH_CLAER_DATA,
} from 'app/consts/actionTypes';

export function login(login, password, isRememberMe) {
    return (dispatch) => {
        const data = {
            userName: login,
            password,
        };

        return dispatch(server.post(AUTH_LOGIN, data))
            .then((response) => {
                const {data} = response;
                dispatch(loginWithToken(data.authorizationToken, isRememberMe));
                dispatch(setUserName(data.userName));

                return data;
            })
            .catch(({response}) => {
                const {data} = response;

                dispatch(hideLoader());
                dispatch(showInfoPopup({
                    isVisible: true, 
                    text: data.title ? data.title : data,
                }))

                return data;
            })
    }
}

export function loginWithToken(authorizationToken, isRememberMe) {
    return (dispatch) => {
        auth.login(authorizationToken, isRememberMe, () => {
            dispatch(setAuthToken(authorizationToken));
            appRouterService.forwardToHome();
        });
    }
}

export function logout() {
        auth.logout();
        appRouterService.replaceToLogin();

        return (dispatch) => {
            return dispatch(clearAuthData());
        };
}

export function setAuthToken(authorizationToken) {
    return {
        type: AUTH_SET_AUTH_TOKEN,
        payload: authorizationToken
    };
}

export function setUserName(userName) {
    return {
        type: AUTH_SET_USER_NAME,
        payload: userName
    };
}

export function clearAuthData() {
    return {
        type: AUTH_CLAER_DATA
    };
}
