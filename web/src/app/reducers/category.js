import {
    CATEGORIES_ALL,
    CATEGORIES_CLEAN,
    CATEGORY_CURRENT,
    CATEGORY_CURRENT_CLEAN,
} from 'app/consts/actionTypes';

export default function reducer(state = {
    currentCategory: null,
    categories: []
}, action) {
    switch (action.type) {
        case CATEGORIES_ALL: {
            return {...state, categories: action.payload};
        }
        case CATEGORIES_CLEAN: {
            return {...state, categories: []};
        }
        case CATEGORY_CURRENT: {
            return {...state, currentCategory: action.payload};
        }
        case CATEGORY_CURRENT_CLEAN: {
            return {...state, currentCategory: null};
        }
    }
    return state;
}
