import authService from 'app/services/authService';
import {
    AUTH_SET_AUTH_TOKEN,
    AUTH_SET_USER_NAME,
    AUTH_CLAER_DATA,
} from 'app/consts/actionTypes';

export default function reducer(state = {
    authorizationToken: authService.getAuthorizationToken(),
    userName: null,
}, action) {
    switch (action.type) {
        case AUTH_SET_AUTH_TOKEN: {
            return {...state, authorizationToken: action.payload}
        }
        case AUTH_SET_USER_NAME: {
            return {...state, userName: action.payload}
        }
        case AUTH_CLAER_DATA: {
            return {authorizationToken: null, userName: null}
        }
    }

    return state;
}
