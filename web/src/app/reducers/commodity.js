import {
    COMMODITY_ACTIVE_FILTER_ADD,
    COMMODITY_ACTIVE_FILTER_CLEAN,
    COMMODITY_ACTIVE_FILTER_CLEAN_ALL,
    COMMODITY_AVAILABLE_FILTERS,
    COMMODITY_AVAILABLE_FILTERS_CLEAN,
    COMMODITY_FULL_DATA,
    COMMODITY_FULL_DATA_CLEAN,
    COMMODITIES_ALL,
    COMMODITIES_ALL_CLEAN,
    COMMODITIES_RANGE,
    COMMODITIES_RANGE_CLEAN,
    COMMODITIES_TO_PARSE,
    COMMODITIES_TO_PARSE_CLEAN,
    RESPONSES_NEWEST,
    RESPONSES_NEWEST_CLEAN,
} from 'app/consts/actionTypes';

export default function reducer(state = {
    availableFilters: [],
    activeFilters: [],
    commodity: null,
    commoditiesAll: [],
    commoditiesRange: {count: 0, commodities: []},
    commoditiesToParse: [],
    responsesNewest: [],
}, action) {
    switch (action.type) {
        case COMMODITY_ACTIVE_FILTER_ADD: {
            return {
                ...state, 
                activeFilters: state.activeFilters.find(af => af.id === action.payload.id) ? 
                    state.activeFilters.map(af => {
                        return af.id === action.payload.id ? 
                            action.payload : af
                    }) : 
                    [...state.activeFilters, action.payload],
            };
        }
        case COMMODITY_ACTIVE_FILTER_CLEAN: {
            return {
                ...state, 
                activeFilters: state.activeFilters.filter(af => af.id !== action.payload ),
            };
        }
        case COMMODITY_ACTIVE_FILTER_CLEAN_ALL: {
            return {
                ...state, 
                activeFilters: [],
            };
        }
        case COMMODITY_AVAILABLE_FILTERS: {
            return {...state, availableFilters: action.payload};
        }
        case COMMODITY_AVAILABLE_FILTERS_CLEAN: {
            return {...state, availableFilters: []};
        }
        case COMMODITY_FULL_DATA: {
            return {...state, commodity: action.payload};
        }
        case COMMODITY_FULL_DATA_CLEAN: {
            return {...state, commodity: null};
        }
        case COMMODITIES_ALL: {
            return {...state, commoditiesAll: action.payload};
        }
        case COMMODITIES_ALL_CLEAN: {
            return {...state, commoditiesAll: []};
        }
        case COMMODITIES_RANGE: {
            return {...state, commoditiesRange: action.payload};
        }
        case COMMODITIES_RANGE_CLEAN: {
            return {...state, commoditiesRange: {count: 0, commodities: []}};
        }
        case COMMODITIES_TO_PARSE: {
            return {...state, commoditiesToParse: action.payload};
        }
        case COMMODITIES_TO_PARSE_CLEAN: {
            return {...state, commoditiesToParse: []};
        }
        case RESPONSES_NEWEST: {
            return {...state, responsesNewest: action.payload};
        }
        case RESPONSES_NEWEST_CLEAN: {
            return {...state, responsesNewest: []};
        }
    }
    return state;
}
