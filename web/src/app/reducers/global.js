import {
    SET_IS_LOADER_VISIBLE,
    SET_IS_INFO_POPUP,
    SET_IS_INFO_POPUP_CLEAN,
    SET_IS_SELECT_POPUP,
    SET_IS_SELECT_POPUP_CLEAN,
} from 'app/consts/actionTypes';

export default function reducer(state = {
        isLoaderVisible: false,
        infoPopup: {
            isVisible: false,
            text: "",
            onOkHandler: () => {},
        },
        selectPopup: {
            isVisible: false,
            text: "",
            onYesHandler: () => {},
            onNoHandler: () => {},
        }
    }, action) {

    switch (action.type) {
        case SET_IS_LOADER_VISIBLE: {
            return {...state, isLoaderVisible: action.payload}
        }
        case SET_IS_INFO_POPUP: {
            return {...state, infoPopup: action.payload}
        }
        case SET_IS_INFO_POPUP_CLEAN: {
            return {
                ...state, 
                infoPopup: {
                    isVisible: false,
                    text: "",
                    onOkHandler: () => {},
                }
            }
        }
        case SET_IS_SELECT_POPUP: {
            return {...state, selectPopup: action.payload}
        }
        case SET_IS_SELECT_POPUP_CLEAN: {
            return {
                ...state, 
                selectPopup: {
                    isVisible: false,
                    text: "",
                    onYesHandler: () => {},
                    onNoHandler: () => {},
                }
            }
        }
    }

    return state;
}
