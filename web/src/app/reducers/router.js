import {SET_LOCATION_HISTORY} from 'app/consts/actionTypes';

export default function reducer(state = {
    history: [],
}, action) {
    switch (action.type) {
        case SET_LOCATION_HISTORY: {
            const {location} = action.payload;

            return {
                ...state,
                history: [location, ...state.history]
            };
        }
    }

    return state;
}
