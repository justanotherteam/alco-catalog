import {combineReducers} from 'redux';
import {localeReducer} from 'react-localize-redux';
import {reducer as formReducer} from 'redux-form'

import apiRequest from 'app/reducers/apiRequest';
import auth from 'app/reducers/auth';
import category from 'app/reducers/category';
import commodity from 'app/reducers/commodity';
import global from 'app/reducers/global';
import router from 'app/reducers/router';

export default combineReducers({
    apiRequest: apiRequest,
    auth: auth,
    category: category,
    commodity: commodity,
    global: global,
    form: formReducer,
    locale: localeReducer,
    router: router,
})