import * as routeParams from './routeParams';

export const ADMIN_PAGE = '/admin';
export const CATALOG = `/catalog`;
export const CATALOG_PARAMETERIZED = `${CATALOG}/:${routeParams.CATEGORY_ID}`;
export const COMMODITIES_CATEGORY = `/commodities/category`;
export const COMMODITY = `/commodity`;
export const COMMODITY_PARAMETERIZED = `${COMMODITY}/:${routeParams.COMMODITY_ID}`;
export const HOME = `/home`;
export const LOGIN = '/login';
export const LOGOUT = '/logout';
export const ROOT = '/';
export const WILDCARD = '/wildcard';
