export const COUNT_ON_PAGE = 12;
export const NEWEST_COMMODITIES_COUNT = 10;
export const NEWEST_RESPONSES_COUNT = 5;