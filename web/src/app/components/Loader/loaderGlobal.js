import React from 'react';

import Loader from 'app/components/Loader';

const LoaderGlobal = (props) => <Loader
    loaderType='loader--global'
    {...props}
/>;

export default LoaderGlobal;