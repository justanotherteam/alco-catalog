import React from 'react';
import PropTypes from 'prop-types';

function Loader({className, isVisible, loaderType}) {
    const loaderClass = `loader__wrapper_ ${loaderType} ${className}`;

    return (
        isVisible
            ? <div className={loaderClass}>
                <div className='loader'/>
            </div>
            : null
    );
}

Loader.defaultProps = {
    className: '',
};

Loader.propTypes = {
    className: PropTypes.string,
    isVisible: PropTypes.bool,
    loaderType: PropTypes.string,
};

export default Loader;