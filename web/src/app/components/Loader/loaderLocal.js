import React from 'react';

import Loader from 'app/components/Loader';

const LoaderLocal = (props) => <Loader
    loaderType='loader--local'
    {...props}
/>;

export default LoaderLocal;