import React from 'react';

import Loader from 'app/components/Loader';

const LoaderRelative = (props) => <Loader
    loaderType='loader--relative'
    {...props}
/>;

export default LoaderRelative;