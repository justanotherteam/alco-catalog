import React from 'react';
import {connect} from 'react-redux';

export default class Pagination extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
        }
    }

    render() {
        const {count, handler} = this.props;
        const {currentPage} = this.state;
        const buttons = [];
        for(let i = 1; i < count + 1; i++){
            buttons.push(
                <button 
                    className="nav-button" 
                    key={i}
                    onClick={() => {
                        handler(i);
                        this.setState({currentPage: i});
                    }}
                    disabled={currentPage === i}
                >
                    {i}
                </button>
            )
        } 

        return(
            <div className="pagination">
                {buttons}
            </div>
        )
    }
}