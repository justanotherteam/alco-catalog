import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import {hideSelectPopup} from 'app/actions/global';
import {makeSelectSelectPopup} from 'app/selectors/global';


const mapStateToProps = createStructuredSelector({
    popupData: makeSelectSelectPopup(),
}); 

@connect(mapStateToProps)
export default class InfoPopup extends React.PureComponent {
    onYesClick = () => {
        const {dispatch, popupData} = this.props;
        popupData.onYesHandler && popupData.onYesHandler();
        dispatch(hideSelectPopup());
    }    
    
    onNoClick = () => {
        const {dispatch, popupData} = this.props;
        popupData.onNoHandler && popupData.onNoHandler();
        dispatch(hideSelectPopup());
    }
        
    render() {
        const {popupData} = this.props;

        return(
            <div className="popup-container">
                <div className="popup-content">
                    <div className="text">
                        <h2>{popupData.text}</h2>
                    </div>
                    <div className="control">
                        <button className="add-button" onClick={this.onYesClick}>YES</button>
                        <button className="delete-button" onClick={this.onNoClick}>NO</button>
                    </div>
                </div>
            </div>
        )
    }
}