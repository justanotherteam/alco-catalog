import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import {hideInfoPopup} from 'app/actions/global';
import {makeSelectInfoPopup} from 'app/selectors/global';


const mapStateToProps = createStructuredSelector({
    popupData: makeSelectInfoPopup(),
}); 

@connect(mapStateToProps)
export default class InfoPopup extends React.PureComponent {
    onYesClick = () => {
        const {dispatch, popupData} = this.props;
        popupData.onOkHandler && popupData.onOkHandler();
        dispatch(hideInfoPopup());
    }    

    render() {
        const {popupData} = this.props;

        return(
            <div className="popup-container">
                <div className="popup-content">
                    <div className="text">
                        <h2>{popupData.text}</h2>
                    </div>
                    <div className="control">
                        <button className="ok-button" onClick={this.onYesClick}>OK</button>
                    </div>
                </div>
            </div>
        )
    }
}