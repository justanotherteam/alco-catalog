import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import CommodityComponent from 'app/components/CommodityComponent';
import Loader from 'app/components/Loader';
import Pagination from 'app/components/Pagination';
import commodityActions from 'app/actions/commodityActions';
import commodityService from 'app/services/commodityService';
import {makeSelectCurrentCargory} from 'app/selectors/category';
import {makeSelectCommoditiesRange} from 'app/selectors/commodity';
import {COUNT_ON_PAGE} from 'app/consts/global';

const mapStateToProps = createStructuredSelector({
    commoditiesRange: makeSelectCommoditiesRange(),
    currentCargory: makeSelectCurrentCargory(),
}); 

@connect(mapStateToProps)
export default class CommoditiesContainer extends React.PureComponent {
    componentDidMount() {
        const {dispatch, currentCargory} = this.props;
        let data = {
            page: 1,
            count: COUNT_ON_PAGE,
            filters: [],
            categoryId: currentCargory,
        };
        dispatch(commodityActions.cleanCommoditiesRange())
        dispatch(commodityService.getCommodities(data));
    }
    
    componentWillUnmount() {
        const {dispatch} = this.props;
        dispatch(commodityActions.cleanCommoditiesRange());
    }

    onNavigationClick = (page) => {
        const {dispatch, activeFilters, currentCargory} = this.props;
        dispatch(commodityService.getCommodities({
            filters: activeFilters, 
            count: COUNT_ON_PAGE, 
            page,
            categoryId: currentCargory,
        }));
    }

    render() {
        const {commoditiesRange} = this.props;
        
        return commoditiesRange ? 
            <div className="commodities-list-container">
                <div className="commodities-list">{ 
                    commoditiesRange.commodities.map(c => {
                        return(
                            <CommodityComponent
                                id={c.id}
                                key={c.id}
                            />
                        )
                    })}
                </div>
                <Pagination
                    count={Math.ceil(commoditiesRange.count / COUNT_ON_PAGE)}
                    handler={this.onNavigationClick}
                />
            </div> : 
            <Loader isVisible={true}/>
    }
}