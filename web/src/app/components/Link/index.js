import React from 'react';
import PropTypes from 'prop-types';
import {Link as ReactLink, NavLink} from 'react-router-dom';

const Link = (props) => {
    const {activeClassName, className, isExternalLink, value, to, onClick, exact, children} = props;
    const Component = to ? (activeClassName ? NavLink : ReactLink) : 'a';
    const target = isExternalLink ? '_blank' : null;

    return (
        isExternalLink ?
            <a
                className={'link ' + className}
                href={to}
                onClick={onClick}
                target='_blank'
            >
                {children || value}
            </a>
            : <Component
                {...(activeClassName ? {activeClassName} : {})}
                className={'link ' + className}
                exact={exact}
                onClick={onClick}
                target={target}
                to={to}
            >
                {children || value}
            </Component>
    );
};

Link.defaultProps = {
    className: '',
    isExternalLink: false,
};

Link.propTypes = {
    className: PropTypes.string,
    isExternalLink: PropTypes.bool,
    to: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.func]),
    onClick: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default Link;
