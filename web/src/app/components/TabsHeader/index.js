import React from 'react';

export default class TabsHeader extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            headerIndex: props.startIndex,
        };
    }    

    render() {
        const { tabs, onHeaderItemClick } = this.props;

        const tabElements = tabs.map(tab => {
            return (
                <div
                    key={tab.index}
                    onClick={() => {
                        onHeaderItemClick(tab.index);
                        this.setState({headerIndex: tab.index});
                    }}
                    className={`tabs-header__item ${this.state.headerIndex == tab.index ? 'tabs-header__item--active' : ''}`}
                >
                    {tab.title}
                    
                </div>
            )
        });

        return (
            <div className='tabs-header'>
                {tabElements}
            </div>
        );
    }
}