import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import Link from 'app/components/Link';
import {makeSelectAllCategories} from 'app/selectors/category';
import {makeSelectIsLoggedIn} from 'app/selectors/auth';
import * as routePaths from 'app/consts/routePaths';

const mapStateToProps = createStructuredSelector({
    categories: makeSelectAllCategories(),
    isLoggedIn: makeSelectIsLoggedIn(),
}); 

@connect(mapStateToProps)
export default class Header extends React.PureComponent {
    render() {
        const { categories, isLoggedIn } = this.props;

        return(
            <div className="header">
                <div className="header-container header-container-left">
                    <div className="header-item">
                        <Link
                            to={routePaths.HOME}
                            value={"Home"}
                        />
                    </div>
                </div>
                <div className="header-container header-container-middle">
                   {categories.map((category, index) => 
                        <div className="header-item" key={index}>
                            <Link
                                to={`${routePaths.CATALOG}/${category.id}`}
                                value={category.name}
                            />
                        </div>
                    )}
                </div>
                <div className="header-container header-container-right">
                    {isLoggedIn ? [
                        <div className="header-item">
                            <Link
                                to={`${routePaths.ADMIN_PAGE}`}
                                value={"Admin"}
                            />
                        </div>,
                        <div className="header-item">
                            <Link
                                to={`${routePaths.LOGOUT}`}
                                value={"Logout"}
                            />
                        </div>] :
                        <div className="header-item">
                            <Link
                                to={`${routePaths.LOGIN}`}
                                value={"Login"}
                            />
                        </div>
                    }
                </div>
            </div>
        )
    }
}