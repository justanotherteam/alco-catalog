import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import CheckBox from '@material-ui/core/Checkbox';

const mapStateToProps = createStructuredSelector({
}); 

@connect(mapStateToProps)
export default class BoolFilter extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: false,
        }
    }

    render() {
        const {data} = this.props;
        return(
            <div className="bool-filter">
                <CheckBox
                    name={data.name}
                    checked={this.state.isChecked}
                    onChange={() => this.setState({isChecked: !this.state.isChecked})}                
                />
            </div>
        )
    }
}