import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import Loader from 'app/components/Loader';
import BoolFilter from 'app/components/Filters/boolFilter';
import RangeFilter from 'app/components/Filters/rangeFilter';
import ValueFilter from 'app/components/Filters/valueFilter';
import {
    makeSelectAvailableFilters, 
    makeSelectActiveFilters,
} from 'app/selectors/commodity';
import {makeSelectCurrentCargory} from 'app/selectors/category';
import commodityService from 'app/services/commodityService';
import commodityActions from 'app/actions/commodityActions';
import * as filterTypes from 'app/consts/filterTypes';
import {COUNT_ON_PAGE} from 'app/consts/global';

const mapStateToProps = createStructuredSelector({
    availableFilters: makeSelectAvailableFilters(),
    activeFilters: makeSelectActiveFilters(),
    currentCargory: makeSelectCurrentCargory(),
}); 

@connect(mapStateToProps)
export default class Filters extends React.PureComponent {
    onFilterClick = () => {
        const {dispatch, activeFilters, currentCargory} = this.props;
        let data = {
            page: 1,
            count: COUNT_ON_PAGE,
            filters: activeFilters,
            categoryId: currentCargory,
        };
        dispatch(commodityActions.cleanCommoditiesRange())
        dispatch(commodityService.getCommodities(data));
    }

    render() {
        const {availableFilters, activeFilters} = this.props;
        if(availableFilters.length < 1)
            return(
                <Loader isVisible={true}/>
            );
        return(
            <div className="filters">
                <h2>Filters: {activeFilters.length}</h2>
                <button onClick={this.onFilterClick}>FILTER</button>
                {availableFilters.map(f => {
                    switch (f.type) {
                        case filterTypes.BOOL: {
                            return <BoolFilter key={f.id} data={f}/>
                        }
                        case filterTypes.RANGE: {
                            return <RangeFilter key={f.id} data={f}/>
                        }
                        case filterTypes.VALUE: {
                            return <ValueFilter key={f.id} data={f}/>
                        }
                    }
                    return null;
                })}
                <button onClick={this.onFilterClick}>FILTER</button>
            </div>
        )
    }
}