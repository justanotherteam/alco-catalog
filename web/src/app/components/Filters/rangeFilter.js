import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';

import commodityActions from 'app/actions/commodityActions';
import {makeSelectActiveFilterById} from 'app/selectors/commodity';

const mapStateToProps = (state, ownProps) => createStructuredSelector({
    activeFilter: makeSelectActiveFilterById(ownProps.id),
}); 

@connect(mapStateToProps)
export default class RangeFilter extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            value: props.activeFilter ? 
                props.activeFilter.value :
                [props.data.minAvailableValue, props.data.maxAvailableValue],
            isActive: props.activeFilter,
        };
    }
        
    onSliderChange = value => {
        const {data, dispatch} = this.props;
        
        dispatch(commodityActions.addActiveFilter({
            id: data.id,
            lowestValue: value[0],
            higherValue: value[1],
        }));
        this.setState({isActive: true, value})
    };
        
    handleDel = () => {
        const {data, dispatch} = this.props;
        
        dispatch(commodityActions.cleanActiveFilter(data.id));
        this.setState({
            isActive: false,
            value: [data.minAvailableValue, data.maxAvailableValue],
        })
    };
        
    render() {
        const {data} = this.props;
        const {isActive} = this.state;
        return data.minAvailableValue != data.maxAvailableValue && 
        (
            <div className="range-filter">
                <h2>{data.name}</h2>
                    <label>{this.state.value[0].toFixed(2)} - {this.state.value[1].toFixed(2)}</label>
                {
                    isActive && 
                    <button type="button" onClick={this.handleDel}>
                        DEL
                    </button>
                }
                <br />
                <br />
                <Range 
                    allowCross={false} 
                    min={this.props.data.minAvailableValue}
                    max={this.props.data.maxAvailableValue}
                    step={(this.props.data.maxAvailableValue - this.props.data.minAvailableValue)/10}
                    value={this.state.value} 
                    onChange={this.onSliderChange} 
                    trackStyle={[{ backgroundColor: 'red' }]}
                    handleStyle={[{ borderColor: "red"}, { borderColor: "red"}]}
                />
            </div>
        );
    }
}