import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import CheckBox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';

import commodityActions from 'app/actions/commodityActions';
import {makeSelectActiveFilterById} from 'app/selectors/commodity';

const mapStateToProps = (state, ownProps) => createStructuredSelector({
    activeFilter: makeSelectActiveFilterById(ownProps.id),
}); 

@connect(mapStateToProps)
export default class ValueFilter extends React.PureComponent {
    constructor(props) {
        super(props);
        
        let values = props.activeFilter ? 
            props.activeFilter.values : 
            props.data.availableValues;
        
        this.state = {
            values: values.map((av, i) => ({id: i, isChecked: false, value: av})),
            isActive: props.activeFilter
        }
    }

    onChange = (index) => {
        const {data, dispatch} = this.props;
        
        this.setState((state) => {
            const newValues = state.values.map(val => {
                return val.id !== index ? val : {...val, isChecked: !val.isChecked}
            });
            const values = newValues.filter(v => v.isChecked);
            
            dispatch(commodityActions.addActiveFilter({
                id: data.id,
                values: values.map(v => v.value),
            }));
            
            return {
                values: newValues,
                isActive: true,
            }        
        });
    };
        
    handleDel = () => {
        const {data, dispatch} = this.props;
        
        dispatch(commodityActions.cleanActiveFilter(data.id));
        this.setState({
            isActive: false,
            values: data.availableValues.map((av, i) => ({id: i, isChecked: false, value: av})),
        })
    };
        
    render() {
        const {data} = this.props;
        const {isActive} = this.state;

        return data.availableValues.length > 0 &&
        (
            <div className="value-filter">
                <h2>{data.name}</h2>
                {
                    isActive && 
                    <button type="button" onClick={this.handleDel}>
                        DEL
                    </button>
                }
                <FormGroup>
                    {this.state.values.map((v, i) => (
                    <FormControlLabel
                        key={i}
                        control={<CheckBox
                            checked={v.isChecked}
                            onChange={() => this.onChange(i)}                
                        />}
                        label={data.availableValues[i]}
                    />
                ))}
                </FormGroup>
            </div>
        );
    }
}