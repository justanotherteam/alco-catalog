import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import 'rc-slider/assets/index.css';

import {makeSelectCommodityById} from 'app/selectors/commodity';
import {COMMODITY} from 'app/consts/routePaths';
import Link from 'app/components/Link';

const mapStateToProps = (state, ownProps) => createStructuredSelector({
    commodity: makeSelectCommodityById(ownProps.id),
}); 

@connect(mapStateToProps)
export default class CommodityComponent extends React.PureComponent {
    render() {
        const {commodity} = this.props;
        return (
            <Link
                to={`${COMMODITY}/${commodity.id}`}
                className="commodity-component"
            >
                <div>
                    <div className="commodity-image">
                        <img src={commodity.pictureURL}></img>
                    </div>
                    <h2 className="commodity-name">{commodity.name}</h2>
                    <div className="user-data">
                        <div className="average-stars">{"Starts: " + commodity.averageStars}</div>
                        <div className="responses-count">{"Responses: " + commodity.responsesCount}</div>
                    </div>
                    <h2 className="price">{commodity.price + " ₴"}</h2>
                </div>
            </Link>
        );
    }
}