import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import reducer from 'app/reducers';

export default function configureStore() {
    const middlewares = [promise(), thunk];

    if (process.env.NODE_ENV === `development`) {
        const {logger} = require(`redux-logger`);

        middlewares.push(logger);
    }

    const middleware = applyMiddleware(...middlewares);
    return createStore(reducer, middleware);
}