import {createSelector} from 'reselect';

const selectCategories = (state) => state.category;

const makeSelectAllCategories = () => createSelector(
    selectCategories,
    (state) => state.categories
);

const makeSelectCurrentCargory = () => createSelector(
    selectCategories,
    (state) => state.currentCategory
);


export {
    makeSelectAllCategories,
    makeSelectCurrentCargory,
};
