import {createSelector} from 'reselect';

const selectCommodity = (state) => state.commodity;

const makeSelectFullCommodityData = () => createSelector(
    selectCommodity,
    (state) => state.commodity
);

const makeSelectAvailableFilters = () => createSelector(
    selectCommodity,
    (state) => state.availableFilters
);

const makeSelectActiveFilters = () => createSelector(
    selectCommodity,
    (state) => state.activeFilters
);

const makeSelectActiveFilterById = (id) => createSelector(
    selectCommodity,
    (state) => state.activeFilters.find(af => af.id === id)
);

const makeSelectCommoditiesAll = () => createSelector(
    selectCommodity,
    (state) => state.commoditiesAll
);

const makeSelectCommoditiesNewest = () => createSelector(
    selectCommodity,
    (state) => state.commoditiesRange.commodities
);

const makeSelectCommoditiesRange = () => createSelector(
    selectCommodity,
    (state) => state.commoditiesRange
);

const makeSelectCommoditiesToParse = () => createSelector(
    selectCommodity,
    (state) => state.commoditiesToParse
);

const makeSelectCommodityById = (id) => createSelector(
    selectCommodity,
    (state) => state.commoditiesRange.commodities.find(c => c.id === id)
);

const makeSelectResponsesNewest = () => createSelector(
    selectCommodity,
    (state) => state.responsesNewest
);

export {
    makeSelectFullCommodityData,
    makeSelectAvailableFilters,
    makeSelectActiveFilters,
    makeSelectActiveFilterById,
    makeSelectCommoditiesAll,
    makeSelectCommoditiesNewest,
    makeSelectCommoditiesRange,
    makeSelectCommoditiesToParse,
    makeSelectCommodityById,
    makeSelectResponsesNewest,
};
