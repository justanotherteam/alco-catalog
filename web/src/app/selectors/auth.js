import {createSelector} from 'reselect';

const selectAuth = (state) => state.auth;

const makeSelectAuthorizationToken = () => createSelector(
    selectAuth,
    (authState) => authState.authorizationToken
);

const makeSelectUserName = () => createSelector(
    selectAuth,
    (authState) => authState.userName
);

const makeSelectIsLoggedIn = () => createSelector(
    makeSelectAuthorizationToken(),
    (authorizationToken) => !!authorizationToken
);

export {
    makeSelectAuthorizationToken,
    makeSelectIsLoggedIn,
    makeSelectUserName,
    selectAuth,
};
