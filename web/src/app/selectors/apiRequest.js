import {createSelector} from 'reselect';

const selectState = (state) => state.apiRequest;

const makeSelectAPIRequest = (requestType) => createSelector (
    selectState,
    (state) => {
        if (!state.requests[requestType]) {
            state.requests[requestType] =
                {
                    data: null,
                    isFetching: false,
                    isFetched: false,
                    error: null,
                }
        }
        return state.requests[requestType];
    }
);

const makeSelectAPIData = (requestType) => createSelector(
    makeSelectAPIRequest(requestType),
    (state) => (state.data)
);

export {
    makeSelectAPIData,
    makeSelectAPIRequest,
};