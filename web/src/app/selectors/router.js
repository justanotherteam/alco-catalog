import {createSelector} from 'reselect';

const select = (state) => state.router;

const makeSelectLocationHistory = () => createSelector(
    select,
    (routerState) => routerState.history
);

export {
    makeSelectLocationHistory,
};