import {createSelector} from 'reselect';

const selectGlobal = (state) => state.global;

const makeIsLoaderVisible = () => createSelector(
    selectGlobal,
    (globalState) => globalState.isLoaderVisible
);

const makeSelectInfoPopup = () => createSelector(
    selectGlobal,
    (globalState) => globalState.infoPopup
);

const makeSelectInfoPopupIsVisible = () => createSelector(
    selectGlobal,
    (globalState) => globalState.infoPopup.isVisible
);

const makeSelectSelectPopup = () => createSelector(
    selectGlobal,
    (globalState) => globalState.selectPopup
);

const makeSelectSelectPopupIsVisible = () => createSelector(
    selectGlobal,
    (globalState) => globalState.selectPopup.isVisible
);

export {
    makeIsLoaderVisible,
    makeSelectInfoPopup,
    makeSelectInfoPopupIsVisible,
    makeSelectSelectPopup,
    makeSelectSelectPopupIsVisible,
};