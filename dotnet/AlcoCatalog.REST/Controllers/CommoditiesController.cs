﻿using AlcoCatalogBL.Services;
using AlcoCatalogBL.Services.Models;
using AlcoCatalogBL.Services.Models.Commodities;
using AlcoCatalogBL.Services.Models.Commodity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace AlcoCatalog.REST.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CommoditiesController : ControllerBase
    {
        private CommodityService service = new CommodityService();


        [HttpGet("filters/{categoryId}")]
        public IActionResult GetFilters(Guid categoryId)
        {
            try
            {
                return new ObjectResult(service.GetFilters(categoryId));
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }

        [HttpOptions()]
        [Route("{commodityId}")]
        [Route("GetRange/")]
        [Route("all/")]
        [Route("toParse/")]
        [Route("add/")]
        [Route("delete/{id}")]
        [Route("newest/{count}")]
        [Route("responses/newest/{count}")]
        [Route("filters/{categoryId}")]
        public IActionResult Options(Guid id = new Guid())
        {
            return StatusCode(200);
        }



        [HttpPost("GetRange/")]
        public IActionResult GetRange([FromBody]CommodityGetRangeRequest data)
        {
            try
            {
               return new ObjectResult(service.GetRange(data));
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }

        [HttpGet("newest/{count}")]
        public IActionResult GetNewestCommodities(int count)
        {
            try
            {
                return new ObjectResult(service.GetNewCommodities(count));
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }

        [HttpGet("responses/newest/{count}")]
        public IActionResult GetNewestResponses(int count)
        {
            try
            {
                return new ObjectResult(service.GetNewResponse(count));
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }

        [HttpGet("{commodityId}")]
        public IActionResult GetCommodity(Guid commodityId)
        {
            try
            {
                return new ObjectResult(service.GetCommodity(commodityId));
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }

        [Authorize]
        [HttpGet("all/")]
        public IActionResult GetAllCommodities()
        {
            try
            {
                return new ObjectResult(service.GetAll());
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }

        [Authorize]
        [HttpGet("toParse/")]
        public IActionResult GetCommoditiesToParse()
        {
            try
            {
                return new ObjectResult(service.GetSendedToParse());
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }

        [Authorize]
        [HttpPost("add/")]
        public IActionResult AddCommodity([FromBody]AddCommodityRequestModel model)
        {
            try
            {
                service.AddCommodityCodeForParsing(model.Code, model.CategoryId);
                return new OkResult();
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }

        }

        [Authorize]
        [HttpDelete("delete/{id}")]
        public IActionResult DeleteCommodity(Guid id)
        {
            try
            {
                service.DeleteCommodity(id);
                return new OkResult();
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }
    }
}
