﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlcoCatalogBL.Services.Interfaces;
using AlcoCatalogBL.Services.Models;
using AlcoCatalogBL.Services.Models.Category;
using AlcoCatalogBL.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AlcoCatalog.REST.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private ICategoryService _service;
        public CategoriesController()
        {
            _service = IoCContainer.Resolve<ICategoryService>();
        }


        [HttpOptions()]
        [Route("")]
        [Route("add/{name}")]
        [Route("delete/{id}")]
        public IActionResult Options(Guid id = new Guid(), string name = "")
        {
            return StatusCode(200);
        }

        [HttpGet]
        public IActionResult GetCategories()
        {
            try
            {
                return new ObjectResult(_service.Get());
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }

        [Authorize]
        [HttpPut("add/{name}")]
        public IActionResult AddCategory(string name)
        {
            try
            {
                return new ObjectResult(_service.Add(name));
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }

        [Authorize]
        [HttpDelete("delete/{id}")]
        public IActionResult DelCategory(Guid id)
        {
            try
            {
                _service.Delete(id);
                return new OkResult();
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new ObjectResult(e.Message);
            }
        }
    }
}