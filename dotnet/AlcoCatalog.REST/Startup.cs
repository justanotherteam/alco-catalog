using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Services;
using AlcoCatalogBL.Services.Interfaces;
using AlcoCatalogBL.Utils;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace AlcoCatalog.REST
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                                // ��������, ����� �� �������������� �������� ��� ��������� ������
                                ValidateIssuer = true,
                                // ������, �������������� ��������
                                ValidIssuer = AuthOptions.ISSUER,

                                // ����� �� �������������� ����������� ������
                                ValidateAudience = true,
                                // ��������� ����������� ������
                                ValidAudience = AuthOptions.AUDIENCE,
                                // ����� �� �������������� ����� �������������
                                ValidateLifetime = true,

                                // ��������� ����� ������������
                                IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                                // ��������� ����� ������������
                                ValidateIssuerSigningKey = true,
                    };
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            IoCContainer.Register<ICategoryDataHandler, CategoryRepository>();
            IoCContainer.Register<ICategoryService, CategoryService>();
            IoCContainer.Register<ICommodityDataHandler, CommodityRepository>();
            IoCContainer.Register<IAccountDataHandler, AccountRepository>();
            IoCContainer.Register<ICharacteristicDataHandler, CharacteristicRepository>();
            IoCContainer.Register<DbContext, AppDbContext>(new AppDbContextFactory().GetDbContextOptionsBuilder().Options);
            DataCreator.Initial(IoCContainer.Resolve<DbContext>());

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                context.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
                context.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, Accept, Origin, Authorization");
                await next();
            });


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();


            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
