﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils.Filters;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NUnitTest
{
    [TestFixture]
    class CharacteristicRepositoryTests : IDisposable
    {
        private AppDbContext _dbContext;
        private CharacteristicRepository _repository;

        public CharacteristicRepositoryTests()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server=localhost;Database=alco_catalog_test;Trusted_Connection=False;User Id=sa;Password=Dima1234;");
            _dbContext = new AppDbContext(optionsBuilder.Options);
            _repository = new CharacteristicRepository(_dbContext);
        }


        [Test]
        public void GetAll_ShouldReturnCount_9()
        {
            //arrange
            var count = 9;

            //act
            var resultCount = _repository.GetAll().ToList().Count;

            //assert
            Assert.AreEqual(count, resultCount);
        }

        [Test]
        public void GetAvailableValues_ShouldReturn_List_1_2()
        {
            //arrange
            var characteristicId = Constants.Characteristics.Endurance;
            var values = new List<string>() { "1", "2" };

            //act
            var resultValues = _repository.GetAvailableValues(characteristicId);

            //assert
            Assert.IsTrue(Utils<string>.CompareLists(values, resultValues));
        }

        [Test]
        public void GetCharacteristicValue_ShouldReturn_1()
        {
            //arrange
            var characteristicId = Constants.Characteristics.Endurance;
            var commodityId = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F6");
            var value = "1";

            //act
            var resultValues = _repository.GetCharacteristicValue(commodityId, characteristicId);

            //assert
            Assert.AreEqual(value, resultValues);
        }

        [Test]
        public void GetCharacteristicValue_ShouldReturn_EmptyString()
        {
            //arrange
            var characteristicId = Constants.Characteristics.Endurance;
            var commodityId = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F8");

            //act
            var resultValue = _repository.GetCharacteristicValue(commodityId, characteristicId);

            //assert
            Assert.AreEqual(string.Empty, resultValue);
        }

        [Test]
        public void UpdateCharactersticValue_ShouldReturn_EmptyString()
        {
            //arrange
            var characteristicId = Constants.Characteristics.Endurance;
            var commodityId = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F6");
            var value = "50";
            var oldValue = _dbContext.CommodityCharacteristics.First(cc => cc.CommodityId == commodityId && cc.CharacteristicId == characteristicId).Value;

            //act
            _repository.UpdateCharactersticValue(characteristicId, commodityId, value);
            var newValue = _dbContext.CommodityCharacteristics.First(cc => cc.CommodityId == commodityId && cc.CharacteristicId == characteristicId);

            //assert
            Assert.AreEqual(value, newValue.Value);

            //after
            newValue.Value = oldValue;
            _dbContext.SaveChanges();
        }

        [Test]
        public void GetCharacteristicName_ShouldReturn_Colour()
        {
            //arrange
            var characteristicId = Constants.Characteristics.Colour;
            var name = "Colour";

            //act
            var resultName = _repository.GetCharacteristicName(characteristicId);

            //assert
            Assert.AreEqual(name, resultName);
        }

        [Test]
        public void GetCharacteristicFilterType_ShouldReturnFilterWithType_Range()
        {
            //arrange
            var characteristicId = Constants.Characteristics.AlcoholPercent;
            var name = "Range";

            //act
            var resultName = _repository.GetCharacteristicFilterType(characteristicId).Name;

            //assert
            Assert.AreEqual(name, resultName);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
