﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NUnitTest
{
    public static class Utils<T>
    {
        public static bool CompareLists(List<T> list1, List<T> list2)
        {
            var firstNotSecond = list1.Except(list2).ToList();
            var secondNotFirst = list2.Except(list1).ToList();
            return !firstNotSecond.Any() && !secondNotFirst.Any();
        }
    }
}
