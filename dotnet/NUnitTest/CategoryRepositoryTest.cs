﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils.Filters;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NUnitTest
{
    [TestFixture]
    class CategoryRepositoryTests : IDisposable
    {
        private AppDbContext _dbContext;
        private CategoryRepository _repository;

        public CategoryRepositoryTests()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server=localhost;Database=alco_catalog_test;Trusted_Connection=False;User Id=sa;Password=Dima1234;");
            _dbContext = new AppDbContext(optionsBuilder.Options);
            _repository = new CategoryRepository(_dbContext);
        }


        [Test]
        public void GetAll_ShouldReturnCount_3()
        {
            //arrange
            var count = 3;

            //act
            var resultCount = _repository.GetAll().ToList().Count;

            //assert
            Assert.AreEqual(count, resultCount);
        }

        [Test]
        public void Add_ShouldAddCategory()
        {
            //arrange
            var name = "test";

            //act
            var resultId = _repository.Add(name);
            var resultCategory = _dbContext.Categories.First(c => c.Id == resultId);

            //assert
            Assert.AreEqual(name, resultCategory.Name);

            //after
            _dbContext.Categories.Remove(resultCategory);
        }

        [Test]
        public void Add_ShouldThrowException_Category_already_exist()
        {
            //arrange
            var name = "Vine";

            //act

            //assert
            var ex = Assert.Throws<CategoryException>(() => _repository.Add(name));
            Assert.That(ex.Message, Is.EqualTo($"Add category error. Category with name = {name} already exist"));
        }

        [Test]
        public void Add_ShouldThrowException_Empty_name()
        {
            //arrange
            var name = "";

            //act

            //assert
            var ex = Assert.Throws<CategoryException>(() => _repository.Add(name));
            Assert.That(ex.Message, Is.EqualTo("Add category error. Empty name"));
        }


        [Test]
        public void Delete_ShouldDeleteCategory()
        {
            //arrange
            var name = "test";
            var id = Guid.NewGuid();
            _dbContext.Categories.Add(new Category() { Id = id, Name = name });
            _dbContext.SaveChanges();

            //act
            _repository.Delete(id);

            //assert
            Assert.IsFalse(_dbContext.Categories.Any(c => c.Id == id));
        }

        [Test]
        public void GetOne_ShouldThrowException_Category_does_not_exist()
        {
            //arrange
            var id = new Guid("F9797BC5-D271-410C-05F6-28D7FA73C499");

            //act

            //assert
            var ex = Assert.Throws<CategoryException>(() => _repository.GetOne(id));
            Assert.That(ex.Message, Is.EqualTo($"Get category error. Category with id = {id} does not exist"));
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
