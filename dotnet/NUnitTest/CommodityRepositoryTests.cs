﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils.Filters;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NUnitTest
{
    [TestFixture]
    class CommodityRepositoryTests : IDisposable
    {
        private AppDbContext _dbContext;
        private CommodityRepository _repository;

        public CommodityRepositoryTests()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server=localhost;Database=alco_catalog_test;Trusted_Connection=False;User Id=sa;Password=Dima1234;");
            _dbContext = new AppDbContext(optionsBuilder.Options);
            _repository = new CommodityRepository(_dbContext);
        }


        [Test]
        public void GetAll_ShouldReturnCount_4()
        {
            //arrange
            var count = 4;

            //act
            var resultCount = _repository.GetAll().Count();

            //assert
            Assert.AreEqual(count, resultCount);
        }

        [Test]
        public void GetCategoryId_ShouldReturn_GuidEmpty()
        {
            //arrange
            var commodityGuid = Guid.Empty;
            var categoryId = Guid.Empty;
            //act
            var categoryIdResult = _repository.GetCategoryId(commodityGuid);

            //assert
            Assert.AreEqual(categoryId, categoryIdResult);
        }

        [Test]
        public void GetCommodity_ShouldReturnCommodityWithCode_П1()
        {
            //arrange
            var code = "П1";
            //act
            var codeResult = _repository.GetCommodity(code).Code;

            //assert
            Assert.AreEqual(code, codeResult);
        }

        [Test]
        public void GetCommodity_WithId_ShouldReturnCommodityWithCode_П2()
        {
            //arrange
            var code = "П2";
            var id = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F7");
            //act
            var codeResult = _repository.GetCommodity(id).Code;

            //assert
            Assert.AreEqual(code, codeResult);
        }

        [Test]
        public void GetOne_Id_ShouldReturnCommodityWithCode_П1()
        {
            //arrange
            var code = "П1";
            var id = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F8");
            //act
            var codeResult = _repository.GetOne(id).Code;

            //assert
            Assert.AreEqual(code, codeResult);
        }

        [Test]
        public void GetRange_WithCategoryVine_ShouldReturnCommoditiesCount_2()
        {
            //arrange
            var count = 2;
            var categoryId = Constants.Categories.Vine;
            //act
            var resultCount = _repository.GetRange(0, 10, new List<AbstractFilter>(), categoryId).Count;

            //assert
            Assert.AreEqual(count, resultCount);
        }

        [Test]
        public void GetRange_WithRangeFilter_ShouldReturnCommoditiesCount_1()
        {
            //arrange
            var count = 1;
            var categoryId = Constants.Categories.General;
            var characteristicId = Constants.Characteristics.AlcoholPercent;
            var charRepo = new CharacteristicRepository(_dbContext);

            var filters = new List<AbstractFilter>() { new RangeFilter(charRepo, characteristicId, 0, 0.14) };
            //act
            var resultCount = _repository.GetRange(0, 1, filters, categoryId).Count;

            //assert
            Assert.AreEqual(count, resultCount);
        }

        [Test]
        public void GetCount_WithCategoryGeneral_ShouldReturn_4()
        {
            //arrange
            var count = 4;
            var categoryId = Constants.Categories.General;

            //act
            var resultCount = _repository.GetCount(new List<AbstractFilter>(), categoryId);

            //assert
            Assert.AreEqual(count, resultCount);
        }

        [Test]
        public void AddCodeForParsing_ShouldAddOneCommodity_WithCateegoryVine_AndCode_V3()
        {
            //arrange
            var count = _dbContext.CommoditiesForParsing.Count();
            var categoryId = Constants.Categories.Vine;
            var code = "V3";

            //act
            var commodityId = _repository.AddCodeForParsing(code, categoryId);
            var addedCommodity = _dbContext.CommoditiesForParsing.FirstOrDefault(c => c.Id == commodityId);

            //assert
            Assert.AreEqual(code, addedCommodity.Code);
            Assert.AreEqual(categoryId, addedCommodity.CategoryId);
            Assert.AreEqual(count + 1, _dbContext.CommoditiesForParsing.Count());

            //after
            _dbContext.CommoditiesForParsing.Remove(addedCommodity);
            _dbContext.SaveChanges();
        }

        [Test]
        public void AddCodeForParsing_ShouldThrowException()
        {
            //arrange
            var categoryId = Constants.Categories.Vine;
            var code = "В2";

            //act

            //assert
            var ex = Assert.Throws<CommodityAlreadyExistException>(() => _repository.AddCodeForParsing(code, categoryId));
            Assert.That(ex.Message, Is.EqualTo($"Commodity with Code = {code} already exist"));
        }

        [Test]
        public void GetBusinessNetworkForCommodity_ShouldReturnCommodityWithPrice_14()
        {
            //arrange
            var price = 14;
            var commodityId = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F8");
            var businessNetworkId = Constants.BusinessNetworks.Rozetka;

            //act
            var resultPrice = _repository.GetBusinessNetworkForCommodity(commodityId, businessNetworkId).Price;

            //assert
            Assert.AreEqual(price, resultPrice);
        }

        [Test]
        public void GetBusinessNetworkForCommodity_ShouldReturn_Null()
        {
            //arrange
            var commodityId = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F9");
            var businessNetworkId = Constants.BusinessNetworks.Rozetka;

            //act
            var result = _repository.GetBusinessNetworkForCommodity(commodityId, businessNetworkId);

            //assert
            Assert.IsNull(result);
        }

        [Test]
        public void GetResponsesCount_ShouldReturn_2()
        {
            //arrange
            var count = 2;
            var commodityId = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F8");

            //act
            var resultCount = _repository.GetResponsesCount(commodityId);

            //assert
            Assert.AreEqual(count, resultCount);
        }

        [Test]
        public void GetAverageStars_ShouldReturn_4()
        {
            //arrange
            var average = 4;
            var commodityId = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F8");

            //act
            var resultAverage = _repository.GetAverageStars(commodityId);

            //assert
            Assert.AreEqual(average, resultAverage);
        }

        [Test]
        public void SetCommodityData_ShouldChangeCodeFor_p1000()
        {
            //arrange
            var newCode = "p1000";
            var commodityId = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F7");
            var oldCommodity = _dbContext.Commodities.First(c => c.Id == commodityId);
            var newCommodity = new Commodity() { Id = commodityId, Code = newCode, CategoryId = Constants.Categories.Bear };

            //act
            _repository.SetCommodityData(newCommodity);
            newCommodity = _dbContext.Commodities.First(c => c.Id == commodityId);

            //assert
            Assert.AreEqual(newCommodity.Code, newCode);

            //after
            _dbContext.Commodities.Remove(newCommodity);
            _dbContext.Commodities.Add(oldCommodity);
            _dbContext.SaveChanges();
        }

        [Test]
        public void SetCommodityData_CountShouldBe_5()
        {
            //arrange
            var count = 5;
            var commodityId = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F9");
            var newCommodity = new Commodity() { Id = commodityId, CategoryId = Constants.Categories.Bear };

            //act
            _repository.SetCommodityData(newCommodity);
            var resultCount = _dbContext.Commodities.Count();

            //assert
            Assert.AreEqual(count, resultCount);

            //after
            _dbContext.Commodities.Remove(newCommodity);
            _dbContext.SaveChanges();
        }

        [Test]
        public void GetBusinessNetwork_GuidEmpty_ShouldThrow_BusinessNetworkNotFoundException()
        {
            //arrange
            var id = Guid.Empty;
            //act

            //assert
            var ex = Assert.Throws<BusinessNetworkNotFoundException>(() => _repository.GetBusinessNetwork(id));
            Assert.That(ex.Message, Is.EqualTo($"BusinessNetwork with Id = {id.ToString()} not found"));
        }

        [Test]
        public void TryDeleteCommodityForParsing_ShouldReturn_False()
        {
            //arrange
            var code = "test";

            //act
            var result = _repository.TryDeleteCommodityForParsing(code);

            //assert
            Assert.IsFalse(result);
        }

        [Test]
        public void TryDeleteCommodityForParsing_ShouldReturn_True()
        {
            //arrange
            var code = "180989516";
            var commodityForParsing = _dbContext.CommoditiesForParsing.First(c => c.Code == code);

            //act
            var result = _repository.TryDeleteCommodityForParsing(code);

            //assert
            Assert.IsTrue(result);

            //after
            _dbContext.CommoditiesForParsing.Add(commodityForParsing);
            _dbContext.SaveChanges();
        }

        [Test]
        public void SetStateForSendForParsing_ShouldSet_True()
        {
            //arrange
            var code = "180989516";

            //act
            _repository.SetStateForSendForParsing(code, true);
            var commodityForParsing = _dbContext.CommoditiesForParsing.First(c => c.Code == code);

            //assert
            Assert.IsTrue(commodityForParsing.IsSendedToParsing);

            //after
            commodityForParsing.IsSendedToParsing = false;
            _dbContext.SaveChanges();
        }

        [Test]
        public void GetNewCommodities_ShouldReturnComCount_3()
        {
            //arrange
            var count = 3;

            //act
            var resultCount = _repository.GetNewCommodities(3).Count;

            //assert
            Assert.AreEqual(count, resultCount);
        }

        [Test]
        public void Delete_ShouldDelete()
        {
            //arrange
            var count = _dbContext.Commodities.Count();
            var id = Guid.NewGuid();
            var categoryId = Constants.Categories.Bear;
            var newCommodity = new Commodity() { Id = id, CategoryId = categoryId };
            _dbContext.Commodities.Add(newCommodity);
            _dbContext.SaveChanges();

            //act
            _repository.Delete(id);
            var newCount = _dbContext.Commodities.Count();

            //assert
            Assert.AreEqual(count, newCount);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
