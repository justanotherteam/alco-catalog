﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Services;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.Utils.Filters;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NUnitTest
{
    [TestFixture]
    class CommodityServiceTest : IDisposable
    {
        private AppDbContext _dbContext;
        private readonly Mock<CommodityRepository> _comRepo;
        private readonly Mock<CharacteristicRepository> _charRepo;
        private readonly CommodityService _serv;

        public CommodityServiceTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server=localhost;Database=alco_catalog_test;Trusted_Connection=False;User Id=sa;Password=Dima1234;");
            _dbContext = new AppDbContext(optionsBuilder.Options);
            _comRepo = new Mock<CommodityRepository>(_dbContext);
            _charRepo = new Mock<CharacteristicRepository>(_dbContext);
            _serv = new CommodityService(_comRepo.Object, _charRepo.Object, _dbContext);
        }

        [Test]
        public void GetCommodity_ShouldReturn_CommodityWithData()
        {
            //arrange
            var id = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F5");
            var picture = "https://img3.zakaz.ua/src.1584529315.ad72436478c_2020-03-19_Aliona/src.1584529315.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg";
            var code = "В2";
            var description = "Супер смачне вино";
            //act
            var resultCommudity = _serv.GetCommodity(id);

            //assert
            Assert.AreEqual(resultCommudity.PictureURL, picture);
            Assert.AreEqual(resultCommudity.Code, code);
            Assert.AreEqual(resultCommudity.Description, description);
        }

        [Test]
        public void GetCommodity_GuidEmpty_ShouldThrow_Commodity_not_found()
        {
            //arrange
            var id = new Guid("A5D7D2FC-D4D8-409B-9D44-08D8057066F5");

            //act
            
            //assert
            var ex = Assert.Throws<CommodityNotFoundException>(() => _serv.GetCommodity(id));
            Assert.That(ex.Message, Is.EqualTo($"Commodity with Id = {id.ToString()} not found"));
        }

        [Test]
        public void GetFilters_ShouldReturnList_FirstNameIsEqual_AlcoholPercent()
        {
            //arrange
            var categoryId = Constants.Categories.General;
            var name = "CountInPack";
            var type = "Range";
            var characteristicId = Constants.Characteristics.CountInPack;
            var serv = new CommodityService(_comRepo.Object, new CharacteristicRepository(_dbContext), _dbContext);

            //act
            var resultList = serv.GetFilters(categoryId);

            //assert
            Assert.AreEqual(name, resultList.First().Name);
            Assert.AreEqual(type, resultList.First().Type);
            Assert.AreEqual(characteristicId, resultList.First().CharacteristicId);
        }

        [Test]
        public void GetFilters_ShouldReturnList_FirstNameIsEqual_AlcoholPercent_2()
        {
            //arrange
            var categoryId = Constants.Categories.General;
            var name = "Kind";
            var type = "Value";
            var characteristicId = Constants.Characteristics.Kind;
            var serv = new CommodityService(_comRepo.Object, new CharacteristicRepository(_dbContext), _dbContext);

            //act
            var result = serv.GetFilters(categoryId)[1];

            //assert
            Assert.AreEqual(name, result.Name);
            Assert.AreEqual(type, result.Type);
            Assert.AreEqual(characteristicId, result.CharacteristicId);
        }

        [Test]
        public void GetRange_ShouldReturnList_FirstNameIsEqual_AlcoholPercent_2()
        {
            //arrange
            var categoryId = Constants.Categories.General;
            var name = "Kind";
            var type = "Value";
            var characteristicId = Constants.Characteristics.Kind;
            var serv = new CommodityService(_comRepo.Object, new CharacteristicRepository(_dbContext), _dbContext);

            //act
            var result = serv.GetFilters(categoryId)[1];

            //assert
            Assert.AreEqual(name, result.Name);
            Assert.AreEqual(type, result.Type);
            Assert.AreEqual(characteristicId, result.CharacteristicId);
        }

        //[Test]
        //public void GetCategories_count_shouldBe_moreThan0()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    commodityRepMock.Setup(r => r.GetCategories()).Returns(GetCategories());
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);

        //    //act
        //    var resultCommudity = serv.GetCategories();

        //    //assert
        //    Assert.Greater(resultCommudity.Count, 0);
        //}

        //[Test]
        //public void GetRange_ModelWithRangeFilterVMMax10Min5InList_ShouldReturn_CommodityWithCodeП1()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var characterisicName = "AlcoholPercent";
        //    var max = 10;
        //    var min = 5;
        //    var commodityId = new Guid("44ABC8C8-E95F-4EE2-7F62-08D800E910E7");
        //    var characteristicId = Guid.NewGuid();
        //    var inputModel = new CommodityListVM
        //    {
        //        Filters = new List<AbstractFilterVM>
        //        {
        //            new RangeFilterVM
        //            {
        //                Characteristic = characterisicName,
        //                HigherValue = max,
        //                LowestValue = min,
        //                IsActive = true,
        //                Name = characterisicName
        //            }
        //        }
        //    };
        //    characteristicRepMock.Setup(c => c.GetFilters(Guid.Empty)).Returns(new List<FilterModel>
        //    {
        //        new FilterModel
        //        {
        //            Name = characterisicName,
        //            CharacteristicId = characteristicId,
        //            Characteristic = new Characteristic { Name = characterisicName, DataType = "DOUBLE" },
        //            FilterType = new FilterTypeModel { Name = "RangeFilter" }
        //        }
        //    });
        //    characteristicRepMock.Setup(c => c.GetCharacteristicId(characterisicName, Guid.Empty))
        //        .Returns(characteristicId);
        //    characteristicRepMock.Setup(c => c.GetCharacteristicValue(commodityId, characteristicId))
        //        .Returns(commodityCharacteristics.First(ch => ch.Commodity.Id == commodityId).Value);
        //    characteristicRepMock.Setup(c => c.GetCharacteristicValueDateType(characteristicId))
        //        .Returns("DOUBLE");

        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    //act
        //    var resultFilters = serv.CreateFilters(ref inputModel);
        //    var filtredCommodities = commodities.Where(c => c.Code == "П1").Where(c => resultFilters.All(f => f.Filter(c))).ToList();
        //    commodityRepMock.Setup(c => c.GetRange(10, 10, Guid.Empty, It.IsAny<List<AbstractFilter>>(), new CommoditySorter(OrderType.AlcoholPercent, OrderVector.Asc)))
        //        .Returns(filtredCommodities);
        //    commodityRepMock.Setup(c => c.GetBusinessNetworkForCommodity(commodityId, Constants.BusinessNetworks.Rozetka))
        //        .Returns(new CommodityInBusinessNetwork());
        //    commodityRepMock.Setup(c => c.GetAverageStars(commodityId))
        //        .Returns(0);
        //    commodityRepMock.Setup(c => c.GetResponsesCount(commodityId))
        //        .Returns(0);

        //    serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    serv.GetRange(1, 10, ref inputModel, new CommoditySorter(OrderType.AlcoholPercent, OrderVector.Asc));
        //    var resultCommodity = inputModel.Commodities.First();
        //    //assert
        //    Assert.True(resultCommodity.Code == "П1");
        //}

        //[Test]
        //public void CreateFilters_ModelWithEnptyFilterList_ShouldAddtoModel_BoolFilterWithNameBF()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var categoryId = Guid.NewGuid();
        //    var characterisicName = "Ch";
        //    var inputModel = new CommodityListVM
        //    {
        //        CategoryId = categoryId
        //    };
        //    characteristicRepMock.Setup(c => c.GetFilters(categoryId)).Returns(new List<FilterModel>
        //    {
        //        new FilterModel
        //        {
        //            Name = "BF",
        //            CharacteristicId = Guid.NewGuid(),
        //            Characteristic = new Characteristic { Name = characterisicName },
        //            FilterType = new FilterTypeModel { Name = "BoolFilter" }
        //        }
        //    });
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    //act
        //    serv.CreateFilters(ref inputModel);
        //    var resultFilter = inputModel.Filters.First();

        //    //assert
        //    Assert.True(resultFilter.GetType() == typeof(BoolFilterVM));
        //    Assert.AreEqual(resultFilter.Name, characterisicName);
        //    Assert.AreEqual(resultFilter.Characteristic, characterisicName);
        //}

        //[Test]
        //public void CreateFilters_ModelWithRangeFilterVMMax10Min5InList_ShouldReturn_RangFilterMax10Min5()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var characterisicName = "AlcoholPercent";
        //    var max = 10;
        //    var min = 5;
        //    var inputModel = new CommodityListVM
        //    {
        //        Filters = new List<AbstractFilterVM> 
        //        { 
        //            new RangeFilterVM 
        //            { 
        //                Characteristic = characterisicName, 
        //                HigherValue = max, 
        //                LowestValue = min,
        //                IsActive = true,
        //                Name = characterisicName
        //            } 
        //        }
        //    };
        //    characteristicRepMock.Setup(c => c.GetFilters(Guid.Empty)).Returns(new List<FilterModel>
        //    {
        //        new FilterModel
        //        {
        //            Name = characterisicName,
        //            CharacteristicId = Guid.NewGuid(),
        //            Characteristic = new Characteristic { Name = characterisicName, DataType = "DOUBLE" },
        //            FilterType = new FilterTypeModel { Name = "RangeFilter" }
        //        }
        //    });

        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    //act
        //    var resultFilter = serv.CreateFilters(ref inputModel).First();

        //    //assert
        //    Assert.True(resultFilter.GetType() == typeof(RangeFilter));
        //    Assert.IsTrue(resultFilter.IsActive);
        //}

        //[Test]
        //public void CreateOne_FilreVMIsNotActive_ShouldReturn_Null()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var inputFilterVM = new RangeFilterVM { IsActive = false };
        //    //act
        //    var resultFilter = serv.CreateOne(inputFilterVM);

        //    //assert
        //    Assert.IsNull(resultFilter);
        //}
        //[Test]
        //public void CreateOne_FilreVMHasInvalidType_ShouldThrow_FilterNotFoundExceptions()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var inputFilterVM = new InvalidFilterVM { IsActive = true };
        //    //act

        //    //assert
        //    var ex = Assert.Throws<FilterNotFoundExceptions>(() => serv.CreateOne(inputFilterVM));
        //    Assert.That(ex.Message, Is.EqualTo($"Filter with VM type = {typeof(InvalidFilterVM).ToString()} not found"));
        //}
        //[Test]
        //public void CreateOne_RangeFilreVM_ShouldReturn_RangeFilter()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var inputFilterVM = new RangeFilterVM { IsActive = true, LowestValue = 10, HigherValue = 20 };
        //    //act
        //    var resultFilter = serv.CreateOne(inputFilterVM);

        //    //assert
        //    Assert.True(resultFilter.GetType() == typeof(RangeFilter));
        //}

        //[Test]
        //public void CreateOne_FilterModelWithTypeReangeFilter_ShouldReturn_RangeFilterVM()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var characteristicId = Guid.NewGuid();
        //    characteristicRepMock.Setup(c => c.GetMaxAvailableValue(characteristicId)).Returns(("10", "DOUBLE"));
        //    characteristicRepMock.Setup(c => c.GetMinAvailableValue(characteristicId)).Returns(("5", "DOUBLE"));
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var inputFilterVModel = new FilterModel
        //    {
        //        FilterType = new FilterTypeModel { Name = "RangeFilter" },
        //        CharacteristicId = characteristicId,
        //        Characteristic = new Characteristic { Name = "TestCharacteristic" }
        //    };
        //    //act
        //    var resultFilter = serv.CreateOne(inputFilterVModel);

        //    //assert
        //    Assert.True(resultFilter.GetType() == typeof(RangeFilterVM));
        //}
        //[Test]
        //public void CreateOne_FilterModelWithTypeReangeFilter_ShouldReturn_RangeFilterVMWithMinValue5AndMaxValue10()
        //{
        //    //arrange
        //    double minValue = 5;
        //    double maxValue = 10;
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var characteristicId = Guid.NewGuid();
        //    characteristicRepMock.Setup(c => c.GetMaxAvailableValue(characteristicId)).Returns((maxValue.ToString(), "DOUBLE"));
        //    characteristicRepMock.Setup(c => c.GetMinAvailableValue(characteristicId)).Returns((minValue.ToString(), "DOUBLE"));
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var inputFilterVModel = new FilterModel
        //    {
        //        FilterType = new FilterTypeModel { Name = "RangeFilter" },
        //        CharacteristicId = characteristicId,
        //        Characteristic = new Characteristic { Name = "TestCharacteristic" }
        //    };
        //    //act
        //    var resultFilter = serv.CreateOne(inputFilterVModel);

        //    //assert
        //    Assert.AreEqual(((RangeFilterVM)resultFilter).MinAvailableValue, minValue);
        //    Assert.AreEqual(((RangeFilterVM)resultFilter).MaxAvailableValue, maxValue);
        //}


        //[Test]
        //public void MapCommodity_CommodityNotInBusinessNetwork_ShouldThrwo_CommodityInBusinessNetworkNotFoundException()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var commodityId = Guid.NewGuid();
        //    commodityRepMock.Setup(c => c.GetBusinessNetworkForCommodity(commodityId, Constants.BusinessNetworks.Rozetka))
        //        .Returns((CommodityInBusinessNetwork)null);
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var inputCommodity = new Commodity
        //    {
        //        Id = commodityId
        //    };
        //    //act

        //    //assert
        //    var ex = Assert.Throws<CommodityInBusinessNetworkNotFoundException>(() => serv.MapCommodity(inputCommodity));
        //    Assert.That(ex.Message, Is.EqualTo($"Commodity with Id = {commodityId} in " +
        //        $"BusinessNetwork with Id = {Constants.BusinessNetworks.Rozetka} not found"));
        //}
        //[Test]
        //public void MapCommodity_WithNameC1_ShouldReturn_CommodityForListVMWithNameC1()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var commodityId = Guid.NewGuid();
        //    var name = "C1";
        //    var averageStars = 3;
        //    var responsesCount = 10;
        //    commodityRepMock.Setup(c => c.GetBusinessNetworkForCommodity(commodityId, Constants.BusinessNetworks.Rozetka))
        //        .Returns(new CommodityInBusinessNetwork());
        //    commodityRepMock.Setup(c => c.GetAverageStars(commodityId))
        //        .Returns(averageStars);
        //    commodityRepMock.Setup(c => c.GetResponsesCount(commodityId))
        //        .Returns(responsesCount);
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var inputCommodity = new Commodity
        //    {
        //        Id = commodityId,
        //        Name = name
        //    };
        //    //act
        //    var resultCommodity = serv.MapCommodity(inputCommodity);
        //    //assert
        //    Assert.AreEqual(resultCommodity.Name, name);
        //}
        //[Test]
        //public void MapCommodity_WithAverageStars3_ShouldReturn_CommodityForListVMWithAverageStars3()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var commodityId = Guid.NewGuid();
        //    var name = "C1";
        //    var averageStars = 3;
        //    var responsesCount = 10;
        //    commodityRepMock.Setup(c => c.GetBusinessNetworkForCommodity(commodityId, Constants.BusinessNetworks.Rozetka))
        //        .Returns(new CommodityInBusinessNetwork());
        //    commodityRepMock.Setup(c => c.GetAverageStars(commodityId))
        //        .Returns(averageStars);
        //    commodityRepMock.Setup(c => c.GetResponsesCount(commodityId))
        //        .Returns(responsesCount);
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var inputCommodity = new Commodity
        //    {
        //        Id = commodityId,
        //        Name = name
        //    };
        //    //act
        //    var resultCommodity = serv.MapCommodity(inputCommodity);
        //    //assert
        //    Assert.AreEqual(resultCommodity.AverageStars, averageStars);
        //}
        //[Test]
        //public void MapCommodity_WithResponsesCount10_ShouldReturn_CommodityForListVMWithResponsesCount10()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var commodityId = Guid.NewGuid();
        //    var name = "C1";
        //    var averageStars = 3;
        //    var responsesCount = 10;
        //    commodityRepMock.Setup(c => c.GetBusinessNetworkForCommodity(commodityId, Constants.BusinessNetworks.Rozetka))
        //        .Returns(new CommodityInBusinessNetwork());
        //    commodityRepMock.Setup(c => c.GetAverageStars(commodityId))
        //        .Returns(averageStars);
        //    commodityRepMock.Setup(c => c.GetResponsesCount(commodityId))
        //        .Returns(responsesCount);
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var inputCommodity = new Commodity
        //    {
        //        Id = commodityId,
        //        Name = name
        //    };
        //    //act
        //    var resultCommodity = serv.MapCommodity(inputCommodity);
        //    //assert
        //    Assert.AreEqual(resultCommodity.ResponsesCount, responsesCount);
        //}

        //[Test]
        //public void MapCommodities_WithCommoditiesWithBusinessNetworkIsNull_ShouldReturn_EmptyList()
        //{
        //    //arrange
        //    var commodityRepMock = new Mock<ICommodityDataHendler>();
        //    var characteristicRepMock = new Mock<ICharacteristicDataHendler>();
        //    var commodity1Id = Guid.NewGuid();
        //    var commodity2Id = Guid.NewGuid();
        //    commodityRepMock.Setup(c => c.GetBusinessNetworkForCommodity(commodity1Id, Constants.BusinessNetworks.Rozetka))
        //        .Returns((CommodityInBusinessNetwork)null);
        //    commodityRepMock.Setup(c => c.GetBusinessNetworkForCommodity(commodity2Id, Constants.BusinessNetworks.Rozetka))
        //        .Returns((CommodityInBusinessNetwork)null);
        //    var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object);
        //    var commodity1 = new Commodity { Id = commodity1Id };
        //    var commodity2 = new Commodity { Id = commodity2Id };
        //    var inputCommodityList = new List<Commodity> { commodity1, commodity2 };
        //    //act
        //    var resultCommodityList = serv.MapCommodities(inputCommodityList);
        //    //assert
        //    Assert.AreEqual(resultCommodityList.Count, 0);
        //}


        public void AddCommodityForParsing_WithExistsCode_ShouldThrow_CommodityAlreadyExistException()
        {
            //arrange
            var commodityRepMock = new Mock<ICommodityDataHandler>();
            var characteristicRepMock = new Mock<ICharacteristicDataHandler>();
            var code = "c1";
            var characteristicId = Guid.NewGuid();
            commodityRepMock.Setup(c => c.AddCodeForParsing(code, characteristicId))
                .Throws(new CommodityAlreadyExistException(code));
            var serv = new CommodityService(commodityRepMock.Object, characteristicRepMock.Object, new AppDbContext(new DbContextOptions<AppDbContext>()));
            //act

            //assert
            var ex = Assert.Throws<CommodityAlreadyExistException>(() => serv.AddCommodityCodeForParsing(code, characteristicId));
            Assert.That(ex.Message, Is.EqualTo($"Commodity with Code = {code} already exist"));
        }


        #region Private
        private Dictionary<Guid, string> GetCategories()
        {
            var res = new Dictionary<Guid, string>();
            categories.ForEach(c => res.Add(c.Id, c.Name));
            return res;
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        #endregion
        #region Data
        private static List<BusinessNetwork> businessNetworks = BusinessNetworks.Values.ToList();
        private static List<Category> categories = Categories.Values.ToList();
        private static List<Commodity> commodities = Commodities.Values.ToList();
        private static List<CommodityForParsing> commoditiesForParsing = CommoditiesForParsing.Values.ToList();
        private static List<CommodityCharacteristic> commodityCharacteristics = CommodityCharacteristics;
        private static List<Characteristic> characteristics = Characteristics.Values.ToList();
        private static List<CommodityInBusinessNetwork> commodityInBusinessNetworks = CommodityInBusinessNetworks;
        private static List<Response> responses = Responses;
        private static List<CharacteristicInBusinessNetwork> characteristicsInBusinessNetwork = CharacteristicsInBusinessNetwork;

        private static Dictionary<string, Category> _categories;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (_categories is null)
                {
                    var list = new List<Category>
                    {
                        new Category(){ Name = "Пиво", Id = Guid.NewGuid() },
                        new Category(){ Name = "Вино", Id = Guid.NewGuid() }
                    };
                    _categories = list.ToDictionary(c => c.Name);
                }
                return _categories;
            }
        }

        private static Dictionary<string, Characteristic> _characteristics;
        public static Dictionary<string, Characteristic> Characteristics
        {
            get
            {
                if (_characteristics is null)
                {
                    var list = new List<Characteristic>
                    {
                        new Characteristic(){ Id = Constants.Characteristics.Volume, Name = "Volume" },
                        new Characteristic(){ Id = Constants.Characteristics.AlcoholPercent, Name = "AlcoholPercent" },
                        new Characteristic(){ Id = Constants.Characteristics.Type, Name = "Type", Category = Categories["Пиво"]},
                        new Characteristic(){ Id = Constants.Characteristics.CountInPack, Name = "CountInPack" },
                        new Characteristic(){ Id = Constants.Characteristics.Colour, Name = "Colour", Category = Categories["Пиво"] },
                        new Characteristic(){ Id = Constants.Characteristics.Brewery, Name = "Brewery", Category = Categories["Пиво"]},
                        new Characteristic(){ Id = Constants.Characteristics.Kind, Name = "Kind"},
                        new Characteristic(){ Id = Constants.Characteristics.Country, Name = "Country" },
                        new Characteristic(){ Id = Constants.Characteristics.Endurance, Name = "Endurance", Category = Categories["Вино"] }
                    };
                    _characteristics = list.ToDictionary(c => c.Name);
                }
                return _characteristics;
            }
        }

        private static Dictionary<string, Commodity> _commodities;
        public static Dictionary<string, Commodity> Commodities
        {
            get
            {
                if (_commodities is null)
                {
                    var list = new List<Commodity>
                    {
                        new Commodity(){ Id = new Guid("44ABC8C8-E95F-4EE2-7F62-08D800E910E7"), Name = "Чернігівське", Category = Categories["Пиво"], Code = "П1", PictureURL = "https://img3.zakaz.ua/20180829.1535552877.ad72436478c_2018-08-29_Tatiana/20180829.1535552877.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg" },
                        new Commodity(){ Id = Guid.NewGuid(), Name = "Біла ніч", Category = Categories["Пиво"], Code = "П2", PictureURL = "https://img2.zakaz.ua/20180829.1535552776.ad72436478c_2018-08-29_Tatiana/20180829.1535552776.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg" },
                        new Commodity(){ Id = Guid.NewGuid(), Name = "Коблево", Category = Categories["Вино"], Code = "В1", PictureURL = "https://img2.zakaz.ua/upload.version_1.0.a58488de4964cad26aff94e2b27a125c.350x350.jpeg" },
                        new Commodity(){ Id = Guid.NewGuid(), Name = "Монастирське", Category = Categories["Вино"], Code = "В2", PictureURL = "https://img3.zakaz.ua/src.1584529315.ad72436478c_2020-03-19_Aliona/src.1584529315.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg", Description = "Супер смачне вино" },
                    };
                    _commodities = list.ToDictionary(c => c.Code);
                }
                return _commodities;
            }
        }
        private static Dictionary<string, CommodityForParsing> _commoditiesForParsing;
        public static Dictionary<string, CommodityForParsing> CommoditiesForParsing
        {
            get
            {
                if (_commoditiesForParsing is null)
                {
                    var list = new List<CommodityForParsing>
                    {
                        //new Commodity(){ Name = "Чернігівське", Category = Categories["Пиво"], Code = "П1", PictureURL = "https://img3.zakaz.ua/20180829.1535552877.ad72436478c_2018-08-29_Tatiana/20180829.1535552877.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg" },
                        //new Commodity(){ Name = "Біла ніч", Category = Categories["Пиво"], Code = "П2", PictureURL = "https://img2.zakaz.ua/20180829.1535552776.ad72436478c_2018-08-29_Tatiana/20180829.1535552776.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg" },
                        //new Commodity(){ Name = "Коблево", Category = Categories["Вино"], Code = "В1", PictureURL = "https://img2.zakaz.ua/upload.version_1.0.a58488de4964cad26aff94e2b27a125c.350x350.jpeg" },
                        //new Commodity(){ Name = "Монастирське", Category = Categories["Вино"], Code = "В2", PictureURL = "https://img3.zakaz.ua/src.1584529315.ad72436478c_2020-03-19_Aliona/src.1584529315.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg", Description = "Супер смачне вино" },
                        new CommodityForParsing(){ Category = Categories["Пиво"], Code = "180989516"},
                        new CommodityForParsing(){ Category = Categories["Пиво"], Code = "199769587"},
                        new CommodityForParsing(){ Category = Categories["Пиво"], Code = "199769533"},
                        new CommodityForParsing(){ Category = Categories["Пиво"], Code = "191150436"},
                        new CommodityForParsing(){ Category = Categories["Пиво"], Code = "96105694"},
                        new CommodityForParsing(){ Category = Categories["Пиво"], Code = "14236550"}
                    };
                    _commoditiesForParsing = list.ToDictionary(c => c.Code);
                }
                return _commoditiesForParsing;
            }
        }

        private static List<CommodityCharacteristic> _commodityCharacteristics;
        public static List<CommodityCharacteristic> CommodityCharacteristics
        {
            get
            {
                if (_commodityCharacteristics is null)
                {
                    var list = new List<CommodityCharacteristic>
                    {
                        //new CommodityCharacteristic(){ Commodity = Commodities["Чернігівське"], Characteristic = Characteristics["Volume"], Value = "0,5"},
                        new CommodityCharacteristic(){ Commodity = Commodities["П1"], Characteristic = Characteristics["AlcoholPercent"], Value = "5"},
                        //new CommodityCharacteristic(){ Commodity = Commodities["Чернігівське"], Characteristic = Characteristics["Type"], Value = "Фільтроване"},
                        //new CommodityCharacteristic(){ Commodity = Commodities["Біла ніч"], Characteristic = Characteristics["Volume"], Value = "0,7"},
                        new CommodityCharacteristic(){ Commodity = Commodities["П1"], Characteristic = Characteristics["AlcoholPercent"], Value = "0,045"},
                        //new CommodityCharacteristic(){ Commodity = Commodities["Біла ніч"], Characteristic = Characteristics["Type"], Value = "Фільтроване"},
                        //new CommodityCharacteristic(){ Commodity = Commodities["Коблево"], Characteristic = Characteristics["Volume"], Value = "0,7"},
                        new CommodityCharacteristic(){ Commodity = Commodities["В1"], Characteristic = Characteristics["AlcoholPercent"], Value = "0,15"},
                        //new CommodityCharacteristic(){ Commodity = Commodities["Коблево"], Characteristic = Characteristics["Endurance"], Value = "1"},
                        //new CommodityCharacteristic(){ Commodity = Commodities["Монастирське"], Characteristic = Characteristics["Volume"], Value = "0,7"},
                        new CommodityCharacteristic(){ Commodity = Commodities["В2"], Characteristic = Characteristics["AlcoholPercent"], Value = "0,17"},
                        //new CommodityCharacteristic(){ Commodity = Commodities["Монастирське"], Characteristic = Characteristics["Endurance"], Value = "2"},
                    };
                    _commodityCharacteristics = list;
                }
                return _commodityCharacteristics;
            }
        }


        private static Dictionary<string, BusinessNetwork> _businessNetworks;
        public static Dictionary<string, BusinessNetwork> BusinessNetworks
        {
            get
            {
                if (_businessNetworks is null)
                {
                    var list = new List<BusinessNetwork>
                    {
                        new BusinessNetwork(){ Id = Constants.BusinessNetworks.Rozetka, Name = "Розетка", SiteURL = "https://rozetka.com.ua/", PictureURL = "https://xl-static.rozetka.com.ua/assets/img/design/logo_n.svg" },
                        new BusinessNetwork(){ Name = "Бухни.юа", SiteURL = "", PictureURL = "" }
                    };
                    _businessNetworks = list.ToDictionary(c => c.Name);
                }
                return _businessNetworks;
            }
        }


        private static List<CommodityInBusinessNetwork> _commodityInBusinessNetworks;
        public static List<CommodityInBusinessNetwork> CommodityInBusinessNetworks
        {
            get
            {
                if (_commodityInBusinessNetworks is null)
                {
                    var list = new List<CommodityInBusinessNetwork>
                    {
                        //new CommodityInBusinessNetwork(){ Commodity = Commodities["Чернігівське"], BusinessNetwork = BusinessNetworks["Розетка"], Price = 14},
                        //new CommodityInBusinessNetwork(){ Commodity = Commodities["Біла ніч"], BusinessNetwork = BusinessNetworks["Розетка"], Price = 16},
                        //new CommodityInBusinessNetwork(){ Commodity = Commodities["Коблево"], BusinessNetwork = BusinessNetworks["Розетка"], Price = 60},
                        //new CommodityInBusinessNetwork(){ Commodity = Commodities["Монастирське"], BusinessNetwork = BusinessNetworks["Розетка"], Price = 75.9},
                        //new CommodityInBusinessNetwork(){ Commodity = Commodities["Біла ніч"], BusinessNetwork = BusinessNetworks["Бухни.юа"], Price = 15.5},
                        //new CommodityInBusinessNetwork(){ Commodity = Commodities["Коблево"], BusinessNetwork = BusinessNetworks["Бухни.юа"], Price = 66},
                        //new CommodityInBusinessNetwork(){ Commodity = Commodities["Монастирське"], BusinessNetwork = BusinessNetworks["Бухни.юа"], Price = 70.5}
                    };
                    _commodityInBusinessNetworks = list;
                }
                return _commodityInBusinessNetworks;
            }
        }


        private static List<Response> _responses;
        public static List<Response> Responses
        {
            get
            {
                if (_responses is null)
                {
                    var list = new List<Response>
                    {
                        //new Response(){ Commodity = Commodities["Чернігівське"], Stars = 5, Text = "Найс"},
                        //new Response(){ Commodity = Commodities["Чернігівське"], Stars = 3, Text = "На другий день башка болить(("}
                    };
                    _responses = list;
                }
                return _responses;
            }
        }

        private static List<CharacteristicInBusinessNetwork> _characteristicsInBusinessNetwork;
        public static List<CharacteristicInBusinessNetwork> CharacteristicsInBusinessNetwork
        {
            get
            {
                if (_characteristicsInBusinessNetwork is null)
                {
                    var list = new List<CharacteristicInBusinessNetwork>
                    {
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Volume, Name = "Объем, л", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.AlcoholPercent, Name = "Крепость", RightTrimCount = 1, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Colour, Name = "Цвет", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Type, Name = "Тип", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Brewery, Name = "Пивоварня", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Kind, Name = "Вид", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Country, Name = "Страна-производитель товара", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.CountInPack, Name = "Количество штук в упаковке", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Endurance, Name = "Endurance", RightTrimCount = 0, LeftTrimCount = 0}
                    };
                    _characteristicsInBusinessNetwork = list;
                }
                return _characteristicsInBusinessNetwork;
            }
        }

        #endregion
    }
}
