﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Services.Interfaces;
using AlcoCatalogBL.Services.Models.Commodities;
using AlcoCatalogBL.Services.Models.Commodity;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.Utils.Filters;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlcoCatalogBL.Services
{
    public class CommodityService : ICommodityService
    {
        private readonly AppDbContext _db;
        private readonly ICommodityDataHandler _commodities;
        private readonly ICharacteristicDataHandler _characteristics;
        public CommodityService()
        {
            _commodities = IoCContainer.Resolve<ICommodityDataHandler>();
            _characteristics = IoCContainer.Resolve<ICharacteristicDataHandler>();
            _db = IoCContainer.Resolve<DbContext>();
        }
        public CommodityService(ICommodityDataHandler commodities, ICharacteristicDataHandler characteristics, AppDbContext db)
        {
            _commodities = commodities;
            _characteristics = characteristics;
            _db = db;
        }

        public CommodityFullData GetCommodity(Guid id)
        {
            var commodity = _commodities.GetCommodity(id);
            if (commodity is null)
                throw new CommodityNotFoundException(id);
            var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Response, ResponseModel>());
            var responseMapper = new Mapper(config);

            config = new MapperConfiguration(cfg =>
                cfg.CreateMap<CommodityCharacteristic, CharacteristicModel>()
                    .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Characteristic.Name)));
            var charMapper = new Mapper(config);

            config = new MapperConfiguration(cfg =>
                cfg.CreateMap<CommodityInBusinessNetwork, PricesModel>()
                    .ForMember(dst => dst.BNImageURL, opt => opt.MapFrom(src => src.BusinessNetwork.PictureURL)));
            var priceMapper = new Mapper(config);

            config = new MapperConfiguration(cfg => cfg.CreateMap<Commodity, CommodityFullData>()
                .ForMember(dst => dst.Characteristics, opt => opt.MapFrom(src => charMapper.Map<List<CharacteristicModel>>(src.Characteristics)))
                .ForMember(dst => dst.Responses, opt => opt.MapFrom(src => responseMapper.Map<List<ResponseModel>>(src.Responses)))
                .ForMember(dst => dst.Prices, opt => opt.MapFrom(src => priceMapper.Map<List<PricesModel>>(src.BusinessNetworks)))
            );
            var mapper = new Mapper(config);
            var result = mapper.Map<CommodityFullData>(commodity);
            return result;
        }

        public List<FilterResponseModel> GetFilters(Guid categoryId)
        {
            var result = new List<FilterResponseModel>();
            _characteristics.GetCharacteristicsByCategory(categoryId)
                .Select(ch => ch.Id).ToList().ForEach(chId => result.Add(CreateOne(chId)));
            return result;
        }


        public CommodityGetRangeResponse GetRange(CommodityGetRangeRequest requestModel)
        {
            var result = new CommodityGetRangeResponse();
            var filters = MapFilters(requestModel.Filters);
            //var sorter = new CommoditySorter(requestModel.OrderData.OrderType, requestModel.OrderData.OrderDirection);
            var commodities = _commodities.GetRange((requestModel.PageNumber - 1) * requestModel.CommoditiesCount, 
                requestModel.CommoditiesCount, filters, requestModel.CategoryId);
            result.TotalCount = _commodities.GetCount(filters, requestModel.CategoryId);
            result.Commodities = MapCommodities(commodities);
            return result;
        }

        public List<CommodityAdminDataResponse> GetAll()
        {
            return  _db.Commodities.Select(c =>
                new CommodityAdminDataResponse()
                {
                    Code = c.Code,
                    Id = c.Id,
                    ImageURL = c.PictureURL,
                    Name = c.Name,
                }).ToList();
        }

        public List<CommodityAdminToParseDataResponse> GetSendedToParse()
        {
            return _db.CommoditiesForParsing.Include(c => c.Category).Select(c =>
                new CommodityAdminToParseDataResponse
                {
                    Id = c.Id,
                    CategoryName = c.Category.Name,
                    Code = c.Code,
                    isSended = c.IsSendedToParsing,
                }).ToList();
        }


        public void DeleteCommodity(Guid id)
            => _commodities.Delete(id);

        private List<AbstractFilter> MapFilters(List<FilterRequestModel> filters)
        {
            var result = new List<AbstractFilter>();
            if (filters is null)
                return result;
            foreach (var filter in filters)
            {
                var mappedFilter = CreateOne(filter);
                if (!(mappedFilter is null))
                    result.Add(mappedFilter);
            }
            return result;
        }

        private AbstractFilter CreateOne(FilterRequestModel filterRM)
        {
            AbstractFilter result = null;
            var filterType = _characteristics.GetCharacteristicFilterTypeId(filterRM.CharacteristicId);

            if (filterType == Constants.FilterType.Value)
                result = new ValueFilter(_characteristics, filterRM.CharacteristicId, filterRM.Values);
            else if (filterType == Constants.FilterType.Range)
                result = new RangeFilter(_characteristics, filterRM.CharacteristicId, filterRM.LowestValue, filterRM.HigherValue);
            else if (filterType == Constants.FilterType.Bool)
                result = new BoolFilter(_characteristics, filterRM.CharacteristicId, filterRM.IsChecked);
            else
                Logger.Log(new FilterNotFoundExceptions(filterType), "");

            return result;
        }

        private FilterResponseModel CreateOne(Guid characteristicId)
        {
            var filterType = _characteristics.GetCharacteristicFilterType(characteristicId);

            FilterResponseModel result = new FilterResponseModel()
            {
                CharacteristicId = characteristicId,
                Name = _characteristics.GetCharacteristicName(characteristicId),
                Type = filterType.Name
            };

            if (filterType.Id == Constants.FilterType.Value)
                result.AvailableValues = _characteristics.GetAvailableValues(characteristicId);
            else if (filterType.Id == Constants.FilterType.Range)
            {
                var availableValues = _characteristics.GetAvailableValues(characteristicId);
                if(availableValues.Count() > 0)
                {
                    result.MaxAvailableValue = availableValues
                        .Select(ch => double.TryParse(ch, out double chd) ? chd : double.MinValue).Max();
                    result.MinAvailableValue = availableValues
                        .Select(ch => double.TryParse(ch, out double chd) ? chd : double.MaxValue).Min();
                }
            }
            else if (filterType.Id == Constants.FilterType.Bool) { }
            else
            {
                Logger.Log(new FilterNotFoundExceptions(filterType.Id), $"Filter type name = {filterType.Name}");
                return null;
            }

            return result;
        }

        private List<CommodityPreviewDataResponse> MapCommodities(List<Commodity> commodities)
        {
            var result = new List<CommodityPreviewDataResponse>();
            foreach (var commodity in commodities)
            {
                try
                {
                    result.Add(MapCommodity(commodity));
                }
                catch (CommodityInBusinessNetworkNotFoundException) { }
            }
            return result;
        }

        private CommodityPreviewDataResponse MapCommodity(Commodity commodity)
        {
            var businessNetwork = _commodities.GetBusinessNetworkForCommodity(commodity.Id, Constants.BusinessNetworks.Rozetka);
            if (businessNetwork == null)
                throw new CommodityInBusinessNetworkNotFoundException(commodity.Id, Constants.BusinessNetworks.Rozetka);
            return new CommodityPreviewDataResponse
            {
                Id = commodity.Id,
                Name = commodity.Name,
                Code = commodity.Code,
                PictureURL = commodity.PictureURL,
                IsDiscount = businessNetwork.IsDiscount,
                Price = businessNetwork.Price,
                OldPrice = businessNetwork.OldPrice,
                AverageStars = _commodities.GetAverageStars(commodity.Id),
                ResponsesCount = _commodities.GetResponsesCount(commodity.Id)
            };

        }

        public Guid AddCommodityCodeForParsing(string code, Guid categoryId)
            => _commodities.AddCodeForParsing(code, categoryId);


        public List<CommodityPreviewDataResponse> GetNewCommodities(int conunt)
            => MapCommodities(_commodities.GetNewCommodities(conunt));

        public List<ResponseFullData> GetNewResponse(int conunt)
        {
            return _db.Responses.Include(r => r.Commodity).OrderByDescending(r => r.CreatedOn)
                .Select(r => new ResponseFullData()
                {
                    AuthorName = r.AuthorName,
                    Stars = r.Stars,
                    Text = r.Text,
                    CommodityId = r.CommodityId,
                    CommodityName = r.Commodity.Name,
                }).ToList();
        }
    }
}
