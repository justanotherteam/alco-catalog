﻿using AlcoCatalogBL.Services.Models.Commodity;
using AlcoCatalogBL.Utils;
using System.Text.Json;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Commodities
{
    public class CommodityGetRangeResponse : ErrorModel
    {
        [JsonPropertyName("count")]
        public int TotalCount { get; set; }


        [JsonPropertyName("commodities")]
        public List<CommodityPreviewDataResponse> Commodities { get; set; }
    }


}
