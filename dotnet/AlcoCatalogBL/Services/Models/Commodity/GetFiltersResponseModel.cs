﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Commodity
{
    public class GetFiltersResponseModel : ErrorModel
    {
        [JsonPropertyName("filters")]
        public IEnumerable<FilterResponseModel> Filters { get; set; }
    }
}
