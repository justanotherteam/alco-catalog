﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Commodity
{
    public class AddCommodityRequestModel
    {
        [JsonPropertyName("categoryId")]
        public Guid CategoryId { get; set; }
        [JsonPropertyName("code")]
        public string Code { get; set; }
    }
}
