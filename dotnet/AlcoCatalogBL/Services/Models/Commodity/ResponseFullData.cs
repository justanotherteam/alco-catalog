﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Commodity
{
    public class ResponseFullData
    {
        [JsonPropertyName("rating")]
        public int Stars { get; set; }
        [JsonPropertyName("author")]
        public string AuthorName { get; set; }
        [JsonPropertyName("text")]
        public string Text { get; set; }
        [JsonPropertyName("commodityName")]
        public string CommodityName { get; set; }
        [JsonPropertyName("commodityId")]
        public Guid CommodityId { get; set; }
    }
}
