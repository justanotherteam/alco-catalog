﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Services.Models.Commodities
{
    public class CommodityPreviewDataResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string PictureURL { get; set; }
        public bool IsDiscount { get; set; }
        public double Price { get; set; }
        public double OldPrice { get; set; }
        public int AverageStars { get; set; }
        public int ResponsesCount { get; set; }
    }
}
