﻿using AlcoCatalogBL.Services.Models.Commodity;
using AlcoCatalogBL.Utils;
using System.Text.Json;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System;

namespace AlcoCatalogBL.Services.Models.Commodities
{
    public class CommodityAdminToParseDataResponse
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("code")]
        public string Code { get; set; }
        [JsonPropertyName("categoryName")]
        public string CategoryName { get; set; }
        [JsonPropertyName("isSended")]
        public bool isSended { get; set; }
    }

}
