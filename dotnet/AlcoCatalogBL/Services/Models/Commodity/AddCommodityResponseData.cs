﻿using System;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Commodity
{
    public class AddCommodityResponseData : ErrorModel
    {
        [JsonPropertyName("id")]
        public Guid? Id { get; set; }
    }
}
