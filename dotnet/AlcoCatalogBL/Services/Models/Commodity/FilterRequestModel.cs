﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Commodity
{
    public class FilterRequestModel
    {
        [JsonPropertyName("id")]
        public Guid CharacteristicId { get; set; }
        [JsonPropertyName("isChecked")]
        public bool IsChecked { get; set; }

        [JsonPropertyName("lowestValue")]
        public double LowestValue { get; set; }
        [JsonPropertyName("higherValue")]
        public double HigherValue { get; set; }
        [JsonPropertyName("values")]
        public List<string> Values { get; set; }
    }
}
