﻿using AlcoCatalogBL.Services.Models.Commodity;
using AlcoCatalogBL.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Commodities
{
    public class CommodityGetRangeRequest
    {
        [JsonPropertyName("count")]
        public int CommoditiesCount { get; set; }
        [JsonPropertyName("page")]
        public int PageNumber { get; set; }

        [JsonPropertyName("order")]
        public CommodityOrder OrderData { get; set; }


        [JsonPropertyName("filters")]
        public List<FilterRequestModel> Filters { get; set; }

        [JsonPropertyName("categoryId")]
        public Guid? CategoryId { get; set; }
    }

    public class CommodityOrder
    {
        [JsonPropertyName("orderType")]
        public OrderType OrderType { get; set; }
        [JsonPropertyName("orderDirection")]
        public OrderDirection OrderDirection { get; set; }
    }

}
