﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Commodity
{
    public class FilterResponseModel
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("type")]
        public string Type { get; set; }
        [JsonPropertyName("id")]
        public Guid CharacteristicId { get; set; }

        [JsonPropertyName("maxAvailableValue")]
        public double MaxAvailableValue { get; set; }
        [JsonPropertyName("minAvailableValue")]
        public double MinAvailableValue { get; set; }
        [JsonPropertyName("availableValues")]
        public List<string> AvailableValues { get; set; }

    }
}
