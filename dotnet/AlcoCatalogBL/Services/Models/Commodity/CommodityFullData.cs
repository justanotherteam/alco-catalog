﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Commodity
{
    public class CommodityFullData : ErrorModel
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("categoryId")]
        public Guid CategoryId { get; set; }
        [JsonPropertyName("category")]
        public string Category { get; set; }
        [JsonPropertyName("code")]
        public string Code { get; set; }
        [JsonPropertyName("pirtureUrl")]
        public string PictureURL { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("characteristics")]
        public List<CharacteristicModel> Characteristics { get; set; }
        [JsonPropertyName("prices")]
        public List<PricesModel> Prices { get; set; }
        [JsonPropertyName("responses")]
        public List<ResponseModel> Responses { get; set; }
    }

    public class CharacteristicModel
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("value")]
        public string Value { get; set; }
    }
    public class PricesModel
    {
        [JsonPropertyName("url")]
        public string Link { get; set; }
        [JsonPropertyName("storeImageUrl")]
        public string BNImageURL { get; set; }
        [JsonPropertyName("price")]
        public double Price { get; set; }
        [JsonPropertyName("oldPrice")]
        public double OldPrice { get; set; }
    }
    public class ResponseModel
    {
        [JsonPropertyName("rating")]
        public int Stars { get; set; }
        [JsonPropertyName("author")]
        public string AuthorName { get; set; }
        [JsonPropertyName("text")]
        public string Text { get; set; }
    }
}
