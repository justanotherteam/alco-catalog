﻿using AlcoCatalogBL.Services.Models.Commodity;
using AlcoCatalogBL.Utils;
using System.Text.Json;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System;

namespace AlcoCatalogBL.Services.Models.Commodities
{
    public class CommodityAdminDataResponse
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("code")]
        public string Code { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("imageUrl")]
        public string ImageURL { get; set; }
    }

}
