﻿using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models
{
    public class ErrorModel
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
