﻿using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Account
{
    public class TokenRequestModel
    {
        [JsonPropertyName("userName")]
        public string UserName { get; set; }
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}
