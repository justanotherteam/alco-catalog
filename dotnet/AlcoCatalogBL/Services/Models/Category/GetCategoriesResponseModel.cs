﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Category
{
    public class GetCategoriesResponseModel : ErrorModel
    {
        [JsonPropertyName("categories")]
        public IEnumerable<CategoryModel> Categories { get; set; }
        public GetCategoriesResponseModel()
        {
            Categories = new List<CategoryModel>();
        }
    }
    public class CategoryModel
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
    }

}
