﻿using System;
using System.Text.Json.Serialization;

namespace AlcoCatalogBL.Services.Models.Category
{
    public class AddCategoryResponseModel : ErrorModel
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
    }
}
