﻿using AlcoCatalogBL.Models;
using AlcoCatalogBL.Services.Models.Commodities;
using AlcoCatalogBL.Services.Models.Commodity;
using AlcoCatalogBL.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Services.Interfaces
{
    public interface ICommodityService
    {
        public List<FilterResponseModel> GetFilters(Guid categoryId);
        public CommodityGetRangeResponse GetRange(CommodityGetRangeRequest requestModel);
        public List<CommodityAdminDataResponse> GetAll();
        public List<CommodityAdminToParseDataResponse> GetSendedToParse();
        public CommodityFullData GetCommodity(Guid id);
        public Guid AddCommodityCodeForParsing(string code, Guid categoryId);
        public List<CommodityPreviewDataResponse> GetNewCommodities(int conunt);
        public List<ResponseFullData> GetNewResponse(int conunt);

    }
}
