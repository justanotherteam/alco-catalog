﻿using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Services.Interfaces
{
    public interface IAccountService
    {
        public User GetUser(string username, string password);
    }
}
