﻿using AlcoCatalogBL.Services.Models;
using AlcoCatalogBL.Services.Models.Category;
using System;

namespace AlcoCatalogBL.Services.Interfaces
{
    public interface ICategoryService
    {
        GetCategoriesResponseModel Get();
        void Delete(Guid id);
        AddCategoryResponseModel Add(string name);
    }
}
