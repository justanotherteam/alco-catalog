﻿using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Services.Interfaces;
using AlcoCatalogBL.Services.Models;
using AlcoCatalogBL.Services.Models.Category;
using AlcoCatalogBL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlcoCatalogBL.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryDataHandler _categories;
        public CategoryService()
        {
            _categories = IoCContainer.Resolve<ICategoryDataHandler>();
        }
        public CategoryService(ICategoryDataHandler categories)
        {
            _categories = categories;
        }

        public AddCategoryResponseModel Add(string name)
            => new AddCategoryResponseModel { Id = _categories.Add(name) };

        public void Delete(Guid id)
            => _categories.Delete(id);

        public GetCategoriesResponseModel Get()
            => new GetCategoriesResponseModel {
                Categories = _categories.GetAll().Select(c => new CategoryModel { Id = c.Id, Name = c.Name })};
        
    }
}
