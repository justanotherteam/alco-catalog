﻿using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Services.Interfaces;
using AlcoCatalogBL.Utils;

namespace AlcoCatalogBL.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountDataHandler _accounts;
        public AccountService()
        {
            _accounts = IoCContainer.Resolve<IAccountDataHandler>();
        }
        public AccountService(IAccountDataHandler accounts)
        {
            _accounts = accounts;
        }
        public User GetUser(string username, string password)
            => _accounts.GetOne(username, password);
    }
}
