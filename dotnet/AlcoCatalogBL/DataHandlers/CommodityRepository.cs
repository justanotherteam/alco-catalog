﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.Utils.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AlcoCatalogBL.DataHandlers
{
    public class CommodityRepository : ICommodityDataHandler
    {
        private readonly AppDbContext _dbContext;
        public CommodityRepository()
        {
            _dbContext = IoCContainer.Resolve<DbContext>();
        }
        public CommodityRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Commodity> GetAll()
            => _dbContext.Commodities.ToList();

        public Dictionary<Guid, string> GetCategories()
        {
            var result = new Dictionary<Guid, string>();
            _dbContext.Categories.ToList().ForEach(c => result.Add(c.Id, c.Name));
            return result;
        }

        public Guid GetCategoryId(Guid commodityId)
            => _dbContext.Commodities
                .FirstOrDefault(c => c.Id == commodityId)?.CategoryId ?? Guid.Empty;

        public Commodity GetCommodity(Guid id)
            => GetCommodity(c => c.Id == id);

        public Commodity GetCommodity(string code)
            => GetCommodity(c => c.Code == code);

        private Commodity GetCommodity(Expression<Func<Commodity, bool>> func)
            => _dbContext.Commodities
                .Include(c => c.Characteristics)
                    .ThenInclude(ch => ch.Characteristic)
                .Include(c => c.Category)
                .Include(c => c.Responses)
                .Include(c => c.BusinessNetworks)
                    .ThenInclude(bn => bn.BusinessNetwork)
                .FirstOrDefault(func);

        public Commodity GetOne(Guid id)
            => _dbContext.Commodities.FirstOrDefault(c => c.Id == id);

        public List<Commodity> GetRange(int firstIndex, int conunt, List<AbstractFilter> filters, Guid? categoryId)
        {
            var commoditiesQuery = _dbContext.Commodities
                .Include(c => c.Characteristics)
                    .ThenInclude(ch => ch.Characteristic).ToList();
            if (!(categoryId is null) && categoryId != Constants.Categories.General)
                commoditiesQuery = commoditiesQuery.Where(c => c.CategoryId == categoryId).ToList();
            return commoditiesQuery.Where(c => filters.All(f => f.Filter(c)))
                .ToList().Skip(firstIndex).Take(conunt).ToList();
        }

        public int GetCount(List<AbstractFilter> filters, Guid? categoryId)
        {
            var i = _dbContext.Commodities
                .Include(c => c.Characteristics)
                    .ThenInclude(ch => ch.Characteristic).Where(c => !string.IsNullOrEmpty(c.Name)).ToList();
            if (!(categoryId is null) && categoryId != Constants.Categories.General)
                i = i.Where(c => c.CategoryId == categoryId).ToList();
            return i.Where(c => filters.All(f => f.Filter(c))).Count();
        }

        public Guid AddCodeForParsing(string code, Guid categoryId)
        {
            var id = Guid.NewGuid();
            if (_dbContext.Commodities.Any(c => c.Code == code) 
                || _dbContext.CommoditiesForParsing.Any(c => c.Code == code))
                    throw new CommodityAlreadyExistException(code);
            _dbContext.CommoditiesForParsing.Add(new CommodityForParsing { Id = id, CategoryId = categoryId, Code = code });
            _dbContext.SaveChanges();
            return id;
        }

        public CommodityInBusinessNetwork GetBusinessNetworkForCommodity(Guid commodityId, Guid businessNetworkId)
            => _dbContext.CommodityInBusinessNetworks
                .Include(cbn => cbn.BusinessNetwork).FirstOrDefault(bn =>
                    bn.CommodityId == commodityId && bn.BusinessNetworkId == businessNetworkId);

        public int GetResponsesCount(Guid commodityId)
            => _dbContext.Responses.Where(r =>
                    r.CommodityId == commodityId).Count();

        public int GetAverageStars(Guid commodityId)
            => (int)Math.Round(_dbContext.Responses.Select(r =>
                    r.Stars).Average());

        public void SetCommodityData(Commodity commodity)
        {
            commodity.ModifiedOn = DateTime.Now;
            var oldCommodity = _dbContext.Commodities.FirstOrDefault(c => c.Id == commodity.Id);
            if (!(oldCommodity is null))
            {
                _dbContext.Commodities.Remove(oldCommodity);
                _dbContext.SaveChanges();
            }
            _dbContext.Commodities.Add(commodity);
            _dbContext.SaveChanges();
        }

        public BusinessNetwork GetBusinessNetwork(Guid id)
            => _dbContext.BusinessNetworks.FirstOrDefault(bn => bn.Id == id) ?? throw new BusinessNetworkNotFoundException(id);

        public void RemoveResponses(Guid commodityId)
        {
            _dbContext.RemoveRange(_dbContext.Responses.Where(r => r.CommodityId == commodityId));
            _dbContext.SaveChanges();
        }

        public void RemoveBusinessNetwork(Guid businessNetworkId)
            => _dbContext.RemoveRange(_dbContext.BusinessNetworks.Where(bn => bn.Id == businessNetworkId));

        public List<CommodityForParsing> GetAllCommoditiesForParsing()
            => _dbContext.CommoditiesForParsing.ToList();

        public bool TryDeleteCommodityForParsing(string code)
        {
            var commodity = _dbContext.CommoditiesForParsing.FirstOrDefault(c => c.Code == code);
            if (commodity is null) return false;
            _dbContext.CommoditiesForParsing.Remove(commodity);
            _dbContext.SaveChanges();
            return true;
        }

        public void SetStateForSendForParsing(string code, bool state)
        {
            var commodityForParsing = _dbContext.CommoditiesForParsing.FirstOrDefault(c => c.Code == code);
            if(commodityForParsing != null)
                commodityForParsing.IsSendedToParsing = state;
            var commodity = _dbContext.Commodities.FirstOrDefault(c => c.Code == code);
            if (commodity != null)
                commodity.IsSendedToParsing = state;
            _dbContext.SaveChanges();
        }

        public Guid GetCategoryIdFromCommodityForParsing(string commodityCode)
            => _dbContext.CommoditiesForParsing
                .FirstOrDefault(c => c.Code == commodityCode)?.CategoryId ?? new Guid("FF2E3371-49E0-4506-EFFA-08D808BE91CC");

        public List<Commodity> GetNewCommodities(int count)
            => _dbContext.Commodities
                .Include(c => c.Characteristics)
                    .ThenInclude(ch => ch.Characteristic)
                    .Where(c => !string.IsNullOrEmpty(c.Name))
                    .OrderByDescending(c => c.ModifiedOn).Take(count).ToList();

        public void Delete(Guid id)
        {
            if (_dbContext.Commodities.Select(c => c.Id).Contains(id))
            {
                _dbContext.Commodities.Remove(_dbContext.Commodities.First(c => c.Id == id));
                _dbContext.SaveChanges();
            }
        }
    }
}
