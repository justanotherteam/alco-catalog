﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlcoCatalogBL.DataHandlers
{
    public class AccountRepository : IAccountDataHandler
    {
        private readonly AppDbContext _dbContext;
        public AccountRepository()
        {
            _dbContext = IoCContainer.Resolve<DbContext>();
        }
        public AccountRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAll()
        {
            throw new NotImplementedException();
        }

        public User GetOne(string username, string password)
            => _dbContext.Users.FirstOrDefault(x => x.Login == username && x.Password == password);

        public User GetOne(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
