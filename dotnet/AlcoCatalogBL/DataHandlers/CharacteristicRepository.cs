﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlcoCatalogBL.DataHandlers
{
    public class CharacteristicRepository : ICharacteristicDataHandler
    {
        private readonly AppDbContext _dbContext;
        public CharacteristicRepository()
        {
            _dbContext = IoCContainer.Resolve<DbContext>();
        }
        public CharacteristicRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> GetAvailableValues(Guid characteristicId)
            => _dbContext.CommodityCharacteristics
                .Where(cc => cc.CharacteristicId == characteristicId)
                .GroupBy(ch => ch.Value)
                .Select(g => g.Key).ToList();


        public List<Characteristic> GetCharacteristicsByCategory(Guid categoryId)
            => _dbContext.Characteristics
                .Where(ch => ch.CategoryId == categoryId || ch.CategoryId == Constants.Categories.General || ch.CategoryId == null).ToList();

        public string GetCharacteristicValue(Guid commodityId, Guid characteristicId)
        {
            var result = _dbContext.CommodityCharacteristics
                .FirstOrDefault(cc => cc.CommodityId == commodityId
                        && characteristicId == cc.CharacteristicId);
            if(result is null)
                return "";
            return result.Value;
        }


        public Characteristic GetOne(Guid id)
            => _dbContext.Characteristics.FirstOrDefault(ch => ch.Id == id);

        public void UpdateCharactersticValue(Guid characteristicId, Guid commodityId, string value)
        {
            var characteristicValue = _dbContext.CommodityCharacteristics
                .FirstOrDefault(cc => (cc.CharacteristicId == characteristicId && cc.CommodityId == commodityId));
            if (characteristicValue is null)
            {
                _dbContext.CommodityCharacteristics.Add(new CommodityCharacteristic
                {
                    CommodityId = commodityId,
                    CharacteristicId = characteristicId,
                    Value = value
                });
            }
            else
                characteristicValue.Value = value;
            _dbContext.SaveChanges();
        }

        public CharacteristicInBusinessNetwork GetCharacteristicInBusinessNetwork(string characteristicName, Guid businessNetwork)
            => _dbContext.CharacteristicsInBusinessNetwork
                .FirstOrDefault(cbn => cbn.Name == characteristicName && cbn.BusinessNetworkId == businessNetwork);

        public string GetCharacteristicName(Guid characteristicId)
         => _dbContext.Characteristics
                .Where(ch => ch.Id == characteristicId)
                .FirstOrDefault()?.Name;

        public Guid? GetCharacteristicFilterTypeId(Guid characteristicId)
            => _dbContext.Characteristics
                .Where(ch => ch.Id == characteristicId)
                .FirstOrDefault()?.FilterTypeId;
            

        public FilterType GetCharacteristicFilterType(Guid characteristicId)
            => _dbContext.Characteristics
                .Include(ch => ch.FilterType)
                .Where(ch => ch.Id == characteristicId)
                .FirstOrDefault()?.FilterType;

        public IEnumerable<Characteristic> GetAll()
            => _dbContext.Characteristics.ToList();


        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
