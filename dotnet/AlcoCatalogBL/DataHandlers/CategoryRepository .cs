﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.Utils.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AlcoCatalogBL.DataHandlers
{
    public class CategoryRepository : ICategoryDataHandler
    {
        private readonly AppDbContext _dbContext;
        public CategoryRepository()
        {
            _dbContext = IoCContainer.Resolve<DbContext>();
        }
        public CategoryRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Guid Add(string name)
        {
            if(string.IsNullOrEmpty(name))
                throw new CategoryException("Add category error. Empty name");
            if(_dbContext.Categories.Select(c => c.Name).Contains(name))
                throw new CategoryException($"Add category error. Category with name = {name} already exist");

            var newId = Guid.NewGuid();
            var newCategory = new Category
            {
                Id = newId,
                Name = name,
                Characteristics = _dbContext.Characteristics.Where(ch => ch.CategoryId == Constants.Categories.General).ToList()
            };

            _dbContext.Categories.Add(newCategory);
            _dbContext.SaveChanges();

            return newId;
        }

        public void Delete(Guid id)
        {
            if(_dbContext.Categories.Select(c => c.Id).Contains(id))
            {
                _dbContext.Categories.Remove(_dbContext.Categories.First(c => c.Id == id));
                _dbContext.SaveChanges();
            }
        }

        public IEnumerable<Category> GetAll()
            => _dbContext.Categories.ToList();

        public Category GetOne(Guid id)
        {
            if (!_dbContext.Categories.Any(c => c.Id == id))
                throw new CategoryException($"Get category error. Category with id = {id} does not exist");
            return _dbContext.Categories.First(c => c.Id == id);
        }
    }
}
