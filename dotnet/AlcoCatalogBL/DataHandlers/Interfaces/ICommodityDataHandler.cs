﻿using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.Utils.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.DataHandlers.Interfaces
{
    public interface ICommodityDataHandler : IBaseDataHandler<Commodity>
    {
        public Commodity GetCommodity(Guid id);
        public Commodity GetCommodity(string code);
        public List<CommodityForParsing> GetAllCommoditiesForParsing();
        public bool TryDeleteCommodityForParsing(string code);
        public void SetStateForSendForParsing(string code, bool state);
        public List<Commodity> GetRange(int firstIndex, int count, List<AbstractFilter> filters, Guid? categoryId);
        public int GetCount(List<AbstractFilter> filters, Guid? categoryId);
        public List<Commodity> GetNewCommodities(int conunt);
        public Guid GetCategoryId(Guid commodityId);
        public Guid GetCategoryIdFromCommodityForParsing(string commodityCode);
        public Guid AddCodeForParsing(string code, Guid categoryId);
        public CommodityInBusinessNetwork GetBusinessNetworkForCommodity(Guid commodityId, Guid businessNetworkId);
        public void SetCommodityData(Commodity commodity);
        public BusinessNetwork GetBusinessNetwork(Guid id);
        public void RemoveResponses(Guid commodityId);
        public void RemoveBusinessNetwork(Guid businessNetworkId);
        public int GetResponsesCount(Guid commodityId);
        public int GetAverageStars(Guid commodityId);
    }
}
