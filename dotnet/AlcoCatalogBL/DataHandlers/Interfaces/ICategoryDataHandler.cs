﻿using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.Utils.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.DataHandlers.Interfaces
{
    public interface ICategoryDataHandler : IBaseDataHandler<Category>
    {
        Guid Add(string name);
    }
}
