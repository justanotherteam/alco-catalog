﻿using System;
using System.Collections.Generic;

namespace AlcoCatalogBL.DataHandlers.Interfaces
{
    public interface IBaseDataHandler<T> where T : class
    {
        public T GetOne(Guid id);
        public IEnumerable<T> GetAll();
        public void Delete(Guid id);
    }
}
