﻿using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.DataHandlers.Interfaces
{
    public interface IAccountDataHandler : IBaseDataHandler<User>
    {
        User GetOne(string username, string password);
    }
}
