﻿using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;

namespace AlcoCatalogBL.DataHandlers.Interfaces
{
    public interface ICharacteristicDataHandler : IBaseDataHandler<Characteristic>
    {
        public string GetCharacteristicName(Guid characteristicId);
        public string GetCharacteristicValue(Guid commodityId, Guid characteristicId);
        public Guid? GetCharacteristicFilterTypeId(Guid characteristicId);
        public FilterType GetCharacteristicFilterType(Guid characteristicId);
        public List<string> GetAvailableValues(Guid characteristicId);
        public List<Characteristic> GetCharacteristicsByCategory(Guid categoryId);
        public void UpdateCharactersticValue(Guid characteristicId, Guid commodityId, string value);
        public CharacteristicInBusinessNetwork GetCharacteristicInBusinessNetwork(string characteristicName, Guid businessNetwork);

    }
}
