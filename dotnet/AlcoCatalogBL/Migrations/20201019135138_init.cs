﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AlcoCatalogBL.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BusinessNetworks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    ModifiedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    Name = table.Column<string>(nullable: true),
                    SiteURL = table.Column<string>(nullable: true),
                    PictureURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessNetworks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    ModifiedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    LowestAlcoholContains = table.Column<double>(nullable: false),
                    HighestAlcoholContains = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FilterTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilterTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    Inner = table.Column<string>(nullable: true),
                    ExceptionMessage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    NormalizedUserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    NormalizedEmail = table.Column<string>(nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    ModifiedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Commodities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    ModifiedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    Name = table.Column<string>(nullable: true),
                    CategoryId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    PictureURL = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsSendedToParsing = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commodities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Commodities_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommoditiesForParsing",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    ModifiedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    Code = table.Column<string>(nullable: true),
                    CategoryId = table.Column<Guid>(nullable: false),
                    IsSendedToParsing = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommoditiesForParsing", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommoditiesForParsing_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Characteristics",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    ModifiedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    Name = table.Column<string>(nullable: true),
                    CategoryId = table.Column<Guid>(nullable: true),
                    FilterTypeId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characteristics", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Characteristics_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Characteristics_FilterTypes_FilterTypeId",
                        column: x => x.FilterTypeId,
                        principalTable: "FilterTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommodityInBusinessNetworks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    ModifiedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    CommodityId = table.Column<Guid>(nullable: false),
                    BusinessNetworkId = table.Column<Guid>(nullable: false),
                    Link = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false),
                    OldPrice = table.Column<double>(nullable: false),
                    IsDiscount = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommodityInBusinessNetworks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommodityInBusinessNetworks_BusinessNetworks_BusinessNetworkId",
                        column: x => x.BusinessNetworkId,
                        principalTable: "BusinessNetworks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CommodityInBusinessNetworks_Commodities_CommodityId",
                        column: x => x.CommodityId,
                        principalTable: "Commodities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Responses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    ModifiedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    CommodityId = table.Column<Guid>(nullable: false),
                    Stars = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    Date = table.Column<string>(nullable: true),
                    Likes = table.Column<int>(nullable: false),
                    Dislikes = table.Column<int>(nullable: false),
                    Benefits = table.Column<string>(nullable: true),
                    Disadvantages = table.Column<string>(nullable: true),
                    AuthorName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Responses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Responses_Commodities_CommodityId",
                        column: x => x.CommodityId,
                        principalTable: "Commodities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CharacteristicsInBusinessNetwork",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    BusinessNetworkId = table.Column<Guid>(nullable: false),
                    CharacteristicId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    RightTrimCount = table.Column<int>(nullable: false),
                    LeftTrimCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacteristicsInBusinessNetwork", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharacteristicsInBusinessNetwork_BusinessNetworks_BusinessNetworkId",
                        column: x => x.BusinessNetworkId,
                        principalTable: "BusinessNetworks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacteristicsInBusinessNetwork_Characteristics_CharacteristicId",
                        column: x => x.CharacteristicId,
                        principalTable: "Characteristics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommodityCharacteristics",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    ModifiedOn = table.Column<DateTime>(nullable: true, defaultValueSql: "GETDATE()"),
                    CommodityId = table.Column<Guid>(nullable: false),
                    CharacteristicId = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommodityCharacteristics", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommodityCharacteristics_Characteristics_CharacteristicId",
                        column: x => x.CharacteristicId,
                        principalTable: "Characteristics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CommodityCharacteristics_Commodities_CommodityId",
                        column: x => x.CommodityId,
                        principalTable: "Commodities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Characteristics_CategoryId",
                table: "Characteristics",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Characteristics_FilterTypeId",
                table: "Characteristics",
                column: "FilterTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacteristicsInBusinessNetwork_BusinessNetworkId",
                table: "CharacteristicsInBusinessNetwork",
                column: "BusinessNetworkId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacteristicsInBusinessNetwork_CharacteristicId",
                table: "CharacteristicsInBusinessNetwork",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_Commodities_CategoryId",
                table: "Commodities",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CommoditiesForParsing_CategoryId",
                table: "CommoditiesForParsing",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CommodityCharacteristics_CharacteristicId",
                table: "CommodityCharacteristics",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_CommodityCharacteristics_CommodityId",
                table: "CommodityCharacteristics",
                column: "CommodityId");

            migrationBuilder.CreateIndex(
                name: "IX_CommodityInBusinessNetworks_BusinessNetworkId",
                table: "CommodityInBusinessNetworks",
                column: "BusinessNetworkId");

            migrationBuilder.CreateIndex(
                name: "IX_CommodityInBusinessNetworks_CommodityId",
                table: "CommodityInBusinessNetworks",
                column: "CommodityId");

            migrationBuilder.CreateIndex(
                name: "IX_Responses_CommodityId",
                table: "Responses",
                column: "CommodityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacteristicsInBusinessNetwork");

            migrationBuilder.DropTable(
                name: "CommoditiesForParsing");

            migrationBuilder.DropTable(
                name: "CommodityCharacteristics");

            migrationBuilder.DropTable(
                name: "CommodityInBusinessNetworks");

            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "Responses");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Characteristics");

            migrationBuilder.DropTable(
                name: "BusinessNetworks");

            migrationBuilder.DropTable(
                name: "Commodities");

            migrationBuilder.DropTable(
                name: "FilterTypes");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
