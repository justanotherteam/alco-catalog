﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Exceptions
{
    public class CharacteristicNotFound : Exception
    {
        public CharacteristicNotFound(string characteristicName, Guid categoryId)
            : base($"Characteristic with Name = {characteristicName} and Category with Id = {categoryId.ToString()} not found")
        { }
    }
    public class InvalidCharacteristicValue : Exception
    {
        public InvalidCharacteristicValue(Guid characteristicId, string value)
            : base($"Characteristic with Id = {characteristicId.ToString()}, " +
                  $" and value={value} " +
                  $"is invalid")
        { }
    }
    public class CharacteristicValueParsingException : Exception
    {
        public CharacteristicValueParsingException(string value, string dataType)
            : base($"Parsing error. Parsing value = {value} to date type {dataType}")
        { }
    }
    public class ValueNotFoundException : Exception
    {
        public ValueNotFoundException(Guid characteristicId)
            : base($"No one characteristic value with id = {characteristicId.ToString()} was not found in table CharacteristicValues")
        { }
    }
    public class CommodityNotFoundException : Exception
    {
        public CommodityNotFoundException(Guid id)
            : base($"Commodity with Id = {id.ToString()} not found")
        { }
        public CommodityNotFoundException(string code)
            : base($"Commodity with code = {code} not found")
        { }
    }
    public class CommodityAlreadyExistException : Exception
    {
        public CommodityAlreadyExistException(string code)
            : base($"Commodity with Code = {code} already exist")
        { }
    }
    public class FilterNotFoundExceptions : Exception
    {
        public FilterNotFoundExceptions(Guid? filterTypeId)
            : base($"Filter type with id = {filterTypeId?.ToString()} not found")
        { }
    }
    public class CommodityInBusinessNetworkNotFoundException : Exception
    {
        public CommodityInBusinessNetworkNotFoundException(Guid commodityid, Guid businessNetworkId)
            : base($"Commodity with Id = {commodityid} in BusinessNetwork with Id = {businessNetworkId} not found")
        { }
    }
    public class BusinessNetworkNotFoundException : Exception
    {
        public BusinessNetworkNotFoundException(Guid bnId)
            : base($"BusinessNetwork with Id = {bnId.ToString()} not found")
        { }
    }
    public class EmptyCommodityDataException : Exception
    {
        public EmptyCommodityDataException(string code)
            : base($"Cravling service return commodity with Code = {code} with empty data")
        { }
    }
}
