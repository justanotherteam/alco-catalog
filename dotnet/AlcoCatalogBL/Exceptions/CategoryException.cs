﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Exceptions
{
    public class CategoryException : Exception
    {
        public CategoryException(string message)
            : base(message) { }
    }
}
