﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Models
{
    public class FilterType : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Name { get; set; }
        public List<Characteristic> Characteristics { get; set; }
    }
}
