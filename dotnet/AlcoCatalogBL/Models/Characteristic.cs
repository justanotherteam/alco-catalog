﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Models
{
    public class Characteristic : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Name { get; set; }
        public Guid? CategoryId { get; set; }
        public FilterType FilterType { get; set; }
        public Guid? FilterTypeId { get; set; }
        public Category Category { get; set; }
        public List<CommodityCharacteristic> Commodities { get; set; }
        public List<CharacteristicInBusinessNetwork> CharacteristicsInBusinessNetwork { get; set; }
    }
}
