﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Models
{
    public class Log : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
        public string Inner { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
