﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlcoCatalogBL.Models
{
    [DataContract(IsReference = true)]
    public class Response : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [JsonIgnore]
        [XmlIgnore]
        public Commodity Commodity { get; set; }
        public Guid CommodityId { get; set; }
        public int Stars { get; set; }
        public string Text { get; set; }
        public string Date { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public string Benefits { get; set; }
        public string Disadvantages { get; set; }
        public string AuthorName { get; set; }
    }
}
