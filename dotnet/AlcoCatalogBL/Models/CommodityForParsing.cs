﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Models
{
    public class CommodityForParsing : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Code { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
        public bool IsSendedToParsing { get; set; }
    }
}
