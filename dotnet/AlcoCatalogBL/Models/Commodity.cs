﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Models
{
    [DataContract(IsReference = true)]
    public class Commodity : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Name { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
        public string Code { get; set; }
        public string PictureURL { get; set; }
        public string Description { get; set; }
        public bool IsSendedToParsing { get; set; }
        public List<CommodityCharacteristic> Characteristics { get; set; }
        public List<CommodityInBusinessNetwork> BusinessNetworks { get; set; }
        public List<Response> Responses { get; set; }
    }
}
