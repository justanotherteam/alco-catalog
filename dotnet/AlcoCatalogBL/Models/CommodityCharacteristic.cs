﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Models
{
    [DataContract(IsReference = true)]
    public class CommodityCharacteristic : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid CommodityId { get; set; }
        public Commodity Commodity { get; set; }
        public Guid CharacteristicId { get; set; }
        public Characteristic Characteristic { get; set; }
        public string Value { get; set; }
    }
}
