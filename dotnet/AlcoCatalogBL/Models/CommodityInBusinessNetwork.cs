﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Models
{
    [DataContract(IsReference = true)]
    public class CommodityInBusinessNetwork : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid CommodityId { get; set; }
        public Commodity Commodity { get; set; }
        public Guid BusinessNetworkId { get; set; }
        public BusinessNetwork BusinessNetwork { get; set; }
        public string Link { get; set; }
        public double Price { get; set; }
        public double OldPrice { get; set; }
        public bool IsDiscount { get; set; }
    }
}
