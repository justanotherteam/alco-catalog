﻿
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils
{
    public class CommoditySorter
    {
        public readonly OrderType _orderType;
        public readonly OrderDirection _orderVector;
        public Func<Commodity, object> Function { get; private set; }
        public CommoditySorter(OrderType orderType, OrderDirection orderVector)
        {
            _orderType = orderType;
            _orderVector = orderVector;
            switch (_orderType)
            {
                case OrderType.AlcoholPercent:
                    Function = x => x.Characteristics.FirstOrDefault(c => c.Characteristic.Name == "AlcoholPercent").Value;
                    break;
            }
        }
        public override bool Equals(object obj)
        {
            var comparer = (CommoditySorter)obj;
            return (this._orderType == comparer._orderType && this._orderVector == comparer._orderVector);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    public enum OrderType
    {
        Date,
        Price,
        AlcoholPercent
    }
    public enum OrderDirection
    {
        Desc,
        Asc
    }
}
