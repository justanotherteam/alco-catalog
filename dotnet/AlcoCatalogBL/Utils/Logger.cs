﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Utils
{
    public static class Logger
    {
        private static AppDbContext _dbContext = IoCContainer.Resolve<DbContext>();

        public static void Log(Exception ex, string message)
        {
            _dbContext.Logs.Add(new Log
            {
                Message = message,
                Source = ex.Source,
                Inner = ex.InnerException is null ? ex.InnerException.Message : null,
                ExceptionMessage = ex.Message
            });
            _dbContext.SaveChanges();
        }
    }
}
