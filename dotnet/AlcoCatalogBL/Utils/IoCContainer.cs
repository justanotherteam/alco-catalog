﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils
{
    public static class IoCContainer
    {
        private static readonly Dictionary<Type, ResloveModel> _registeredObjects = new Dictionary<Type, ResloveModel>();

        public static dynamic Resolve<TKey>()
        {
            object parameters = _registeredObjects[typeof(TKey)].Params;
            if(parameters == null)
                return Activator.CreateInstance(_registeredObjects[typeof(TKey)].Type);
            else
                return Activator.CreateInstance(_registeredObjects[typeof(TKey)].Type, _registeredObjects[typeof(TKey)].Params);

        }

        public static void Register<TKey, TConcrete>() where TConcrete : TKey
        {
            _registeredObjects[typeof(TKey)] = new ResloveModel { Type = typeof(TConcrete), Params = null };
        }
        public static void Register<TKey, TConcrete>(object parameters) where TConcrete : TKey
        {
            _registeredObjects[typeof(TKey)] = new ResloveModel { Type = typeof(TConcrete), Params = parameters };
        }
    }
    public class ResloveModel
    {
        public Type Type { get; set; }
        public object Params { get; set; }
    }
}
