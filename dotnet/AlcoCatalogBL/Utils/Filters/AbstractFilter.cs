﻿using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils.Filters
{
    public abstract class AbstractFilter
    {
        protected readonly ICharacteristicDataHandler _repository;
        protected readonly Guid _characteristicId;
        public AbstractFilter(ICharacteristicDataHandler repository, Guid characteristicId)
        {
            _repository = repository;
            _characteristicId = characteristicId;
        }


        public abstract bool Filter(Commodity commodity);
    }
}
