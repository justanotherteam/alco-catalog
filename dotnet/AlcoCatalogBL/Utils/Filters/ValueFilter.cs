﻿using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AlcoCatalogBL.Utils.Filters
{
    public class ValueFilter : AbstractFilter
    {
        private readonly List<string> _values;
        public ValueFilter(ICharacteristicDataHandler repository, Guid characteristicId,
            List<string> values)
                : base(repository, characteristicId)
        {
            _values = values;
        }
        public ValueFilter(ICharacteristicDataHandler repository, Guid characteristicId,
            string value)
                : base(repository, characteristicId)
        {
            _values = new List<string> { value };
        }
        public override bool Filter(Commodity commodity)
        {
            if (!commodity.Characteristics.Select(ch => ch.CharacteristicId).Contains(_characteristicId)) return true;
            string value = _repository.GetCharacteristicValue(commodity.Id, _characteristicId);
            return _values.Any(v => v == value);
        }

    }
}
