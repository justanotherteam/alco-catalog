﻿using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils.Filters
{
    public class RangeFilter : AbstractFilter
    {
        private readonly double _lowestLimit;
        private readonly double _highestLimit;
        public RangeFilter(ICharacteristicDataHandler repository, Guid characteristicId,
            double lowestLimit = double.MinValue, double highestLimit = double.MaxValue)
                : base(repository, characteristicId)
        {
            _lowestLimit = lowestLimit == 0 ? double.MinValue : lowestLimit;
            _highestLimit = highestLimit == 0 ? double.MaxValue : highestLimit;
        }
        public override bool Filter(Commodity commodity)
        {
            if (_lowestLimit == double.MinValue && _highestLimit == double.MaxValue) return true;
            if (!commodity.Characteristics.Select(ch => ch.CharacteristicId).Contains(_characteristicId)) return false;
            string stringValue = _repository.GetCharacteristicValue(commodity.Id, _characteristicId).Replace('.', ',');
            if (!double.TryParse(stringValue, out double value))
            {
                Logger.Log(new InvalidCharacteristicValue(_characteristicId, stringValue), "RangeFilter error");
                return false;
            }
            return (value >= _lowestLimit && value <= _highestLimit);
        }
    }
}
