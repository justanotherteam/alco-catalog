﻿using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using System;
using System.Linq;

namespace AlcoCatalogBL.Utils.Filters
{
    public class BoolFilter : AbstractFilter
    {
        private readonly bool _value;
        public BoolFilter(ICharacteristicDataHandler repository, Guid characteristicId,
            bool value = false)
                : base(repository, characteristicId)
        {
            _value = value;
        }
        public override bool Filter(Commodity commodity)
        {
            if (!commodity.Characteristics.Select(ch => ch.CharacteristicId).Contains(_characteristicId)) return true;
            var stringValue = _repository.GetCharacteristicValue(commodity.Id, _characteristicId);
            if (!bool.TryParse(stringValue, out bool value))
            {
                Logger.Log(new InvalidCharacteristicValue(_characteristicId, stringValue), "BoolFilter error");
                return false;
            }
            return value == _value;
        }
    }
}
