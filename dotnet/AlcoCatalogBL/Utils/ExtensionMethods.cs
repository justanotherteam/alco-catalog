﻿using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils
{
    public static class ExtensionMethods
    {
        public static List<Commodity> Sort(this List<Commodity> list, CommoditySorter sorter)
        {
            if (sorter._orderVector == OrderDirection.Asc)
                return list.OrderBy(sorter.Function).ToList();
            return list.OrderByDescending(sorter.Function).ToList();
        }
    }
}
