﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Data
{
    public static class Constants
    {
        public static class Characteristics
        {
            public static Guid AlcoholPercent = new Guid("5BDAF982-3A63-4294-410F-08D7EED44D3A");
            public static Guid Volume = new Guid("F5BBB4F2-DD0B-4AF6-410E-08D7EED44D3A");
            public static Guid Type = new Guid("E7D08B8E-9BE9-41E4-4110-08D7EED44D3A");
            public static Guid CountInPack = new Guid("E7D08B8E-9BE9-41E4-4110-08D7EED44D31");
            public static Guid Colour = new Guid("E7D08B8E-9BE9-41E4-4110-08D7EED44D32");
            public static Guid Brewery = new Guid("E7D08B8E-9BE9-41E4-4110-08D7EED44D33");
            public static Guid Kind = new Guid("E7D08B8E-9BE9-41E4-4110-08D7EED44D34");
            public static Guid Country = new Guid("E7D08B8E-9BE9-41E4-4110-08D7EED44D35");
            public static Guid Endurance = new Guid("68AC2429-6D8B-401E-4111-08D7EED44D3A");
        };
        public static class BusinessNetworks
        {
            public static Guid Rozetka = new Guid("F9797BC5-D271-410C-05F6-08D7FA73C502");
        }
        public static class FilterType
        {
            public static Guid Range = new Guid("F9797BC5-D271-410C-05F6-18D7FA73C500");
            public static Guid Value = new Guid("F9797BC5-D271-410C-05F6-18D7FA73C501");
            public static Guid Bool = new Guid("F9797BC5-D271-410C-05F6-18D7FA73C502");
        }

        public static class Categories
        {
            public static Guid Vine = new Guid("F9797BC5-D271-410C-05F6-28D7FA73C500");
            public static Guid Bear = new Guid("F9797BC5-D271-410C-05F6-28D7FA73C501");
            public static Guid General = new Guid("F9797BC5-D271-410C-05F6-28D7FA73C502");
        }


    }

}
