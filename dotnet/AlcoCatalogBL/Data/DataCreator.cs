﻿using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlcoCatalogBL.Data
{
    public class DataCreator
    {
        public static void Initial(AppDbContext context)
        {
            if (!context.BusinessNetworks.Any())
                context.BusinessNetworks.AddRange(BusinessNetworks.Values);
            if (!context.Categories.Any())
                context.Categories.AddRange(Categories.Values);
            if (!context.Commodities.Any())
                context.Commodities.AddRange(Commodities.Values);
            //if (!context.CommoditiesForParsing.Any())
            //    context.CommoditiesForParsing.AddRange(CommoditiesForParsing.Values);
            if (!context.CommodityCharacteristics.Any())
                context.CommodityCharacteristics.AddRange(CommodityCharacteristics);
            if (!context.Characteristics.Any())
                context.Characteristics.AddRange(Characteristics.Values);
            if (!context.CommodityInBusinessNetworks.Any())
                context.CommodityInBusinessNetworks.AddRange(CommodityInBusinessNetworks);
            if (!context.Responses.Any())
                context.Responses.AddRange(Responses);
            if (!context.CharacteristicsInBusinessNetwork.Any())
                context.CharacteristicsInBusinessNetwork.AddRange(CharacteristicsInBusinessNetwork);
            if (!context.FilterTypes.Any())
                context.FilterTypes.AddRange(FilterTypes.Values);
            if (!context.Users.Any())
                context.Users.AddRange(Users);
            context.SaveChanges();
        }

        private static Dictionary<string, Category> _categories;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (_categories is null)
                {
                    var list = new List<Category>
                    {
                        new Category(){ Id = Constants.Categories.Bear, Name = "Beer" },
                        new Category(){ Id = Constants.Categories.Vine, Name = "Vine" },
                        new Category(){ Id = Constants.Categories.General, Name = "General" }
                    };
                    _categories = list.ToDictionary(c => c.Name);
                }
                return _categories;
            }
        }

        private static Dictionary<string, FilterType> _filterTypes;
        public static Dictionary<string, FilterType> FilterTypes
        {
            get
            {
                if (_filterTypes is null)
                {
                    var list = new List<FilterType>
                    {
                        new FilterType(){ Id = Constants.FilterType.Bool, Name = "Bool" },
                        new FilterType(){ Id = Constants.FilterType.Range, Name = "Range" },
                        new FilterType(){ Id = Constants.FilterType.Value, Name = "Value" }
                    };
                    _filterTypes = list.ToDictionary(f => f.Name);
                }
                return _filterTypes;
            }
        }

        private static Dictionary<string, Characteristic> _characteristics;
        public static Dictionary<string, Characteristic> Characteristics
        {
            get
            {
                if (_characteristics is null)
                {
                    var list = new List<Characteristic>
                    {
                        new Characteristic(){ Id = Constants.Characteristics.Volume, Name = "Volume", Category = Categories["General"], FilterType = FilterTypes["Range"] },
                        new Characteristic(){ Id = Constants.Characteristics.AlcoholPercent, Name = "AlcoholPercent", Category = Categories["General"], FilterType = FilterTypes["Range"] },
                        new Characteristic(){ Id = Constants.Characteristics.Type, Name = "Type", Category = Categories["Beer"], FilterType = FilterTypes["Value"]},
                        new Characteristic(){ Id = Constants.Characteristics.CountInPack, Name = "CountInPack", Category = Categories["General"], FilterType = FilterTypes["Range"] },
                        new Characteristic(){ Id = Constants.Characteristics.Colour, Name = "Colour", Category = Categories["Beer"],  FilterType = FilterTypes["Value"] },
                        new Characteristic(){ Id = Constants.Characteristics.Brewery, Name = "Brewery", Category = Categories["Beer"],  FilterType = FilterTypes["Value"]},
                        new Characteristic(){ Id = Constants.Characteristics.Kind, Name = "Kind", Category = Categories["General"], FilterType = FilterTypes["Value"] },
                        new Characteristic(){ Id = Constants.Characteristics.Country, Name = "Country", Category = Categories["General"],  FilterType = FilterTypes["Value"] },
                        new Characteristic(){ Id = Constants.Characteristics.Endurance, Name = "Endurance", Category = Categories["Vine"], FilterType = FilterTypes["Range"] }
                    };
                    _characteristics = list.ToDictionary(c => c.Name);
                }
                return _characteristics;
            }
        }

        private static Dictionary<string, Commodity> _commodities;
        public static Dictionary<string, Commodity> Commodities
        {
            get
            {
                if (_commodities is null)
                {
                    var list = new List<Commodity>
                    {
                        new Commodity(){ Id = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F8"), Name = "Чернігівське", Category = Categories["Beer"], Code = "П1", PictureURL = "https://img3.zakaz.ua/20180829.1535552877.ad72436478c_2018-08-29_Tatiana/20180829.1535552877.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg" },
                        new Commodity(){ Id = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F7"), Name = "Біла ніч", Category = Categories["Beer"], Code = "П2", PictureURL = "https://img2.zakaz.ua/20180829.1535552776.ad72436478c_2018-08-29_Tatiana/20180829.1535552776.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg" },
                        new Commodity(){ Id = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F6"), Name = "Коблево", Category = Categories["Vine"], Code = "В1", PictureURL = "https://img2.zakaz.ua/upload.version_1.0.a58488de4964cad26aff94e2b27a125c.350x350.jpeg" },
                        new Commodity(){ Id = new Guid("A6D7D2FC-D4D8-409B-9D44-08D8057066F5"), Name = "Монастирське", Category = Categories["Vine"], Code = "В2", PictureURL = "https://img3.zakaz.ua/src.1584529315.ad72436478c_2020-03-19_Aliona/src.1584529315.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg", Description = "Супер смачне вино" },
                        //new Commodity(){ Category = Categories["Beer"], Code = "180989516"},
                        //new Commodity(){ Category = Categories["Beer"], Code = "199769587"},
                        //new Commodity(){ Category = Categories["Beer"], Code = "199769533"},
                        //new Commodity(){ Category = Categories["Beer"], Code = "191150436"},
                        //new Commodity(){ Category = Categories["Beer"], Code = "96105694"},
                        //new Commodity(){ Category = Categories["Beer"], Code = "14236550"}
                    };
                    _commodities = list.ToDictionary(c => c.Code);
                }
                return _commodities;
            }
        }
        private static Dictionary<string, CommodityForParsing> _commoditiesForParsing;
        public static Dictionary<string, CommodityForParsing> CommoditiesForParsing
        {
            get
            {
                if (_commoditiesForParsing is null)
                {
                    var list = new List<CommodityForParsing>
                    {
                        //new Commodity(){ Name = "Чернігівське", Category = Categories["Beer"], Code = "П1", PictureURL = "https://img3.zakaz.ua/20180829.1535552877.ad72436478c_2018-08-29_Tatiana/20180829.1535552877.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg" },
                        //new Commodity(){ Name = "Біла ніч", Category = Categories["Beer"], Code = "П2", PictureURL = "https://img2.zakaz.ua/20180829.1535552776.ad72436478c_2018-08-29_Tatiana/20180829.1535552776.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg" },
                        //new Commodity(){ Name = "Коблево", Category = Categories["Vine"], Code = "В1", PictureURL = "https://img2.zakaz.ua/upload.version_1.0.a58488de4964cad26aff94e2b27a125c.350x350.jpeg" },
                        //new Commodity(){ Name = "Монастирське", Category = Categories["Vine"], Code = "В2", PictureURL = "https://img3.zakaz.ua/src.1584529315.ad72436478c_2020-03-19_Aliona/src.1584529315.SNCPSG10.obj.0.1.jpg.oe.jpg.pf.jpg.350nowm.jpg.350x.jpg", Description = "Супер смачне вино" },
                        new CommodityForParsing(){ Category = Categories["Beer"], Code = "180989516"},
                        new CommodityForParsing(){ Category = Categories["Beer"], Code = "199769587"},
                        new CommodityForParsing(){ Category = Categories["Beer"], Code = "199769533"},
                        new CommodityForParsing(){ Category = Categories["Beer"], Code = "191150436"},
                        new CommodityForParsing(){ Category = Categories["Beer"], Code = "96105694"},
                        new CommodityForParsing(){ Category = Categories["Beer"], Code = "14236550"}
                    };
                    _commoditiesForParsing = list.ToDictionary(c => c.Code);
                }
                return _commoditiesForParsing;
            }
        }

        private static List<CommodityCharacteristic> _commodityCharacteristics;
        public static List<CommodityCharacteristic> CommodityCharacteristics
        {
            get
            {
                if (_commodityCharacteristics is null)
                {
                    var list = new List<CommodityCharacteristic>
                    {
                        new CommodityCharacteristic(){ Commodity = Commodities["П1"], Characteristic = Characteristics["Volume"], Value = "0,5"},
                        new CommodityCharacteristic(){ Commodity = Commodities["П1"], Characteristic = Characteristics["AlcoholPercent"], Value = "0,05"},
                        new CommodityCharacteristic(){ Commodity = Commodities["П1"], Characteristic = Characteristics["Type"], Value = "Фільтроване"},
                        new CommodityCharacteristic(){ Commodity = Commodities["П2"], Characteristic = Characteristics["Volume"], Value = "0,7"},
                        new CommodityCharacteristic(){ Commodity = Commodities["П2"], Characteristic = Characteristics["AlcoholPercent"], Value = "0,045"},
                        new CommodityCharacteristic(){ Commodity = Commodities["П2"], Characteristic = Characteristics["Type"], Value = "Фільтроване"},
                        new CommodityCharacteristic(){ Commodity = Commodities["В1"], Characteristic = Characteristics["Volume"], Value = "0,7"},
                        new CommodityCharacteristic(){ Commodity = Commodities["В1"], Characteristic = Characteristics["AlcoholPercent"], Value = "0,15"},
                        new CommodityCharacteristic(){ Commodity = Commodities["В1"], Characteristic = Characteristics["Endurance"], Value = "1"},
                        new CommodityCharacteristic(){ Commodity = Commodities["В2"], Characteristic = Characteristics["Volume"], Value = "0,7"},
                        new CommodityCharacteristic(){ Commodity = Commodities["В2"], Characteristic = Characteristics["AlcoholPercent"], Value = "0,17"},
                        new CommodityCharacteristic(){ Commodity = Commodities["В2"], Characteristic = Characteristics["Endurance"], Value = "2"},
                    };
                    _commodityCharacteristics = list;
                }
                return _commodityCharacteristics;
            }
        }


        private static Dictionary<string, BusinessNetwork> _businessNetworks;
        public static Dictionary<string, BusinessNetwork> BusinessNetworks
        {
            get
            {
                if (_businessNetworks is null)
                {
                    var list = new List<BusinessNetwork>
                    {
                        new BusinessNetwork(){ Id = Constants.BusinessNetworks.Rozetka, Name = "Розетка", SiteURL = "https://rozetka.com.ua/", PictureURL = "https://horoshop.ua/images/28/99031323856777.png" },
                        new BusinessNetwork(){ Name = "Бухни.юа", SiteURL = "", PictureURL = "" }
                    };
                    _businessNetworks = list.ToDictionary(c => c.Name);
                }
                return _businessNetworks;
            }
        }


        private static List<CommodityInBusinessNetwork> _commodityInBusinessNetworks;
        public static List<CommodityInBusinessNetwork> CommodityInBusinessNetworks
        {
            get
            {
                if (_commodityInBusinessNetworks is null)
                {
                    var list = new List<CommodityInBusinessNetwork>
                    {
                        new CommodityInBusinessNetwork(){ Commodity = Commodities["П1"], BusinessNetwork = BusinessNetworks["Розетка"], Price = 14},
                        new CommodityInBusinessNetwork(){ Commodity = Commodities["П2"], BusinessNetwork = BusinessNetworks["Розетка"], Price = 16},
                        new CommodityInBusinessNetwork(){ Commodity = Commodities["В1"], BusinessNetwork = BusinessNetworks["Розетка"], Price = 60},
                        new CommodityInBusinessNetwork(){ Commodity = Commodities["В2"], BusinessNetwork = BusinessNetworks["Розетка"], Price = 75.9},
                        new CommodityInBusinessNetwork(){ Commodity = Commodities["П2"], BusinessNetwork = BusinessNetworks["Бухни.юа"], Price = 15.5},
                        new CommodityInBusinessNetwork(){ Commodity = Commodities["В1"], BusinessNetwork = BusinessNetworks["Бухни.юа"], Price = 66},
                        new CommodityInBusinessNetwork(){ Commodity = Commodities["В2"], BusinessNetwork = BusinessNetworks["Бухни.юа"], Price = 70.5}
                    };
                    _commodityInBusinessNetworks = list;
                }
                return _commodityInBusinessNetworks;
            }
        }


        private static List<Response> _responses;
        public static List<Response> Responses
        {
            get
            {
                if (_responses is null)
                {
                    var list = new List<Response>
                    {
                        new Response(){ Commodity = Commodities["П1"], Stars = 5, Text = "Найс"},
                        new Response(){ Commodity = Commodities["П1"], Stars = 3, Text = "На другий день башка болить(("}
                    };
                    _responses = list;
                }
                return _responses;
            }
        }

        private static List<CharacteristicInBusinessNetwork> _characteristicsInBusinessNetwork;
        public static List<CharacteristicInBusinessNetwork> CharacteristicsInBusinessNetwork
        {
            get
            {
                if (_characteristicsInBusinessNetwork is null)
                {
                    var list = new List<CharacteristicInBusinessNetwork>
                    {
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Volume, Name = "Объем, л", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Volume, Name = "Объем 1 шт, л", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.AlcoholPercent, Name = "Крепость", RightTrimCount = 1, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Colour, Name = "Цвет", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Type, Name = "Тип", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Brewery, Name = "Пивоварня", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Kind, Name = "Вид", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Country, Name = "Страна-производитель товара", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Country, Name = "Страна происхождения", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.CountInPack, Name = "Количество штук в упаковке", RightTrimCount = 0, LeftTrimCount = 0},
                        new CharacteristicInBusinessNetwork(){ BusinessNetworkId = BusinessNetworks["Розетка"].Id, CharacteristicId = Constants.Characteristics.Endurance, Name = "Endurance", RightTrimCount = 0, LeftTrimCount = 0}
                    };
                    _characteristicsInBusinessNetwork = list;
                }
                return _characteristicsInBusinessNetwork;
            }
        }

        private static List<User> _users;
        public static List<User> Users
        {
            get
            {
                if (_users is null)
                {
                    var list = new List<User>
                    {
                        new User{ Login="admin", Password="12345" },
                        new User{ Login="qwerty@gmail.com", Password="55555" }
                    };
                    _users = list;
                }
                return _users;
            }
        }
    }
}

