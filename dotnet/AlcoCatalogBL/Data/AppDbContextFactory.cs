﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Data
{
    public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server=localhost;Database=alco_catalog;Trusted_Connection=False;User Id=sa;Password=Dima1234;");

            return new AppDbContext(optionsBuilder.Options);
        }

        public DbContextOptionsBuilder GetDbContextOptionsBuilder()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server=localhost;Database=alco_catalog;Trusted_Connection=False;User Id=sa;Password=Dima1234;");
            return optionsBuilder;
        }
    }
}
