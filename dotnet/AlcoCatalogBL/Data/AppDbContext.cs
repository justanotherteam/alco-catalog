﻿using AlcoCatalogBL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<BusinessNetwork> BusinessNetworks { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Characteristic> Characteristics { get; set; }
        public DbSet<Commodity> Commodities { get; set; }
        public DbSet<CommodityForParsing> CommoditiesForParsing { get; set; }
        public DbSet<CommodityCharacteristic> CommodityCharacteristics { get; set; }
        public DbSet<CommodityInBusinessNetwork> CommodityInBusinessNetworks { get; set; }
        public DbSet<Response> Responses { get; set; }
        public DbSet<CharacteristicInBusinessNetwork> CharacteristicsInBusinessNetwork { get; set; }
        public DbSet<FilterType> FilterTypes { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            #region Characteristic
            modelBuilder.Entity<Characteristic>()
                .HasOne(c => c.Category)
                .WithMany(cg => cg.Characteristics)
                .HasForeignKey(c => c.CategoryId)
                .HasPrincipalKey(cg => cg.Id)
                .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Characteristic>()
                .HasOne(c => c.FilterType)
                .WithMany(ft => ft.Characteristics)
                .HasForeignKey(c => c.FilterTypeId)
                .HasPrincipalKey(ft => ft.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Characteristic>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<Characteristic>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region CommodityCharacteristic
            modelBuilder.Entity<CommodityCharacteristic>()
                .HasOne(cc => cc.Characteristic)
                .WithMany(c => c.Commodities)
                .HasForeignKey(cc => cc.CharacteristicId)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CommodityCharacteristic>()
                .HasOne(cc => cc.Commodity)
                .WithMany(c => c.Characteristics)
                .HasForeignKey(cc => cc.CommodityId)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CommodityCharacteristic>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<CommodityCharacteristic>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region CommodityForParsing
            modelBuilder.Entity<CommodityForParsing>()
                .HasOne(c => c.Category)
                .WithMany(cg => cg.CommoditiesForParsing)
                .HasForeignKey(c => c.CategoryId)
                .HasPrincipalKey(cg => cg.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CommodityForParsing>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<CommodityForParsing>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region Commodity
            modelBuilder.Entity<Commodity>()
                .HasOne(c => c.Category)
                .WithMany(cg => cg.Commodities)
                .HasForeignKey(c => c.CategoryId)
                .HasPrincipalKey(cg => cg.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Commodity>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<Commodity>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region CommodityInBusinessNetwork
            modelBuilder.Entity<CommodityInBusinessNetwork>()
                .HasOne(cb => cb.Commodity)
                .WithMany(c => c.BusinessNetworks)
                .HasForeignKey(cb => cb.CommodityId)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CommodityInBusinessNetwork>()
                .HasOne(cb => cb.BusinessNetwork)
                .WithMany(b => b.Commodities)
                .HasForeignKey(cb => cb.BusinessNetworkId)
                .HasPrincipalKey(b => b.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CommodityInBusinessNetwork>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<CommodityInBusinessNetwork>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region Response
            modelBuilder.Entity<Response>()
                .HasOne(r => r.Commodity)
                .WithMany(c => c.Responses)
                .HasForeignKey(r => r.CommodityId)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Response>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<Response>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region Category

            modelBuilder.Entity<CommodityCharacteristic>()
                .HasOne(cc => cc.Characteristic)
                .WithMany(c => c.Commodities)
                .HasForeignKey(cc => cc.CharacteristicId)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<Category>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<Category>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region BusinessNetworknse
            modelBuilder.Entity<BusinessNetwork>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<BusinessNetwork>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region User
            modelBuilder.Entity<User>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<User>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region CharacteristicInBusinessNetwork
            modelBuilder.Entity<CharacteristicInBusinessNetwork>()
                .HasOne(cbn => cbn.Characteristic)
                .WithMany(ch => ch.CharacteristicsInBusinessNetwork)
                .HasForeignKey(cbn => cbn.CharacteristicId)
                .HasPrincipalKey(ch => ch.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CharacteristicInBusinessNetwork>()
                .HasOne(cbn => cbn.BusinessNetwork)
                .WithMany(bn => bn.CharacteristicsInBusinessNetwork)
                .HasForeignKey(cbn => cbn.BusinessNetworkId)
                .HasPrincipalKey(ch => ch.Id)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CharacteristicInBusinessNetwork>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<CharacteristicInBusinessNetwork>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
            #region Log
            modelBuilder.Entity<Log>()
                .Property(x => x.CreatedOn)
                .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<Log>()
                .Property(x => x.ModifiedOn)
                .HasDefaultValueSql("GETDATE()");
            #endregion
        }
    }
}
