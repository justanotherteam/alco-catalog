﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Services;
using AlcoCatalogBL.Services.Models.Commodities;
using AlcoCatalogBL.Utils;
using Microsoft.EntityFrameworkCore;
using System;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server=DESKTOP-Q8D1INN;Database=alco_catalog;Trusted_Connection=True;");
            IoCContainer.Register<ICommodityDataHandler, CommodityRepository>();
            IoCContainer.Register<ICharacteristicDataHandler, CharacteristicRepository>();
            IoCContainer.Register<DbContext, AppDbContext>(optionsBuilder.Options);
            DataCreator.Initial(IoCContainer.Resolve<DbContext>());

            CommodityService service = new CommodityService();
            var categories = service.GetRange(new CommodityGetRangeRequest { CommoditiesCount = 1, PageNumber = 1});
            Console.WriteLine("Hello World!");
        }
    }
}
