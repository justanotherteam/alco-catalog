﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ParsingWorkerService.RabbitMQ;
using ParsingWorkerService.RabbitMQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static ParsingWorkerService.RabbitMQ.IRabbitMQHendler;

namespace ParsingWorkerService.Services
{
    public interface ICommodityService
    {
        public Task Parse(CancellationToken stoppingToken);
    }
    public class CommodityService : ICommodityService
    {
        private readonly ILogger _logger;
        private readonly ICommodityDataHandler _commodities;
        private readonly ICharacteristicDataHandler _characteristics;
        private readonly IRabbitMQHendler _rabbitMQHendler;
        private List<string> _dataFromRabbit;
        public CommodityService(ILogger<CommodityService> logger, IRabbitMQHendler rabbitMQHendler)
        {
            _rabbitMQHendler = rabbitMQHendler;
            _logger = logger;
            _commodities = IoCContainer.Resolve<ICommodityDataHandler>();
            _characteristics = IoCContainer.Resolve<ICharacteristicDataHandler>();
            _dataFromRabbit = new List<string>();
            SubscribeOnCommoditiesDataGet();
        }

        public async Task Parse(CancellationToken stoppingToken)
        {
            while (true)
            {
                try
                {
                    ParseCommodities();
                    CreateTasksForParsing();
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Parsing error");
                }
                await Task.Delay(10000, stoppingToken);
            }
        }



        public List<string> GetCommoditiesCodes()
            =>  _commodities.GetAll().Select(c => c.Code).ToList();

        private void CreateTasksForParsing()
        {
            var codes = _commodities.GetAll().Where(c => c.ModifiedOn <= DateTime.Now.AddDays(-1) && !c.IsSendedToParsing).Select(c => c.Code).ToList();
            codes.AddRange(_commodities.GetAllCommoditiesForParsing().Where(c => !c.IsSendedToParsing).Select(c => c.Code).ToList());
            _logger.LogInformation($"Commodities for parse count = {codes.Count}");
            foreach (var code in codes)
            {
                _rabbitMQHendler.SetInQueue("rozetka-parser-input", code);
                _commodities.SetStateForSendForParsing(code, true);
            }
        }

        private void ParseCommodities()
        {
            var dataArr = new string[_dataFromRabbit.Count];
            _dataFromRabbit.CopyTo(dataArr);
            _dataFromRabbit = new List<string>();

            foreach (var json in dataArr)
            {
                try
                {
                    var model =
                       JsonConvert.DeserializeObject<CommodityModel>(json);

                    if (model.Data == null)
                        throw new EmptyCommodityDataException(model.Code);
                    var commodity = _commodities.GetCommodity(model.Code);
                    if (commodity is null)
                    {
                        _commodities.SetCommodityData(new Commodity
                        {
                            Code = model.Code,
                            CategoryId = _commodities.GetCategoryIdFromCommodityForParsing(model.Code)
                        });
                        commodity = _commodities.GetCommodity(model.Code);
                    }
                    var businessNetwork = commodity.BusinessNetworks
                        .FirstOrDefault(bn => bn.BusinessNetworkId == Constants.BusinessNetworks.Rozetka);
                    if (businessNetwork is null)
                        businessNetwork = new CommodityInBusinessNetwork
                        {
                            BusinessNetwork = _commodities.GetBusinessNetwork(Constants.BusinessNetworks.Rozetka),
                            BusinessNetworkId = Constants.BusinessNetworks.Rozetka
                        };
                    else
                        _commodities.RemoveBusinessNetwork(businessNetwork.Id);
                    businessNetwork.Price = model.Data.Price;
                    businessNetwork.Link = model.Data.URL;
                    businessNetwork.OldPrice = double.Parse(model.Data.OldPrice.Replace(" ", ""));
                    businessNetwork.IsDiscount = model.Data.IsDiscount;
                    commodity.BusinessNetworks.Add(businessNetwork);

                    commodity.PictureURL = model.Data.ImageURL;
                    commodity.Name = model.Data.Title;
                    if (model.Data.Comments != null)
                    {
                        _commodities.RemoveResponses(commodity.Id);
                        foreach (var comment in model.Data.Comments)
                        {
                            commodity.Responses.Add(new Response
                            {
                                Text = comment.Text,
                                Stars = comment.Stars,
                                AuthorName = comment.Author,
                                Date = comment.Date
                            });
                        }
                    }
                    commodity.ModifiedOn = DateTime.Now;
                    commodity.IsSendedToParsing = false;
                    _commodities.SetCommodityData(commodity);
                    if (model.Data.Characteristics != null)
                    {
                        foreach (var characteristic in model.Data.Characteristics)
                        {
                            SetCharacteristicForCommodity(commodity.Id, characteristic.Key, characteristic.Value);
                        }
                    }
                    if (!_commodities.TryDeleteCommodityForParsing(commodity.Code))
                        _commodities.SetStateForSendForParsing(commodity.Code, false);
                    _logger.LogInformation($"Commodity {commodity.Code} parsed");

                }
                catch (Exception e)
                {
                    //_commodities.SetStateForSendForParsing(model.Code, false);
                    _logger.LogError($"Error in CreateTasksForParsing. Messgae = {e.Message}, Json = {json}");
                }

            }
        }

        private void SetCharacteristicForCommodity(Guid commodityId, string characteristicName, string characteristicValue)
        {
            var characteristic = _characteristics.GetCharacteristicInBusinessNetwork(characteristicName, Constants.BusinessNetworks.Rozetka);
            if (characteristic is null) return;
            characteristicValue = characteristicValue.Replace('.', ',');
            characteristicValue = characteristicValue.Substring(0, characteristicValue.Length - characteristic.RightTrimCount);
            characteristicValue = characteristicValue.Substring(characteristic.LeftTrimCount, characteristicValue.Length);
            _characteristics.UpdateCharactersticValue(characteristic.CharacteristicId, commodityId, characteristicValue);
        }

        private void SubscribeOnCommoditiesDataGet()
        {
            UpdateCommodity callback = new UpdateCommodity((json) =>
            {
                _dataFromRabbit.Add(json);
            });
            _rabbitMQHendler.GetFromQueue("rozetka-parser-output", callback);
        }

    }
}
