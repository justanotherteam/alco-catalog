using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHandlers;
using AlcoCatalogBL.DataHandlers.Interfaces;
using AlcoCatalogBL.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ParsingWorkerService.RabbitMQ;
using ParsingWorkerService.Services;


namespace ParsingWorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
                    optionsBuilder.UseSqlServer("Server=localhost;Database=alco_catalog;Trusted_Connection=False;User Id=sa;Password=Dima1234;");
                    IoCContainer.Register<ICommodityService, CommodityService>();
                    IoCContainer.Register<ICommodityDataHandler, CommodityRepository>();
                    IoCContainer.Register<ICharacteristicDataHandler, CharacteristicRepository>();
                    IoCContainer.Register<DbContext, AppDbContext>(optionsBuilder.Options);
                    services.AddTransient<IRabbitMQHendler, RabbitMQHendler>();
                    services.AddHostedService<ConsumeScopedServiceHostedService>();
                    services.AddScoped<ICommodityService, CommodityService>();
                });
    }
}
