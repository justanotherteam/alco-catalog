﻿using AlcoCatalogBL.Utils;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using static ParsingWorkerService.RabbitMQ.IRabbitMQHendler;

namespace ParsingWorkerService.RabbitMQ
{
    public class RabbitMQHendler : IRabbitMQHendler, IDisposable
    {
        private readonly ILogger _logger;
        private readonly ConnectionFactory _factory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        public RabbitMQHendler(ILogger<RabbitMQHendler> logger)
        {
            _logger = logger;
            _factory = new ConnectionFactory();
            _factory.Uri = new Uri($"amqp://guest:guest@77.47.224.185:5672/");
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: "rozetka-parser-output",
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
            _channel.QueueDeclare(queue: "rozetka-parser-input",
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
        }

        public void Dispose()
        {
            _connection.Dispose();
            _channel.Dispose();
        }

        public void SetInQueue(string queueName, string message)
        {

            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: "",
                                    routingKey: queueName,
                                    basicProperties: null,
                                    body: body);
            _logger.LogInformation($"Commodity with code = {message} sent to crawling");
        }

        public void GetFromQueue(string queueName, UpdateCommodity callback)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                callback(Encoding.UTF8.GetString(ea.Body.ToArray()));
            };
            _channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }
    }
}
