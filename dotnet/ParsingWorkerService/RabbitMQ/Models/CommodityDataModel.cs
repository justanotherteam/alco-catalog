﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParsingWorkerService.RabbitMQ.Models
{
    public class CommodityDataModel
    {
        public string Title { get; set; }
        public string URL { get; set; }
        public double Price { get; set; }
        public Dictionary<string, string> Characteristics { get; set; }
        public string ImageURL { get; set; }
        public bool IsDiscount { get; set; }
        public string OldPrice { get; set; }
        public List<CommentModel> Comments { get; set; }
    }
}
