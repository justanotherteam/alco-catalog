﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParsingWorkerService.RabbitMQ.Models
{
    public class CommentModel
    {
        public string Date { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public int Stars { get; set; }
    }
}
