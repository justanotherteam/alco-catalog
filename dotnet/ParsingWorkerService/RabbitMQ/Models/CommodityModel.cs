﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParsingWorkerService.RabbitMQ.Models
{
    public class CommodityModel
    {
        public CommodityDataModel Data { get; set; }
        public string Code { get; set; }
    }
}
