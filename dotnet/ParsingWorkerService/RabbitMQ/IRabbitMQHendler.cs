﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParsingWorkerService.RabbitMQ
{
    public interface IRabbitMQHendler
    {
        public delegate void UpdateCommodity(string json);
        public void SetInQueue(string queueName, string message);
        public void GetFromQueue(string queueName, UpdateCommodity callback);
    }
}
