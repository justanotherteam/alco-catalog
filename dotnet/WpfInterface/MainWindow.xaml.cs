﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHendlers.DBDataHendlers;
using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.DataHendlers.SerializeDataHendlers;
using AlcoCatalogBL.Services;
using AlcoCatalogBL.Services.Interfaces;
using AlcoCatalogBL.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server=77.47.224.185;Database=rozetka-parser;User Id=alcoholic;Password=qwerty;Trusted_Connection=False;MultipleActiveResultSets=true");
            IoCContainer.Register<ICommodityService, CommodityService>();
            IoCContainer.Register<ICommodityDataHendler, CommodityRepository>();
            IoCContainer.Register<ICharacteristicDataHendler, CharacteristicRepository>();
            IoCContainer.Register<AbstractSerializer, JSonSerializer>();
            IoCContainer.Register<DbContext, AppDbContext>(optionsBuilder.Options);
        }

        private void Categories_Button_Click(object sender, RoutedEventArgs e)
        {
            CategoryEditor win = new CategoryEditor();
            win.Show();
        }

        private void Responses_Button_Click(object sender, RoutedEventArgs e)
        {
            ResponseEditor win = new ResponseEditor();
            win.Show();
        }
    }
}
