﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.DataHendlers.SerializeDataHendlers;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfInterface
{
    /// <summary>
    /// Interaction logic for ResponseEditor.xaml
    /// </summary>
    public partial class ResponseEditor : Window
    {
        private readonly AppDbContext _db;
        private readonly ICommodityDataHendler _commodities;
        private readonly ICharacteristicDataHendler _characteristic;
        private readonly AbstractSerializer _serializer;
        public ResponseEditor()
        {
            InitializeComponent();
            _serializer = IoCContainer.Resolve<AbstractSerializer>();
            _db = IoCContainer.Resolve<DbContext>();
            _commodities = IoCContainer.Resolve<ICommodityDataHendler>();
            _characteristic = IoCContainer.Resolve<ICharacteristicDataHendler>();
            RenewViewList();
        }
        private void RenewViewList()
        {
            this.Responses_List.Items.Clear();
            var gridView = new GridView();
            this.Responses_List.View = gridView;
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Text",
                DisplayMemberBinding = new Binding("Text")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Stars",
                DisplayMemberBinding = new Binding("Stars")
            });
            foreach (var response in _db.Responses.ToList())
            {
                this.Responses_List.Items.Add(response);
            }
        }

        

        private void Accept_Button_Click_1(object sender, RoutedEventArgs e)
        {
            var responseText = this.ResponseText_TextBox.Text;
            int stars = 0;
            if (string.IsNullOrEmpty(responseText))
                return;
            int.TryParse(Stars_TextBox.Text, out stars);
            _db.Responses.Add(new Response { Text = responseText, Stars = stars, CommodityId = _db.Commodities.First().Id});
            _db.SaveChanges();
            RenewViewList();
        }

        private void Serialize_Button_Click(object sender, RoutedEventArgs e)
        {
            _serializer.Serialize();
        }
    }
}
