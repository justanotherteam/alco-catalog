﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.DataHendlers.SerializeDataHendlers;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Services.Interfaces;
using AlcoCatalogBL.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfInterface
{
    /// <summary>
    /// Interaction logic for CategoryEditor.xaml
    /// </summary>
    public partial class CategoryEditor : Window
    {
        private readonly AppDbContext _db;
        private readonly ICommodityDataHendler _commodities;
        private readonly AbstractSerializer _serializer;
        public CategoryEditor()
        {
            InitializeComponent();
            _serializer = IoCContainer.Resolve<AbstractSerializer>();
            _db = IoCContainer.Resolve<DbContext>();
            _commodities = IoCContainer.Resolve<ICommodityDataHendler>();
            RenewViewList();
        }
        private void RenewViewList()
        {
            this.Categories_List.Items.Clear();
            var gridView = new GridView();
            this.Categories_List.View = gridView;
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Id",
                DisplayMemberBinding = new Binding("Id")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Name",
                DisplayMemberBinding = new Binding("Name")
            });
            foreach (var category in _commodities.GetCategories())
            {
                this.Categories_List.Items.Add(new Category { Id = category.Key, Name = category.Value });
            }
        }

        private void Accept_Button_Click(object sender, RoutedEventArgs e)
        {
            var categoryName = this.Category_Name_TextBox.Text;
            if (string.IsNullOrEmpty(categoryName))
                return;
            _db.Categories.Add(new Category { Name = categoryName });
            _db.SaveChanges();
            RenewViewList();
        }

        private void Serialize_Button_Click(object sender, RoutedEventArgs e)
        {
            _serializer.Serialize();
        }
    }
}
